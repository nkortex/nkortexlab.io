---
title: (ES)Instalar f.lux en Fedora 18
date: 2013/02/27 21:33:01
layout: post
path: "/flux-install-fedora/"
category: "Imported from my older blog"
---
f.lux es un programa que camba el brillo y el tono de tu pantalla (temperatura) automáticamente según la claridad que haya. Ayudando a relajar tu vista y ahorrando batería si usas portátil. Según algunos expertos, los tonos cálidos de las pantallas por la noche son mejores para nuestra vista. ![](http://dl.herf.org/flux-icon-sm.png) No he encontrado ningún tutorial en Español sobre como instalarlo así que aquí os lo dejo: Nos bajamos el .tgz desde aquí: <http://secure.herf.org/flux/xflux.tgz> Lo descomprimimos en /usr/bin Al ejecutarlo le decimos donde estamos con nuestra latitud y longitud. Para saber estos datos, podemos verlos en Google Maps. Por ejemplo, si nuestra latitud es 24.342 y nuestra longitud es 25.111 ejecutaríamos el programa de este modo: 
    
    
    xflux -l 24.342 -g 25.111

(-l indica latitud y -g longitud) Ahora solo hace falta añadir la aplicación para que se inicie al encender el equipo y listo! Al principio os resultará raro, pero probad unos días y veréis como os convence

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
