---
title: (ES)Va siendo hora de que empieces a plantearte todo
date: 2013/01/03 13:34:37
layout: post
path: "/think/"
category: "Imported from my older blog"
---
Lo primero que deberías plantearte es las cosas que dejas pasar. Dejas pasar muchísimas cosas a lo largo del día. En vez de afanarte por vivir cada uno de tus momentos, dejas que estos pasen, pensando quizás que algún día volverán. Lo siento amigo pero no. Nunca vuelven. Cada minuto que pasa y que no es aprovechado completamente se pierde en el tiempo. Se convertirá en un instante perdido que nadie recordará jamás, pues no merecerá ser recordado. Y deberías darte cuenta antes de que sea demasiado tarde. No dejes de dar una vuelta cuando todavía hace Sol porque estés viendo la televisión, pues llegará un día en el que no puedas disfrutar de ese momento, y será demasiado tarde. No dejes de demostrar a los tuyos que les quieres, que les aprecias, pues llegará un día en el que no estarán ahí para escucharte, y será demasiado tarde. No dejes que te esclavicen, pues te acostumbrarás a la servidumbre y cuando quieras salir, será demasiado tarde. No dejes que se te vaya la vida mientras te entretienes con nimiedades, pues solo tienes una; y llegará el día, cuando no haya vuelta atrás, en el que no sabrás en que has empleado tus días, pues ninguno de esos días te dio motivo alguno para ser recordado.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
