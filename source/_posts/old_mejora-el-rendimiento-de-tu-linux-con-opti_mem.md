---
title: (ES)Mejora el rendimiento de tu Linux con opti_mem
date: 2013/12/29 12:25:51
layout: post
path: "/linux-performance-optimicer/"
category: "Imported from my older blog"
---

![](http://educacionmiguel.blogspot.es/img/RAM.jpg) Hace poco descubrí opti_mem. Este es un script muy simple que optimiza el uso que hace nuestro sistema Linux de la memoria principal. Entre otras cosas equilibra el uso de memoria de intercambio, ofreciéndonos una sustancial mejora del rendimiento general del sistema. Para usarlo basta con ejecutar el siguiente comando en un terminal: 
    
    
    wget -q http://023.es/opti_mem.sh; bash opti_mem.sh

Puede ser una buena idea añadirlo en forma de alias para tenerlo a mano cuando el rendimiento empiece a caer.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[wasabu](#310 "2014-01-01 11:29:18"):** muy bueno, ahora la computadora me anda genial!!!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
