---
title: (ES)Introducción al Bash Scripting, 2ª Clase
date: 2013/09/12 08:29:52
layout: post
path: "/bash-intro-2/"
category: "Imported from my older blog"
---

Antes de nada quería pedir disculpas por pasar tanto tiempo sin publicar, pero he vuelto y os traigo la segunda clase del PDF "Introducción al Bash Scripting". Contenido: • Estructuras de control II ◦ Bloques While y Until ◦ Case • Parámetros • Funciones • Redirección • Cauces Este PDF, al igual que el anterior, tiene una licencia Creative Commons Reconocimiento-NoComercial-CompartirIgual 3.0 Unported. Con esta licencia tienes permiso para copiar y distribuir el contenido como quieras, siempre que cites al autor original y no lo uses con fines comerciales:http://www.mediafire.com/?fcpg1bp7x4ubafm

La primera lección la podéis descargar desde aquí: <http://www.mediafire.com/view/rg3y7m686yxab3p/1.pdf>

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Andrés Orduña](#230 "2013-09-13 20:25:28"):** Un saludo desde México! Ya descargué la primera clase y todas las que siguen formaran parte de mi carpeta de consulta, "Tumba burros" como decimos por acá! :D

**[admin](#231 "2013-09-14 17:45:16"):** Me alegro de que te fuera de utilidad, Andrés! Gracias por tu comentario y por tu visita. En breve publicaré nuevas lecciones



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
