---
title: (ES)Solucionar error "Multilib version problems found" en Yum
date: 2013/01/30 12:14:06
layout: post
path: "/multilib-version-problems-found-fix/"
category: "Imported from my older blog"
---
Intentando actualizar mi Fedora como cualquier otra vez, me ha salido el error: 
    
    
    Error:  Multilib version problems found
    

Y no me dejaba continuar. Arreglarlo es tan fácil como ejecutar: 
    
    
    sudo package-cleanup --cleandupes
    

Así de sencillo :)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
