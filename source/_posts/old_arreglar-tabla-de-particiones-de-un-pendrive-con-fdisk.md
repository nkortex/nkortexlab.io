---
title: (ES)Arreglar tabla de particiones de un pendrive con fdisk
date: 2015/09/24 10:21:46
---
En ocasiones, al grabar imágenes .iso en memorias flash con dd se corrompe la tabla de particiones del dispositivo y se queda inutilizable. Según la magnitud del destrozo, es posible que ni siquiera gparted sepa arreglarlo. Para esos momentos lo mejor es tener a mano fdisk y arreglarlo "a mano". Esta aplicación probablemente venga instalada en tu distribución. Para empezar, vamos a ver el nombre del dispositivo en cuestión con: 
    
    
    # fdisk -l

Una vez lo identificamos, por ejemplo /dev/sdb, ejecutamos: 
    
    
    # fdisk /dev/sdb

Y nos pide los comandos que queramos realizar sobre ese dispositivo. Estos cambios no se escriben hasta que no ejecutamos 'w', por lo que si te equivocas, puedes empezar de nuevo sin peligro. Lo que queremos hacer es borrar la lista de particiones y crear una nueva partición. Ejecutamos: 
    
    
    o (borra la tabla)
    
    n(crea una partición)
    
    p(esta partición será primaria)
    
    1(numero de partición)
    
    Luego dejamos los valores de tamaño por defecto (todo el dispositivo)
    
    t(elegir tipo de formato)
    
    w(escribir datos permanentemente)

Después de esto, tendremos un pen como nuevo

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
