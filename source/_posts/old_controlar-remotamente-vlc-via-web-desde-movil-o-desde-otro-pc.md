---
title: (ES)Controlar remotamente VLC vía web (desde móvil o desde otro PC)
date: 2013/01/07 12:03:50
layout: post
path: "/control-vlc-web-interface/"
category: "Imported from my older blog"
---
Tras deciros como bajaros automáticamente los subtítulos desde VLC para vuestras series en un post anterior, hoy os explico como configurar VLC para poder controlarlo vía web desde tu teléfono o desde otro ordenador. No es dificil y os hará la vida más fácil, al poder usar tu móvil como mando a distancia desde la cama :D ![](http://images.videolan.org/images/largeVLC.png) Lo primero que hay que hacer es abrir el reproductor, menú herramientas > preferencias (o control + P) vamos a "entrada y códecs" y en Transporte de emisión Live555 marcamos http. Esto está marcado por defecto, pero asegúrate de que es así. Luego hay que entrar en unas opciones más detalladas por lo que abajo a la derecha seleccionamos "todo" en vez de "sencillo". Esto nos mostrará muchísimas más opciones. Vé a "interfaces principales" dentro de "Interfaces" allí marcamos "Web" y pulsamos guardar. De este modo ya debería funcionar, sin embargo, VLC tiene una seguridad extra que debemos quitar para que nos deje acceder vía web. Para ello hacemos lo siguiente: 
    
    
    sudo gedit /usr/share/vlc/lua/http/.hosts

En vez de gedit, puedes usar nano o tu editor de texto favorito. Esto es en linux. En Windows, el archivo .hosts se encuentra en: [Carpeta de VLC]\VideoLAN\VLC\lua\http pero no voy a entrar en detalles acerca de windows porque os vais a encontrar con problemas de permisos al intentar cambiar ese archivo. Una vez abierto, cambiamos el contenido que tenga por esto: 
    
    
    #
    # Access-list for VLC HTTP interface
    # $Id$
    #
    
    # localhost
    ::1
    127.0.0.1
    
    # link-local addresses
    fe80::/64
    
    # private addresses
    fc00::/7
    fec0::/10
    10.0.0.0/8
    172.16.0.0/12
    192.168.0.0/16
    169.254.0.0/16
    
    # The world (uncommenting these 2 lines is not quite safe)
    #::/0
    #0.0.0.0/0

Lo único que he hecho es descomentar algunas líneas. Ya si que sí. Accedes a <http://127.0.0.1:8080/> (con VLC abierto) y te saldrá algo parecido a esto: ![Captura de pantalla de 2013-01-07 12:55:00](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-01-07-125500.png) Desde esta interfaz puedes controlar desde la línea de tiempo, hasta el volumen; pudiendo también pausar, reanudar, abrir otro archivo de vídeo o activar la pantalla completa. Pero claro, esto sólo desde tu propio pc, lo cual no es muy útil que digamos... Para acceder desde otro dispositivo dentro de la misma red (tu portátil o tu móvil) accede a la dirección de tu ordenador (por ejemplo 192.168.1.121) seguido del puerto 8080: **192.168.1.121:8080** Y listo! :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
