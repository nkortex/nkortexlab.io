---
title: (ES)Introducción al Bash Scripting, 1ª Clase
date: 2013/04/29 11:55:41
layout: post
path: "/bash-intro-1/"
category: "Imported from my older blog"
---

Como algunos de vosotros ya sabréis, estoy preparando material para aprender a utilizar el terminal de linux y hacer scripts bash. Hoy os traigo el primer PDF cuyo contenido es: • Introducción a los scripts • Variables y arrays • Estructuras de control I ◦ Bloques If-Else ◦ Bucles For El documento tiene una licencia Creative Commons Reconocimiento-NoComercial-CompartirIgual 3.0 Unported. Con esta licencia tienes permiso para copiar y distribuir el contenido como quieras, siempre que cites al autor original y no lo uses con fines comerciales: [http://www.mediafire.com/view/?rg3y7m686yxab3p](http://www.mediafire.com/view/?rg3y7m686yxab3p) Muchas gracias por tu interés :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Ronald](#132 "2013-07-10 21:13:50"):** Hola Cortex. Excelente blog, no pude pagar con un Tweet porque no tengo Twitter, pero si con un +1 aparte de la recomendación con amigos de redes sociales y trabajo! Saludos.

**[admin](#147 "2013-07-26 08:58:33"):** Muchas gracias Ronald !!! Me alegro que te guste ;)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
