---
title: (ES)(Bash Scripting)Programar tareas con cron
date: 2013/10/13 14:26:21
---
cron es una herramienta presente en la inmensa mayoría de sistemas Linux que se utiliza para programar la ejecución de programas en unos momentos establecidos o cada cierto intervalo de tiempo. Para conseguir que un determinado programa se ejecute en el momento que nosotros queramos, hay que indicarle a cron la hora, el día,... con una estructura como esta: mm hh dd MM ss comando_a_ejecutar

      * mm – Minuto en el que se ejecutará(0-59)
      * hh – Hora (0-23)
      * dd – Día de mes (1-31)
      * MM – Mes (1-12)
      * ss – Día de la semana (0-6 o sun, mon, tue...)
![](http://sathisharthars.files.wordpress.com/2013/05/crontab-syntax.gif)   Puede parecer raro en un principio, a base de usarlo te acabas acostumbrando. Es importante tener claro que, además de valores enteros, podemos utilizar asteriscos a modo de comodín; un asterisco sustituye a todos los valores posibles del campo. Veamos unos ejemplos:

  * * * * * * script_que_se_ejecuta_cada_minuto
  * 30 * * * * script_que_se_ejecuta_cada_hora (A y media)
  * 0 3 * * * script_que_se_ejecuta_cada_dia (A las 3)
  * 30 12 * * 2 script_que_se_ejecuta_cada_martes (A las 12 y media)
  * 0 0 15 1 * script_que_se_ejecuta_cada_15_de_enero (A medianoche)
Si queremos que se ejecuten en intervalos de tiempo más precisos, podemos utilizar el signo “ / “, o el signo “ - “ ; por ejemplo:

  * 0 */2 * * * script_que_se_ejecuta_cada_2_horas
  * */15 * * * * script_que_se_ejecuta_cada_15_minutos
  * 0 8-13 * * * script_que_se_ejecuta_cada_hora_de_8_a_1
  * 0 12 * * 1-5 script_que_se_ejecuta_a_las_12_de_lunes_a_viernes
Para introducir comentarios, se puede utilizar el carácter “#”. Todo lo que aparezca detrás de este carácter será ignorado por cron. Cron admite también algunos alias para las entradas más comunes:

  * @yearly o @annually script_que_se_ejecuta_cada_año
  * @monthly script_que_se_ejecuta_cada_mes
  * @weekly script_que_se_ejecuta_cada_semana
  * @daily script_que_se_ejecuta_cada_día
  * @hourly script_que_se_ejecuta_cada_hora
  * @reboot script_que_se_ejecuta_tras_cada_reinicio
Para editar las entradas de cron que ya tengamos programadas o para añadir nuevas, vamos a utilizar el comando:

crontab -e
Si lo que queremos es simplemente listarlas, podemos hacerlo con:

crontab -l

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
