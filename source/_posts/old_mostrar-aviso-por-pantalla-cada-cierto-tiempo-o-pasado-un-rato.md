---
title: (ES)Mostrar aviso por pantalla cada cierto tiempo o pasado un rato
date: 2013/11/06 16:10:41
layout: post
path: "/using-display-from-cron-works/"
category: "Imported from my older blog"
---

Y tan pronto publico el post 100, ahora voy con el 101 ;) Antes tenía un problema y era que no podía desplegar ventanas de zenity desde cron; es decir, si ejecutaba un script que creaba cuadros de diálogo de zenity funcionaba, pero si ese script lo planificaba mediante cron, no lo mostraba. Hoy he encontrado la solución. Esta solución consiste en especificar la pantalla (display) sobre la que se mostrará. Esto soluciona el problema. ¿Y como especificamos que pantalla usar? Pues mediante la variable DISPLAY: 
    
    
    DISPLAY=:0 zenity --entry                 #Muestra una entrada zenity en la pantalla 0

Esto hará que la veamos en la pantalla principal. Y puede, por supuesto, utilizarse con otros programas que usen X (gráficos); así como usarse en displays virtuales (ver [xvfb](http://en.wikipedia.org/wiki/Xvfb)). Supongo que no funcionaba porque mostraba la ventana en otra pantalla, pero si alguien sabe el motivo que lo comente. Ahora vamos a utilizar este conocimiento para crearnos un programita de alarmas: no aprendemos si no lo utilizamos. El script va a consistir en lo siguiente: 

  * Consultar el asunto
  * Pedir datos necesarios en formato at
  * Instalarla en el sistema
Ahí va el código: 
    
    
    #!/bin/bash
    
    #Programado para 023.es
    #Utilizalo, copialo, modificalo pero mantén la autoría ;)
    
    asunto=$(zenity --entry --text="Introduce el asunto a recordar")
    momento=$(zenity --entry --text="Introduce el momento en formato at")
    echo "DISPLAY=:0 zenity --info --text=\"$asunto\"" | at $momento > /dev/null	
    if [ $? == 0 ]; then
    	zenity --info --text="Todo Ok"
    else
    	zenity --info --text="Error al crear la alarma (Formato incorrecto? -> $momento)"
    fi

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Juangmuriel](#449 "2016-03-10 20:40:39"):** muy bueno, esto es lo que buscaba, lo probare en los scprits del cron.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
