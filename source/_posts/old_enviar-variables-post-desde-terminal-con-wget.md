---
title: (ES)Enviar variables post desde terminal con wget
date: 2013/07/27 11:25:42
layout: post
path: "/send-post-info-with-wget/"
category: "Imported from my older blog"
---
De vez en cuando hace falta enviar variables post a una determinada url para tener acceso a determinados datos o para que el servidor realice una acción concreta. Este paso de variables suele hacerlo el navegador de forma transparente al usuario, pero is quieres automatizar esto desde un script puedes hacerlo de forma fácil gracias a **wget** y su opción **\--post-data**. Se utiliza de este modo: 
    
    
    wget --post-data 'variableuno=valoruno&variable2=valor2' url

Para que veáis un ejemplo, aquí os dejo un pequeño script para publicar comentarios en wordpress de forma automatizada (siempre que no tengan captcha ni esas cosas): 
    
    
    #!/bin/bash
    
    # WPcommenter - by 023.es
    # Publica un comentario en un blog wordpress
    # $1= autor
    # $2= mail
    # $3= url
    # $4= comentario
    # $5= pagina donde publicarlo
    # $6= ID del post donde se publicará
    
    autor=$1
    mail=$2
    url=$3
    coment=$4
    pagina=$5
    postid=$6
    
    wget --post-data "author=$autor&email=$mail&url=$url&comment=$coment&comment_parent=0&submit=\"Publicar comentario\"&comment_post_ID=$postid" $pagina/wp-comments-post.php

Espero que sea de utilidad!

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
