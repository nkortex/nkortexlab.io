---
title: (ES)Como ir al cine gratis con SQL injection
date: 2016/06/25 15:26:53
layout: post
path: "/free-cinema-with-sql-injection/"
category: "Imported from my older blog"
---

El otro día estuve en el cine viendo Buscando a Dory, una película totalmente recomendable si te gustó Buscando a Nemo. El caso es que compramos las entradas por internet (5.90€ cada entrada). El bendito desarrollo tecnológico que nos permite reservar un asiento desde la comodidad de nuestro hogar. Un par de clicks, introduces tu información de pago y te mandan a tu correo un link con tu entrada. Este link es una url así: http://compraentradas.com/EntradaMail/373\[...]6133 y contiene un código de barras que posteriormente se escanea en el propio cine y se imprimen tus entradas: ![Captura de pantalla de 2016-06-25 16-40-48](/wp-content/uploads/2016/06/Captura-de-pantalla-de-2016-06-25-16-40-48.png) Pero... no hay ningún método de autenticación del usuario, por lo que cualquiera puede acceder a esa entrada si tiene el link. Investigando un poco veo que los ids de las entradas van aumentando pero no son consecutivas. Se podría probar con un ataque de fuerza bruta probando números hasta dar con una pelicula que nos convenza, en un cine cercano y en un horario que nos venga bien, imprimirla e ir al cine antes que su legítimo dueño. Una vez dentro procura sentarte en otro asiento por si van a buscarte. Pero como ya he dicho, estamos en el siglo XXI y la tecnología nos lo pone más fácil todavía. En compraentradas.com tienen un sistema que te permite recuperar tus entradas si las has perdido( http://www.compraentradas.com/CompruebaCompra ). Para ello te pide el cine y la fecha de compra como datos obligatorios y tu email, el código de entrada o el número del código de barras:   ![caption id="attachment_1052" align="aligncenter" width="917"](http://023.es/wp-content/uploads/2016/06/Captura-de-pantalla-de-2016-06-25-16-54-35.png) He perdido la entrada pero me sé el código de barras o la referencia. Si.

Esto te devuelve la entrada (o entradas (?) ) que coinciden con los criterios de búsqueda. Y si sabes como funciona puedes verlo como una máquina de entradas gratis.El caso es que esto huele a SQL injection desde aquí. Y no del difícil, basta con utilizar la palabra mágica ' or '1'='1  en el campo de código de barras o referencia. Por lo visto a alguien no le pareció importante filtrar esa entrada de datos. Ahora seleccionamos el cine al que queremos ir, como fecha de compra hoy, o hace pocos días y como código de barras todos los que hagan cumplir que 1=1: ![Captura de pantalla de 2016-06-25 17-02-38](http://023.es/wp-content/uploads/2016/06/Captura-de-pantalla-de-2016-06-25-17-02-38.png) Esa búsqueda, por ejemplo, devuelve 287 resultados. 287 entradas entre las que elegir. Las que tienen la etiqueta "CORRECTA" enlazan al código de barras listo para imprimir: ![Captura de pantalla de 2016-06-25 17-05-03](http://023.es/wp-content/uploads/2016/06/Captura-de-pantalla-de-2016-06-25-17-05-03.png) Me quedo con Star Wars. ![Captura de pantalla de 2016-06-25 17-10-58](http://023.es/wp-content/uploads/2016/06/Captura-de-pantalla-de-2016-06-25-17-10-58.png)   PD: Los de compraentradas están ya informados. Daos prisa o lo arreglarán :D EDIT: Este método ya no funciona, pero viendo la calidad de la plataforma probablemente encontréis más formas de atacar este sistema.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[platanito](#466 "2016-08-23 14:43:25"):** publica cosas cerdo joputa

**[admin](#467 "2016-08-23 14:48:15"):** Lo dice el que tiene un usuario hecho para publicar lo que quiera y aún no tiene ni una triste entrada xD



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
