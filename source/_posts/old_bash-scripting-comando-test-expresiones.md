---
title: (ES)(Bash scripting) Expresiones del comando test
date: 2014/01/27 21:40:28
layout: post
path: "/test-bash-expressions/"
category: "Imported from my older blog"
---
Cada vez que incluimos en nuestros scripts bash bloques if o estructuras while, les pasamos una expresión. Bash internamente procesa esta expresión con el comando test devolviendo 1 o 0 dependiendo si la expresión es cierta o no. ![](http://yaleherald.com/wp-content/uploads/2013/03/equalityJPEG.jpg) En esta entrada voy a recopilar las expresiones aceptadas por el comando test. Es interesante tenerlas a mano, pues en muchas ocasiones nos pueden evitar tener que programar a mano estas comprobaciones. 
    
    
    #!/bin/bash
    
    #023.es
    
    ###########
    # STRINGS #
    ###########
    
    #Devuelve 0 si la cadena no es nula
    test $string; echo $?       
    string="hola"
    test $string; echo $?
    
    #Devuelve 0 si la longitud del string es 0
    test -z $string; echo $?
    #Devuelve 0 si la longitud del string NO es 0
    test -n $string; echo $?
    
    string2="adios"
    #Devuelve 0 si los strings son iguales
    test $string = $string2	 ; echo $?
    
    ###########
    # ENTEROS #
    ###########
    
    int1=2
    int2=5
    
    #Devuelve 0 si los enteros son iguales
    test $int1 -eq $int2; echo $?
    #Devuelve 0 si los enteros NO son iguales
    test $int1 -ne $int2; echo $?
    #Devuelve 0 si int1 es menor estricto que int2
    test $int1 -lt $int2; echo $?
    #Devuelve 0 si int1 es menor o igual que int2
    test $int1 -le $int2; echo $?
    #Devuelve 0 si int1 es mayor estricto que int2
    test $int1 -gt $int2; echo $?
    #Devuelve 0 si int1 es mayor o igual que int2
    test $int1 -ge $int2; echo $?
    
    ############
    # FICHEROS #
    ############
    
    touch archivo
    touch archivo2
    
    #Devuelve 0 si archivo existe
    test -e archivo; echo $?
    #Devuelve 0 si archivo existe y el legible
    test -r archivo; echo $?
    #Devuelve 0 si archivo existe y se puede escribir en el
    test -w archivo; echo $?
    #Devuelve 0 si archivo existe y es ejecutable
    test -x archivo; echo $?
    #Devuelve 0 si archivo existe y es un archivo regular
    test -f archivo; echo $?
    #Devuelve 0 si archivo existe y es un directorio
    test -d archivo; echo $?
    #Devuelve 0 si archivo existe y es un dispositivo de caracteres
    test -c archivo; echo $?
    #Devuelve 0 si archivo existe y es un dispositivo de bloques
    test -b archivo; echo $?
    #Devuelve 0 si archivo existe y es un pipe
    test -p archivo; echo $?
    #Devuelve 0 si archivo existe y es socket
    test -S archivo; echo $?
    #Devuelve 0 si archivo existe y es un enlace blando (simbólico)
    test -L archivo; echo $?
    #Devuelve 0 si archivo existe y el propietario es el usuario actual
    test -O archivo; echo $?
    #Devuelve 0 si archivo existe y el propietario es el grupo actual
    test -G archivo; echo $?
    #Devuelve 0 si archivo existe y tiene activo el sticky bit
    test -k archivo; echo $?
    #Devuelve 0 si archivo existe y tiene un tamaño mayor que 0
    test -s archivo; echo $?
    #Devuelve 0 si archivo es más nuevo que archivo2 (fecha de modificación)
    test archivo -nt archivo2; echo $?
    #Devuelve 0 si archivo es más antiguo que archivo2 (fecha de modificación)
    test archivo -ot archivo2; echo $?

Además, la orden test admite el uso de operadores lógicos: 
    
    
    !       niega una expresión
    -a      operador and
    -o      operador or
    ()      Se permite la agrupación de expresiones con paréntesis

Estas expresiones se pueden evaluar tanto con la orden test como con los dobles corchetes: 
    
    
    $ [[ 3 -lt 9 ]]; echo $?
    0

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
