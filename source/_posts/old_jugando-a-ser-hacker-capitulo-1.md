---
title: (ES)Jugando a ser hacker. Capítulo 1
date: 2014/01/29 18:53:24
layout: post
path: "/hacking-1/"
category: "Imported from my older blog"
---
Me acabo de inventar un nuevo entretenimiento: publicar bromicas que se las podéis hacer a vuestros colegas o familiares que usan Linux pero que no se enteran mucho de la movida. En verdad no hacen nada malo en el sistema, sus efectos son reversibles y unas risas no te las quita nadie. Y hoy, con la primera entrega... EL CAGETIZADOR! *Música épica de fondo* ![](http://www.destructoid.com//ul/249331-NicCagePokemon.jpg) Programado en 15 minutos pero que os asegura horas de diversión. Aquí lo tenéis: 
    
    
    #!/bin/bash
    
    temp_d=$(mktemp -d)
    echo "" > ~/.caged.log
    cd $temp_d
    wget "http://023.es/Nicolas_Cage_Faces.zip" -q -O NCF.zip #Descarga el pastel
    find ~ -type d -maxdepth 3 -mindepth 1 > directorios	#Lista los directorios objetivo
    
    unzip NCF.zip	2> /dev/null > /dev/null		#Descomprime
    
    contador=0
    while read dire ;do
    	((contador++))
    	imagen="$((($contador%22)+1)).jpg"
    	if [[ ! -e $dire/$imagen ]] ; then     #Se asegura de que no exista
    		cp -n "$imagen" "$dire/$imagen"			#Copia la imagen en el directorio
    		echo $dire/$imagen >> ~/.caged.log	        #Guarda log por si se enfadan
    	fi
    done < directorios
    
    exit 0
    
    #Para borrarlo todo:
    while read r; do 
    	rm -f "$r"; 
    done < ~/.caged.log
    rm -f ~/.caged.log

Si lo leéis un poco por encima veréis que se descarga 22 fotos de Nicolas Cage y las distribuye aleatoriamente por la carpeta home de la víctima. Siendo una broma no queremos causar destrozos, por eso se guardan las modificaciones en un log y se asegura que no se sobrescriba ningún archivo con la opción -n de cp. Descarga:[ http://023.es/scripts/caged.sh](/scripts/caged.sh) Sed buenos ;D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
