---
title: (ES)(Bash Scripting) Acortar urls con Adf.ly
date: 2013/06/22 15:09:57
---

![](http://4.bp.blogspot.com/-Xj_Nl74v9vA/UWrREysz7SI/AAAAAAAAAFI/uFyHPx5CJTs/s1600/adfly.png) El script que os traigo hoy sirve de acortador de url con Adf.ly. Una forma fácil de ganarse un dinerillo. Podéis usarlo para vuestros scripts. Funciona pasandole una o varias URLs por parámetro, pero antes hay que configurar las variables con vuestra key y vuestro id de usuario que os proporciona Adf.ly. 
    
    
    #!/bin/bash
    
    #Se le pasa como argumento una o varias urls y las acorta con adf.ly
    
    #Variables a cambiar:
    
    key=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa       #adf.ly key
    uid=00000                                  #adf.ly uid
    tipo=int                                   #tipo de anuncio: int o banner
    dominio=adf.ly                             #dominio resultante. Puede ser adf.ly o q.gs
    
    for url in $@ 
    do
    	curl "http://api.adf.ly/api.php?key=$key&uid=$uid&advert_type=$tipo&domain=$dominio&url=$url"
    done

  Como veis, más simple no puede ser. Si lo queréis cambiar para que acepte más parámetros o para que tenga más opciones, no dudéis en compartirlo.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Pablo Robles](#336 "2014-03-27 16:38:14"):** Hola amigo, oye podrias hacer algo asi pero en lenguaje php. gracias!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
