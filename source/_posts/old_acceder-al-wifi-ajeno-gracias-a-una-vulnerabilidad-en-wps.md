---
title: (ES)Acceder al Wifi ajeno gracias a una vulnerabilidad en WPS
date: 2013/02/27 22:31:21
layout: post
path: "/wps-vulnerabilidad-wifi/"
category: "Imported from my older blog"
---

(Tutorial realizado para la auditoria de tu propia red wifi. Úsalo solamente para comprobar si tu red es segura. Entrar en redes wifi ajenas es delito)
Reaver es un programita que se aprovecha de una vulnerabilidad en el protocolo WPS para acceder a un router sin permiso. Antes de pasar a la práctica, un poquito de teoría: El protocolo WPS proporciona un método fácil de conectar dos dispositivos wifi sin tener que recordar largas contraseñas. Con solo un pin de 8 cifras, podrás estar conectado de forma segura (?) e indolora a tu router. Este pin esta escrito en la pegatina del router y al  tener 8 cifras nos deja con 10⁸ posibles combinaciones ( cien millones). Probar con fuerza bruta 100.000.000 de combinaciones es demasiado; pero aquí es donde hacemos la magia. Unos señores muy listos se dieron cuenta de que una vez introducidos los 4 primeros números, el AP ya avisa de si son correctos. Además, el último número es un simple checksum (suma de verificación). Por lo tanto, esto nos deja con 14⁴ + 10³ (11.000 posibilidades) probando primero con los 4 primeros números y, una vez que los tengamos, con los 3 últimos. Cabe decir que este método no funciona en todos los modelos de router, porque algunos simplemente se bloquean tras un número x de intentos. Pero vale la pena asegurarse. Manos a la obra. Nos bajamos el Reaver desde aquí: <http://code.google.com/p/reaver-wps/downloads/list> (última versión). Descomprimimos, ./configure , make y make install. Lo típico. Ahora ponemos nuestra tarjeta en modo monitor (promiscuo): 
    
    
    airmon-ng start wlan0

Con Wash vemos las redes cercanas qu tienen el WPS activado: `wash -i mon0` (cambiando mon0 por la interfaz que os haya dado airmon) Anotamos el BBSID de la red victima (Que debe ser la tuya, recuerda que este tutorial no está hecho para entrar en la wifi del vecino) y empezamos el ataque con reaver: 
    
    
    reaver -i mon0 -b 00:11:22:33:44:55 -vv

Y en unas cuantas horillas lo tienes :D Aclaraciones: Las redes WLAN_XXX (routers Observa Telecom con Mac 00:19:15:XX:XX:XX) y los livebox de orange traen de pin por defecto el 12345670. Por eso reaver prueba ese pin el primero. Algunos routers detectan los ataques e imponen limitaciones de tiempo. Todo es esperar. Desactiva esta opción en tu router si no quieres tener invasores indeseados. Este ataque es rastreable, así que, si lo ves oportuno, deberías cambiar tu mac por otra a voleo con macchanger. Si tienes el pin de una determinada red, pero la contraseña la han cambiado, el código a ejecutar es: 
    
    
    reaver -i mon0 -b 00:11:22:33:44:55 -p 12345670 -vv

De este modo no tienes que esperar a conseguir otra vez el pin para entrar. Sed buenos!

<!-- PELICAN_END_SUMMARY -->
## Comments

**[david](#104 "2013-06-21 12:05:57"):** Oye una pregunta. Yo me preguntaba si se podria con un programa activar el wps. de un router y conectarte al router ese q el prograna iso activar el wps seria posible?? Si puedes ayudame porfavor?? Muchas gracias asta luego

**[admin](#105 "2013-06-21 12:12:54"):** Hola David, No se si he entendido bien lo que quieres hacer. ¿Quieres ejecutar WPS desde un router sin usar un PC?

**[carlos](#144 "2013-07-25 17:33:33"):** Hola, tengo desabilitado el wps, mi pregunta es si alguien puede activar de nuevo el wps en mi router sin que yo me de cuenta.

**[admin](#145 "2013-07-25 19:52:46"):** Hola Carlos. Nadie puede activarlo a no ser que tengan acceso a tu red y sepan la clave del router. Y si de puede no se como hacerlo

**[maix](#316 "2014-01-30 10:43:50"):** como me conecto al otro router una vez obtenido el pin?

**[admin](#318 "2014-01-30 10:46:35"):** Hola maix, Reaver te devuelve el pin y la clave. Tienes que conectarte normalmente e introducir esa pass. Un saludo

**[nigromant](#434 "2015-12-01 13:00:16"):** hola si se puede activar un router ajeno a distancia, solo tienes que tener las herramientas necesarias , nos vemos, suerte !!! ;-)

**[Gerardo Cortes Suárez](#435 "2015-12-04 18:54:16"):** Hola, estou haciendo un trabajo de seguridad wifi y tengo que demostrar que es posible obtener el pin de una red con wps activado... El problema que el router que tengo lo bloquea al cabo de x intentos. Sabes si hay alguna manera de realizar la prueba? Me valdria reiniciar el router cada vez que se bloquee?



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
