---
title: (ES)(Bash scripting)Recordar un comando olvidado
date: 2013/10/02 18:37:50
---
![](http://www.mecambioamac.com/wp-content/uploads/2010/07/512-Terminal.png) Leo en [misapuntes](http://misapuntes.corta.la/encontrar-un-comando-del-que-no-me-acuerdo/) un tip muy útil para esos momentos en los que no recuerdas exactamente el nombre de un comando pero sabes lo que hace y necesitas que lo haga en ese momento. En estos casos podemos usar el comando **apropos**. El uso sería el siguiente: 
    
    
    apropos <pista>

Siendo pista cualquier cosa que recordemos de ese programa misterioso. Por ejemplo, si no recuerdo el comando **wget**, puedo ejecutar esto: 
    
    
    apropos download

Lo que me devuelve: 
    
    
    arm2hpdl (1) - Add HP download header/trailer to an ARM ELF binary.
    git-fetch (1) - Download objects and refs from another repository
    git-http-fetch (1) - Download from a remote git repository via HTTP
    hp-firmware (1) - Firmware Download Utility
    hp-plugin (1) - Plugin Download and Install Utility
    smbget (1) - wget-like utility for download files over SMB
    update-pciids (8) - download new version of the PCI ID list
    update-usbids (8) - download new version of the USB ID list
    wget (1) - The non-interactive network downloader.
    youtube-dl (1) - download videos from youtube.com or other video platforms

Y lo recordaría instantáneamente. El funcionamiento es sencillo: Busca la pista dada en las cabeceras de los manuales de todos los programas disponibles y muestra las coincidencias. Espero que sea útil

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
