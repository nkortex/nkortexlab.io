---
title: (ES)Notificación por mail o SMS cuando se descarga un torrent
date: 2013/01/20 20:40:46
layout: post
path: "/mail-notify-when-torrent-end/"
category: "Imported from my older blog"
---

En este tutorial (muy útil si usas tu RaspberryPi como cliente torrent) te voy a enseñar a configurar Transmission para que te avise por correo o por SMS cuando acabe de descargar un torrent. Antes de nada, necesitas tener instalado el Transmission y una cuenta en IFTTT.com. Si está todo, empezamos instalando el programa mutt: 
    
    
    sudo yum -y install mutt

Y lo configuramos para el usuario que lo va a usar, creando el archivo de configuración: 
    
    
    gedit /home/tuusuario/.muttrc

Y dentro de esta archivo pegamos: 
    
    
    set from = "USER@gmail.com"
    set realname = "023.es mola"
    set imap_user = "USER@gmail.com"
    set imap_pass = "CLAVE"
    set folder = "imaps://imap.gmail.com:993"
    set spoolfile = "+INBOX"
    set postponed ="+[Gmail]/Drafts"
    set header_cache =~/.mutt/cache/headers
    set message_cachedir =~/.mutt/cache/bodies
    set certificate_file =~/.mutt/certificates
    set smtp_url = "smtp://USER@smtp.gmail.com:587/"
    set smtp_pass = "CLAVE"

Cambiando los campos en mayúscula por tu usuario y contraseña. Luego creamos el directorio donde guardará sus cosas que le hacen falta para funcionar: 
    
    
     mkdir -p /home/tuusuario/.mutt/cache

Y ya tenemos el programa preparado para mandar mensajes, usando el siguiente comando: 
    
    
    echo "Hola mundo!! desde GMail" | mutt -s "023.es" destinatario@gmail.com

Esto de por si ya es útil, pero vamos  añadirle más utilidad haciendo que nos mande un mensaje cada vez que se acabe de descargar un Torrent. Abrimos un nuevo documento vacío con gedit que se llame, por ejemplo, aviso_torrent y que contenga lo siguiente: 
    
    
    #!/bin/bash
    echo "$TR_TORRENT_NAME descargado ok" | mutt -s "Nuevo Torrent Descargado" destinatario@gmail.com

Lo guardamos en la carpeta que queramos. Luego, para que Transmission nos avise, editamos el archivo ~/.config/transmission/settings.json y configuramos la línea "script-torrent-done-filename" cambiandola por el archivo que acabamos de crear. Así: 
    
    
      "script-torrent-done-filename": "carpeta/donde/este/el/archivo/aviso_torrent $TR_TORRENT$/$TR_TORRENT_NAME",

Recuerda darle permisos de ejecución a ese archivo, sino no funcionará. Ok, ahora nos falta lo interesante de verdad, que nos avise por SMS. Para esto, necesitáis una cuenta en IFTTT.com . He creado una receta que se activa cada vez que os llega un correo con el asunto "Nuevo Torrent Descargado" y os manda un SMS a vuestro móvil. Sólo tenéis que activarla desde aquí: <https://ifttt.com/recipes/75431> Espero que os sirva el tutorial, si tenéis algún problema o alguna posible mejora, dejad un comentario :D (La mayor parte de la información de este post fué sacada de el blog [rsppi.blogspot.com.es](http://rsppi.blogspot.com.es/2013/01/envio-de-emails-desde-consola-y-con.html) de muy recomendada lectura)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Facundo](#30 "2013-04-10 19:05:23"):** Buen dia, gracias por el tutorial, es el mismo que vi en rsppi.blogspot.net, salvo el del sms. Tengo un problema con la variable TR_TORRENT_NAME.. se envia vacia !! que puede ser? gracias !!

**[admin](#31 "2013-04-10 19:12:11"):** Buenos días Facundo, gracias por tu visita! Puede ser porque hayas configurado mal el archivo ~/.config/transmission/settings.json ? Mira a ver si has añadido esta linea correctamente: "script-torrent-done-filename": "carpeta/donde/este/el/archivo/aviso_torrent $TR_TORRENT$/$TR_TORRENT_NAME", Incluidas las 2 variables del final

**[jorge](#205 "2013-08-12 20:47:00"):** Buenas noches admin. Me ha parecido muy interesante tu blog. He ido a probar lo de IFTTT y no consigo que me llegue el pin al móvil para empezar a utilizar el servicio. ¿Sabes qué prefijo hay que utilizar para España? Yo probé poniendo 0034 y mi número justo después pero nada. Muchas gracias y sigue así. Un saludo.

**[admin](#208 "2013-08-14 15:37:26"):** Pues a decir verdad hace ya bastante tiempo que no me funciona esta función de IFTTT. Puede ser que tengan problemas con el envío de SMS

**[jorge](#209 "2013-08-14 16:15:14"):** Gracias, pensaba que era yo que estaba haciendo algo mal. Un saludo.

**[anon](#321 "2014-02-09 21:28:15"):** Gracias por tu web me sirvio de mucha ayuda el tutorial sobre yowsup oye podrías hacer un tuto donde enviara un whatsaap al terminar de descargar un torrent

**[admin](#322 "2014-02-10 10:56:15"):** Hola, gracias por tu comentario. Me alegro de que te haya sido de utilidad. No creo que sea difícil adaptar el script para que haga lo que pides; solo hay que llamar a yousup en vez de a mutt con el mensaje que quieres que mande, inténtalo y me publicas un comentario con tu solución. Si no te funciona intento ayudarte ;) Un saludo!

**[Ricardo](#328 "2014-03-15 18:39:48"):** Hola admin, disculpa que te escribi antes por privado, no era mi intencion, simplemente estaba en una pagina que no veia los comentarios, la cosa es que no podia enviar mensajes desde el script creado, pero si manualmente desde linea de comando, la solucion la he encontrado dando vueltas por google y consiste en modificar el /etc/init.d/transmission-daemon y cambiar el user con que se ejecuta el transmission por root. Asi lo hice y funciona perfectamente, aunque no se si usarlo como root sea la solucion mas elegante. Tenia idea de probar con user pi y ver que pasa, pero creo que hay que cambiar varias cosas mas para ello. Bien, eso es todo y gracias por tu blog, es muy bueno.

**[admin](#330 "2014-03-17 09:08:33"):** Hola Ricardo, sin ningún problema. Prefiero responder a las dudas generales por aquí para poder ayudar al máximo de gente posible y dejar el contacto para temas más privados. Tienes que tener cuidado del usuario que ejecuta el script concreto; efectivamente ejecutar cualquier cosa con privilegios root sin que sea estrictamente necesario es bastante peligroso. Prueba a poner en alguna parte del script algo parecido a esto: whoami >> /tmp/quiensoy Luego, abres el fichero quiensoy y te dirá el usuario que ejecuta el script. Todos los archivos de configuración de mutt tendrán que estar basados en este user. Gracias por tu visita, cuando tengas más datos no dudes en responder ;)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
