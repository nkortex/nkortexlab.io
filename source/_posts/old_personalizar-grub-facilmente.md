---
title: (ES)Personalizar GRUB facilmente
date: 2013/01/20 12:06:35
layout: post
path: "/personalice-grub/"
category: "Imported from my older blog"
---

**Grub Customizer** es una práctica herramienta que te permite **personalizar** todos los aspectos del **grub**. Si quieres tener un PC completamente personalizado,  te enseño a usar esta simple pero potente herramienta para personalizar el **arranque** de tu **equipo**. **Instalación** **Fedora** Si usas Fedora, bájate el paquete correspondiente a tu arquitectura: 

[Versión .rpm i686 (32bits)](http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/releases/17/Everything/i386/os/grub-customizer-2.5.6-1.fc17.R.i686.rpm)  [Versión .rpm x86_64 (64bits)](http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/releases/17/Everything/x86_64/os/grub-customizer-2.5.6-1.fc17.R.x86_64.rpm)

**Ubuntu** Si tu distribución es Ubuntu podrás instalarlo a través de su repositorio escribiendo estos 3 comandos en la consola: 
    
    
    sudo add-apt-repository ppa:danielrichter2007/grub-customizer
    sudo apt-get update
    sudo apt-get install grub-customizer

**Uso** Una vez instalado, lo abrimos (puedes encontrarlo en Herramientas del sistema), ponemos nuestra contraseña para permitir modificaciones en el sistema y accedemos a todas las opciones que nos da el programa. Recomendamos que sólo cambiéis cosas si estáis totalmente seguros de lo que hacéis; de lo contrario podréis dejar vuestro ordenador inutilizable. Las opciones más comunes a modificar las podéis encontrar pulsando el botón Preferencias. Allí podréis cambiar el tiempo de espera hasta que cargue automáticamente el sistema por defecto; qué sistema por defecto queréis que os marque; la imagen de fondo y los colores del texto,... ![](http://2.bp.blogspot.com/-Re27PabGa6Y/T_h_tag1KDI/AAAAAAAACZ8/9JFgar1Nwig/s640/Grub-customizer0-500x321.jpg) Una vez que hayáis cambiado todo y lo hayáis dejado a vuestro gusto, pulsáis cerrar, luego guardar y, por último, instalar en MBR. Hecho esto ya puedes reiniciar y ver como ha quedado tu nuevo Grub. (Aportación mía a UsemosLinux)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Diego](#83 "2013-06-04 17:48:12"):** Recién instalo Fedora 18, soy lo más inexperto que te puedas imaginar en el uso de Linux... pero bueno, estoy tratando de istalar Grub Customizer, baje el correspondiente a mi SO Fedora/32bits. Veo un rápido proceso de instalación pero después no encuentro el programa por ningún lado, busque en todas las aplicaciones, uso el buscador del escritorio y nada... Si tenes un tiempo dale una mano a este neófito de linux. Gracias.

**[admin](#84 "2013-06-04 19:21:03"):** Hola Diego! Antes de nada, me alegro de que te adentres en el mundo de linux con Fedora en vez de con Ubuntu o derivados como hace otra gente. Aunque te puedan surgir algunos problemas, esto hará que aprendas mucho más. Para tu contratiempo se me ocurren 2 causas posibles: La primera es que, si lo has instalado mediante interfaz gráfica (dándole doble click) puede haber algún problema que no te muestre. Si te pasa esto en otra ocasión, prueba a instalarlo mediante consola con el comando: sudo yum localinstall instalador.rpm La segunda, y más probable, es que el instalador no sea válido para Fedora 18, pues estaba pensado para Fedora 17. Prueba a instalar este otro: http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/releases/18/Everything/i386/os/grub-customizer-2.5.6-1.fc18.R.i686.rpm Cualquier cosa me dices y lo intentamos solucionar. Muchas gracias por tu visita y tu comentario, espero verte más por aquí ;)

**[Nelson Martell](#284 "2013-11-23 04:39:17"):** Hola. Logré instalarlo en Fedora 19, pero no me carga la imagen que seleccione como fondo, aunque sí me cambia los colores del texto. De fondo sólo me aparece en negro. :( ¿Sabes a qué se deba esto?

**[admin](#287 "2013-11-25 19:03:52"):** Puede ser por un formato incorrecto, que no encuentre la imagen, tema de permisos o resolución no soportada. Prueba a cambiar todo esto a ver si te funciona. Gracias por tu visita ;)

**[IvanLinux](#298 "2013-12-20 05:46:03"):** Gracias x el aporte! =D Saludos! Atte.: IvanLinux



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
