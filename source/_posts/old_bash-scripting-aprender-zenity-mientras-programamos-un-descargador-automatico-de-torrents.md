---
title: (ES)(Bash Scripting) Aprender zenity mientras programamos un descargador automático de torrents
date: 2013/02/20 13:18:09
---

Hoy vamos a aprender a hacer nuestros scripts bash más vistosos, implementándoles una pseudo-interfaz de usuario mediante **zenity**. Este programa nos proporciona diálogos muy variados: mensaje de advertencia, barra de progreso, entrada de texto, selección de color, calendario,... y su uso es muy simple. 

![](http://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/The_Pirate_Bay_logo_bw.svg/529px-The_Pirate_Bay_logo_bw.svg.png)

Para ilustraros como se hace, voy a poneros de ejemplo un descargador de torrents que acabo de programar. Se basa en transmission, The Pirate Bay y zenity. Está comentado para que lo podáis entender (muy penco todo, pero ese no es el objetivo, el objetivo es aprender **zenity**). Aquí tenéis el código: <http://023.es/scripts/LuckyBay.sh> Para que funcione correctamente, debéis meter el archivo "categorias.txt" en el mismo directorio. Este archivo lo tenéis aquí: <http://023.es/scripts/categorias.txt> Como veis, hago unos pasos muy sencillos: 

  * Borro posibles archivos que quedaran de una ejecución anterior
  * Pregunto al usuario lo que quiere bajar y si desea que sea automático o manual
  * Si quiere búsqueda manual, le pregunto por la categoría (Esta parte hay que traducirla y mejorarla)
  * Descargo la información de ThePirateBay y la organizo en una mini-base de datos. Para tener más clara esta parte os recomiendo que echéis un ojo al código
  * En caso de selección manual muestro una lista de resultados ordenados por numero de Seeders
  * Sino, descargo automáticamente el que más Seeders tenga (Normalmente, el que más seeders tiene, tiene menos posibilidades de ser Fake)
  * Paso el enlace magnet a Transmission por medio de la interfaz remota (Tiene que estar activada)
  * Borro los archivos utilizados
Una vez que ya sabemos como funciona el script vamos a lo que íbamos  explicar el uso de **zenity**. Para saber que quiere descargar el usuario uso el diálogo \--entry de este modo: 
    
    
    descargar=$(zenity --entry --text="Introduce la película/serie/juego/disco que quieres descargar (Recuerda que solo funciona si tienes el Transmission abierto y la interfaz web activa)")

La única opción que le paso es \--text, que cambia el texto mostrado al usuario. El resultado se guarda en la variable $descargar para su uso posterior. Luego pregunto si quiere obtener el archivo automáticamente o seleccionarlo de una lista. Para ello utilizo el diálogo \--question así: 
    
    
    zenity --question --text="Quieres seleccionar el archivo a mano o te fias de mi criterio?" --ok-label "Voy a tener suerte" --cancel-label "Buscar manualmente"

Y además de la opción \--text que ya he explicado utilizo dos opciones más: \--ok-label y \--cancel-label que me permiten modificar el texto de los botones aceptar y cancelar respectivamente. Así los puedo utilizar para otras cosas y no solo "Acceptar" o "Cancelar" Ahora viene lo más difícil: el diálogo \--list . Lo utilizo en dos partes del código: para elegir las categorías a buscar y para mostrar los resultados. Voy a explicar solo el primero y el segundo os lo dejo de deberes para casa: 
    
    
    categoria=$(zenity --list --width=400 --height=500 --checklist --separator="," --print-column=3 --title="Elige la categoría del Torrent" --column="Selec." --column="Categoría" --column="ID" $(cat categorias.txt))

En este diálogo uso más opciones: 

  * \--title: Cambia el título de la ventana
  * \--width: Cambia la anchura
  * \--height: Cambia la altura
  * \--checklist: Indica que la primera columna va a estar reservada por los "checkdots". De este modo podemos seleccionar más de una categoría.
  * \--separator: Cuando se seleccionan mas de 1 elementos, este campo indica con que caracter se separarán antes de ser devueltos (en este caso una coma)
  * \--print-column: columna que se devolverá. En nuestro caso necesitamos solamente el ID de la categoría así que devolvemos la tercera. Para devolver todas, se puede usar "all"
  * \--column: Titulo de cada columna, por orden. Hay que poner uno por cada columna.
  * Por último hacemos un cat del archivo donde tenemos los datos ordenados.
Resultado: ![Captura de pantalla de 2013-02-20 14:13:58](/wp-content/uploads/2013/02/Captura-de-pantalla-de-2013-02-20-141358.png)   En la página de Gnome tenéis un tutorial bastante resultón en el que se explican todas las opciones, pero no os extrañéis de que no funcionen los ejemplos, pues algunos están mal. Podéis encontrarlo aquí: <http://help.gnome.org/users/zenity/stable/index.html.es>

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Wuilmer Bolivar](#444 "2016-01-31 04:10:26"):** Con las opciones: –ok-label y –cancel-label puedes modificar los botones aceptar y cancelar, sin embargo es posible desactivar/eliminar el botón "cancelar"?

**[admin](#445 "2016-01-31 10:07:03"):** Si, en vez de pasarle la opción --question, usa el parámetro --info que solo muestra un botón.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
