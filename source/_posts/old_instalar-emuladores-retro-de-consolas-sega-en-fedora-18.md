---
title: (ES)Instalar emuladores retro de consolas SEGA en Fedora 18
date: 2013/01/19 10:42:31
layout: post
path: "/retro-emulators-fedora/"
category: "Imported from my older blog"
---

Los repositorios RPM Fusion tienen muchísimos tesoros ocultos, entre ellos una buena colección de emuladores de videoconsolas antiguas. Las cásicas SEGA que iban a cartucho. Para que podáis instalarlos, debéis tener añadidos los repositorios RPM Fusion. Expliqué como se hace en el post anterior. ![](http://es.globedia.com/imagenes/noticias/2012/7/1/sega-cierra-oficinas-paises_1_1279820.jpg) Sega Master System(Instálate solo uno de los 2, valen para lo mismo): 
    
    
     yum -y install dega-sdl
    
    
     yum -y install osmose

Sega Genesis: 
    
    
    yum -y install degen-sdl

Sega MegaDrive/MegaCD: 
    
    
     yum -y install gens

Sega Saturn: 
    
    
     yum -y install yabause

Sega Dreamcast: 
    
    
    yum -y install lxdream

Ni que decir tiene, que tenéis que ejecutarlos como root, ni que decir tiene. Para bajaros las roms de los juegos, hay mil páginas. La que más me gusta: [http://www.freeroms.com](http://www.freeroms.com/) Si sabéis otra mejor, decidlo xD

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Haeslitt](#76 "2013-05-20 23:35:45"):** Ya insale Lxdream y tambien descargue el jueg de freeroms o ya lo descomprimi con krusader el problema ahora es como meterlo al emulador el juego no sabria si hay que poner un formato de imagen o como ejecutas los juegos dentro del emulador. al descomprimir el juego me genera dos archivos archivo .gdi y dos archivos .bin pero como lo cargo el emulador me dice que necesita una imagen.dst que hacer en este caso... un saludo espero tu respuesta

**[admin](#78 "2013-05-23 16:14:45"):** Hola Haeslitt. Le has instalado la bios?



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
