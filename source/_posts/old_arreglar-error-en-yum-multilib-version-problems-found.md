---
title: (ES)Arreglar error en yum "Multilib version problems found"
date: 2013/06/07 15:02:53
layout: post
path: "/multilib-problem-fix-yum/"
category: "Imported from my older blog"
---
![](http://terminaltolinux.files.wordpress.com/2012/06/yum01.png) Cuando estás actualizando tu distribución con yum o instalando nuevos paquetes y se interrumpe el proceso (Intencionadamente o sin querer) es probable que al intentar seguir con lo que estabas haciendo, yum te de un error parecido a este: 
    
    
    Error:  Multilib version problems found.

Para arreglarlo basta ejecutar: 
    
    
    sudo yum-complete-transaction
    package-cleanup --cleandupes

Una vez hecho esto, prueba a volver a instalar o a acualizar. :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
