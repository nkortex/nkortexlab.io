---
title: (ES)iso2grub Instala de forma automática isos en el grub de Linux
date: 2014/01/29 10:38:08
layout: post
path: "/iso2grub/"
category: "Imported from my older blog"
---

**Actualización** al final del post Si estáis tan hartos como yo de querer probar 1000 distribuciones y no hacerlo por la pereza que da pasarlas a un pen booteable (primero hay que encontrar uno), quemarlas a un disco o peor aun, instalarlas en su propia partición. Pero hoy traigo la solución, un script de fabricación propia que recibe por parámetro un archivo iso y le agrega una entrada en el grub. Al reiniciar, si seleccionas esa entrada, cargará la iso y podrás utilizar el sistema de una forma más fluida que si lo leyera de un DVD. Pide permisos de root, pues son necesarios para modificar el archivo de configuración de grub. Si no os fiáis, mirad el código ;P Su uso es: 
    
    
    iso2grub.sh archivo.iso md5_opcional

También necesitáis saber la ubicación de dos archivos: vmlinuz y initrd. Este último suele tener extensión .lz, .gz o .xz . Para averiguar esta información tenéis que abrir la iso con el gestor de archivadores (o montarla en vuestro sistema) y navegar por los directorios hasta encontrarlos. Una vez los encontráis, le pasáis al programa la ruta relativa al punto de montaje. Por ejemplo: Si montáis la imagen en /usr/iso y el archivo vmlinuz se encuentra en la carpeta boot, solo tenéis que escribir /boot/vmlinuz, no /usr/iso/boot/vmlinuz. Si tenéis dificultades con esto comentadlo en el post y os ayudo (tengo que automatizar la búsqueda de estos archivos, pero no tengo tiempo) 
    
    
    #!/bin/bash
    
    #Instala una nueva entrada en el menú de grub2 para
    #la imagen que se pasa como parámetro, el md5 es opcional
    
    #Distribuido con la mejor intención pero sin ninguna garantía
    #Tranquilos, no maneja archivos importantes, nunca dejará el sistema inutilizado
    #Pero es posible que no funcione con todas las distros
    #By: http://023.es
    
    #Comprueba la cantidad de parámetros
    if [[ $# < 1 ]] ; then 
    echo "Número de argumentos incorrecto. Uso: $0 archivo.iso [md5]" >&2
    exit 1
    fi
    
    #Comprueba si se tienen permisos de root
    if [[ $(whoami) != 'root' ]] ; then 
    echo "Permisos insuficientes. Ejecutar como root" >&2
    exit 1
    fi
    
    #Comprueba la integridad de los datos 
    if [[ $# == 2 ]] ; then 
    echo "Comprobando suma de verificación..."
    mdcinco=$(md5sum $1 | cut -d " " -f1)
    if [[ "$mdcinco" != "$2" ]] ; then
    echo "Suma de comprobación incorrecta. Imagen corrupta o md5sum no instalado" >&2
    echo "Proporcionado $2" >&2
    echo "Calculado $mdcinco" >&2
    exit 1
    fi
    echo -e "Suma de comprobación correcta \033[0;32m \xE2\x9c\x93"
    fi
    
    #Comprueba si existe el archivo de configuración de grub2 necesario
    if [[ ! -w '/etc/grub.d/40_custom' ]] ; then
    echo "Archivo de configuración no encontrado. Grub instalado?" >&2
    error
    fi
    
    #Mendigando un reporte de error
    function error
    {
    echo -e "Iso no soportada, por favor deja un comentario en esta entrada:\nhttp://023.es/iso2grub-instala-de-forma-automatica-isos-en-el-grub-de-linux \ncon el nombre del sistema que intentabas montar (y la versión) y el error que te ha dado. Gracias! :D"
    exit 1
    }
    
    #Detecta el dispositivo donde se encuentra el iso y el punto de montaje, así como otros datos necesarios
    ruta_absoluta=$(readlink -f $1)
    p_montaje=$(df $1 --output=target | tail -n1)
    dispositivo=$(df $1 --output=source | tail -n1)
    
    letra_disco=$(echo $dispositivo | cut -b 8)
    case $letra_disco in
    a) num_disco=0;;
    b) num_disco=1;;
    c) num_disco=2;;
    d) num_disco=3;;
    e) num_disco=4;;
    *) exit 1;;
    esac
    
    particion=$(echo $dispositivo | cut -b 9-)
    iso_scan=${ruta_absoluta##$p_montaje}
    
    #TODO: buscar archivos automáticamente
    read -p "Dentro de la iso, donde se encuentra el archivo vmlinuz? (busca en /boot o /casper)" vmlinuz
    read -p "Dentro de la iso, donde se encuentra el archivo initrd? (extension: .xz , .gz o .lz normalmente)" initrd
    read -p "Titulo que se mostrará en el Grub?" titulo
    
    #Escribe la información de la nueva entrada en el archivo 40_custom
    echo "menuentry \"$titulo\" {
    set root=(hd$num_disco,$particion)
    loopback loop $iso_scan
    linux (loop)$vmlinuz boot=casper iso-scan/filename=$iso_scan --
    initrd (loop)$initrd
    }" >> /etc/grub.d/40_custom
    
    #Actualiza grub
    update-grub > /dev/null 2> /dev/null
    if [[ $? -ne 0 ]]; then 
    grub2-mkconfig -o /boot/grub2/grub.cfg > /dev/null 2> /dev/null
    if [[ $? -eq 0 ]]; then echo -e "Correcto, prueba a reiniciar \033[0;32m \xE2\x9c\x93" ; else echo "Error en update-grub o grub2-mkconfig" >&2 ; error; fi
    else
    echo -e "Correcto, prueba a reiniciar \033[0;32m \xE2\x9c\x93" 
    fi

Podéis descargarlo de aquí: <http://023.es/iso2grub.sh> Cabe destacar que esto solo funciona si la imagen está preparada para funcionar en live (Al igual que cuando instalas en un USB) y que si movéis la imagen de sitio, dejará de funcionar. Comentar nunca está mal visto ;) EDITO: parece que es más difícil adecuar el script a cada distribución, pues cada una se estructura de una manera y ahora, de exámenes no tengo tiempo de verlo. De todos modos lo dejo aquí para que quien quiera lo mejore o lo utilice. Por ahora lo he probado con wifislax 4.8-RC1 y con easypeasy y funciona.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Juangmuriel](#448 "2016-03-10 20:29:05"):** Es una idea muy buena para los aficcinados al distrohopping.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
