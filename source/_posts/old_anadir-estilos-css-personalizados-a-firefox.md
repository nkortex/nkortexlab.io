---
title: (ES)Añadir estilos css personalizados a firefox
date: 2016/02/17 19:14:21
layout: post
path: "/personalize-style-firefox/"
category: "Imported from my older blog"
---
El principal motivo por el que todavía utilizo Google Chrome en vez de Firefox como navegador predeterminado es porque tenía un error que me desquiciaba, y es que el texto que introduzco en los text-input de determinadas páginas (sobre todo Youtube) me aparecía en blanco, al igual que el fondo, lo que hacía que no pudiese ver lo que estaba escribiendo a no ser que seleccionase el texto, y esto se volvía bastante molesto. Creo que tiene que ver con mi tema de GTK, pero por más vueltas que le di no conseguí solucionarlo, por lo que acabé usando Chrome por defecto. Hoy he descubierto un truco para añadir estilos CSS personalizados que se cargarán en todas las páginas que visitemos. Para ello solo tenemos que localizar el archivo `~/.mozilla/firefox/xxxxxxxx.default/chrome/userContent.css`. Si no existe lo creamos y si hay más de una carpeta `xxxxxxxx.default` podemos mirar en el archivo profiles.ini cual es la asociada a nuestro perfil. Una vez lo tenemos localizado, insertamos los estilos personalizados que queramos. En mi caso serán: 
    
    
    input:not(.urlbar-input):not(.textbox-input):not(.form-control):not([type='checkbox']) {
    -moz-appearance: none !important;
    background-color: white;
    color: black;
    }
    
    #downloads-indicator-counter {
    color: white;
    }
    
    textarea {
    -moz-appearance: none !important;
    background-color: white;
    color: black;
    }
    
    select {
    -moz-appearance: none !important;
    background-color: white;
    color: black;
    }
    

![Captura de pantalla de 2016-02-17 20-01-23](/wp-content/uploads/2016/02/Captura-de-pantalla-de-2016-02-17-20-01-23.png) Además, también se pueden modificar los colores de la barra de marcadores y demás elementos de la interfaz de Firefox: 
    
    
    #PersonalToolbar { color: black; }   /* Barra de favoritos*/
    #searchbar { color: black; }    /* Barra de búsqueda*/
    #main-menubar  { color: black; } /* Menu principal*/
    #urlbar { color: black; }    /* Barra de url*/
    

  Facil, verdad?

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
