---
title: (ES)Primeros pasos con VPS I. Primera conexión SSH
date: 2013/03/20 23:44:26
layout: post
path: "/vps-firsts-steps/"
category: "Imported from my older blog"
---

Como tenía algo de crédito en mi cuenta de RedCoruna, he decidido gastarlo en un servidor VPS, pues me hacen un descuento del 50% (Si lees esto antes del 31-3-13 tu también te puedes beneficiar de este descuento). En este server tengo pensado correr aplicaciones web y tener scripts que se ejecuten periódicamente, así no tendré que dejar el ordenador encendido para que se ejecuten. Lo que más me gusta de este servicio es que trae CentOS instalado, una distro muy parecida a Fedora y que maneja paquetes .rpm. Además, me dan acceso root sin ningún cargo adicional. Hoy os explico los primeros pasos que hay que hacer al conectarse por ssh. ![](http://hotfixed.net/wp-content/uploads/2011/09/tunel-ssh.jpg) Abrimos nuestro cliente SSH favorito, si no tienes favorito te recomiendo Putty, y conectamos a la IP que nos han dado. El puerto por defecto es el 22 y supongo que no tendrás que cambiarlo. Una vez conectado, nos pide el usuario y la contraseña. Estos datos también nos los proporciona el proveedor. Una vez dentro podemos empezar a trastear como trasteamos en una terminal normal: 
    
    
    echo "Hola mundo! " > hola.txt ; cat hola.txt ; rm hola.txt

Como puedes ver, todo funciona como de costumbre Ahora, posiblemente tengamos que añadir nuestro usuario al selecto grupo de usuarios que pueden usar sudo.  Para ello, nos logeamos como root (la pass nos la tiene que proporcionar el proveedor) y ejecutamos: 
    
    
    vi /etc/sudoers

Buscamos la siguiente línea: 
    
    
    root    ALL=(ALL) ALL

Y justo debajo añadimos: 
    
    
    user    ALL=(ALL) ALL

Cambiando user por el nombre de usuario correspondiente. Hasta aquí el tutorial de hoy. Mañana sigo :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[maillot de foot](#107 "2013-06-22 18:12:36"):** Bookmarked!!, I like your website!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
