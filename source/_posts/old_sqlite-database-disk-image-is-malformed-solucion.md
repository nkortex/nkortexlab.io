---
title: (ES)(SQLite) "database disk image is malformed" - Solución
date: 2015/06/13 10:31:52
layout: post
path: "/database-malformed-sqlite.fix/"
category: "Imported from my older blog"
---
Hace unos días, mi instalación de Owncloud dejó de funcionar de repente. Por algún motivo, ninguno de mis dispositivos sincronizaba los datos con el servidor. Tras revisar los logs, me encuentro con un error de la base de datos sqlite: "database disk image is malformed". Esto quiere decir que el archivo de la base de datos está corrupto. Por suerte es fácilmente arreglable porque, aunque el formato del archivo sea erróneo, los datos suelen seguir intactos. Para empezar, comprobamos que, efectivamente, el archivo está corrupto: 
    
    
    echo 'PRAGMA integrity_check;' | sqlite3 database.db

Este comando suele devolver un simple 'ok', pero en este caso no lo hará, sino que nos informará de todas las cosas que no encajan. Luego hacemos un dump de todos los datos a otro archivo así: 
    
    
    sqlite3 database.db 
     sqlite> .mode insert
     sqlite> .output dump.sql
     sqlite> .dump

Por último, generamos otra base de datos con la información recopilada: 
    
    
    sqlite3 database_2.db 
     sqlite> .read dump.sql
     sqlite> .exit

No podemos olvidar que estamos rescatando datos de un archivo corrupto, por lo que es posible que algunas tuplas se pierdan, pero siempre es mejor recuperar algo que nada. Aunque sería mejor no tener que hacer esto por tener una copia de seguridad actualizada xP [Fuente](http://techblog.dorogin.com/2011/05/sqliteexception-database-disk-image-is.html)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
