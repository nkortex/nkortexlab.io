---
title: (ES)Enviar Whatsapps desde nuestro linux con yowsup
date: 2013/09/18 14:31:35
layout: post
path: "/Whatsapp-from-terminal/"
category: "Imported from my older blog"
---

Acabo de descubrir una especie de **API** no-oficial de **WhatsApp** escrita en **python** que nos permite mandar mensajes (entre otras cosas) desde el ordenador. El programa se llama **yowsup**, y no se cuanto seguirá funcionando, por eso de que whatsapp intenta erradicar estos servicios. Esto puede ser muy útil para determinadas cosas. A mi se me ocurren: 

  * Chat pc-whatsapp para conversaciones largas.
  * Envío masivo de mensajes (sed buenos)
  * Avisar desde un script a nuestro móvil cuando acabe alguna operación o se produzca algún evento.
No se recomienda usar tu teléfono. En vez de eso, usa uno de los gratuitos que ofrece[ Fonyou](http://www.fonyou.es/). ![](https://lh3.ggpht.com/bwBj9B4fGmTN_of0JS8xdkwklCmqCzSne1tJ9RaUNRQIzU-FEyCuFWzlsLyyPoTbyJE=w300) Para descargar y configurar el script, seguimos esos pasos: 

  1. Instalamos **git**, si no lo tenemos ya: apt-get install git o yum install git
  2. git clone git://github.com/tgalal/yowsup.git
  3. Hacemos un **cd** a la carpeta donde lo hayamos bajado y luego entramos en la carpeta **src**
  4. Editamos el archivo **config.example** y cambiamos los valores por: 
    * cc= 34
    * phone=34numerotelefono
    * id= (En blanco)
    * password= (En blanco, por ahora)
  5. Ahora lanzamos esto: 
    *         python yowsup-cli -c config.example --requestcode sms

  6. Nos llegará al móvil y a la cuenta **FonYou** un nuevo SMS con el código. Es posible que tarde mucho, pero no queda otra que esperar.
  7. Una vez tengamos el código de 6 dígitos ( XXX-XXX) lo introducimos de esta manera: 
    *         python yowsup-cli -c config.example --register XXX-XXX

  8. Modificamos el archivo config.example e introducimos el password que nos ha devuelto en el campo password. (id sigue estando vacío)
Una vez hecho todo esto, podremos enviar whatsapps a quien queramos de esta manera: 
    
    
    python yowsup-cli -c config.example -s 34numero "Probando, probando"

Para ver el resto de opciones del script: 
    
    
    python yowsup-cli -h

Lo dicho, sed buenos [Fuente](http://bitcoinwithpi.blogspot.com.es/2013/03/whatsapp-desde-nuestra-raspberry.html)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Charly](#239 "2013-09-19 06:02:20"):** Excelente amigo!!

**[Joshelu](#240 "2013-09-20 13:10:38"):** Hola! ¿Cuánto te tardó a tí en llegarte el código?. Te lo pregunto porque lo he intentado con un número de Google Voice y no llegó en horas, ahora lo estoy intentando con otro Fonyou y van un par de horitas y tampoco...

**[Julio Poisot De María](#241 "2013-09-21 21:02:54"):** Cuando ejecuto python yowsup-cli -c config.example --requestcode sms me da el siguiente mensaje de error ImportError: No module named dateutil Estoy usando Kubuntu 13.04 y tengo instalados python 2.7 y 3

**[admin](#242 "2013-09-22 08:49:55"):** Has indicado el número con el prefijo correspondiente?

**[admin](#243 "2013-09-22 08:51:18"):** Instala el paquete python-dateutil y prueba de nuevo ;)

**[Joshelu](#245 "2013-09-23 07:54:26"):** Así es, en el caso de Google Voice puse el 1 y en el caso de Fonyou el 34...

**[admin](#247 "2013-09-23 16:56:51"):** Y no te daba ningún error en la solicitud?? Has probado más veces?

**[Joshelu](#252 "2013-09-25 07:46:11"):** Por fin ha llegado! :D

**[Jose F Franco](#262 "2013-10-21 14:31:32"):** Muchas gracias por tu excelente post

**[admin](#265 "2013-10-21 17:57:25"):** Para eso estamos ;) Gracias por tu comentario!

**[Antonio](#285 "2013-11-23 19:06:22"):** Habrá alguna forma de ejecutar yowsup-cli desde una sesión de terminal en Android? Estoy interesado en hacer algunas tareas de automatización usando Tasker y me interesa ejecutarlo desde el propio teléfono.. Al ejecutar yowsup-cli desde Android me sale el siguiente error: yowsup-cli[41]: import not found yowsup-cli[43]: sysntax error: '('unexpected

**[admin](#286 "2013-11-25 19:02:43"):** Hola, no estoy seguro de que esto se pueda hacer. Y si se puede no se como hacerlo, pero Tasker no te permite una opción "compartir" con WhatsApp??

**[Antonio](#290 "2013-11-30 15:22:13"):** Ya logre enviar mensajes automáticos usando Tasker y yowsup-cli.. Tuve que modificar algunas partes del Script para poder enviar los parámetros y ahora funciona a la perfección.. Gracias por el tutorial..

**[jimmy's](#291 "2013-11-30 16:01:02"):** amigo mi pais es colombia, yo en el cc le coloco 54 y el numero del celular comienza por 54 y el numero seguido me sale esto, supongo que esta enviando. status: sent retry_after: 10805 length: 6 method: sms pero a mi celular no le llega ningun mensaje por whatsapp

**[chiara](#292 "2013-12-01 18:34:48"):** Hola, podrías indicarme como descargar yowsup desde esta página? https://github.com/tgalal/yowsup/tree/master/src No tengo Linux. Gracias!

**[admin](#295 "2013-12-07 10:31:12"):** Aquí lo tienes: https://github.com/tgalal/yowsup/archive/master.zip

**[admin](#296 "2013-12-07 10:31:54"):** Puede tardar bastante

**[Rob118](#297 "2013-12-19 20:41:54"):** Hola, lo he probado y va genial, pero al abrirlo en mi movil se desconecta de la PC, tendré que usar un numero a parte, pero dime.... se ha reportado cambios que provoquen que el sistema deje de funcionar? quiero lanzar un concurso por Whatsapp y espero recibir 10000 mensajes al día... me cerrarán la cuenta? cambiaran algo en la api y mi desarrollo al tacho? que has escuchado.

**[admin](#299 "2013-12-20 10:40:08"):** Uff... no me arriesgaría. En las condiciones de servicio de Whatsapp dice que no se puede usar con fines comerciales. Seguramente tengan un control del uso que hace cada usuario y si haces un uso excesivo te bloqueen la cuenta, lo cual sería un desastre si estabas haciendo un concurso. A mi, por ejemplo, me bloquearon por hacer una cantidad exagerada de peticiones a un solo numero (el mio). Yo te recomendaría otra alternativa, o concurso web de toda la vida o alguna otra aplicación de mensajería que cuente con API oficial, y que te indique claramente las peticiones a las que tienes derecho. Un saludo!

**[Enrique](#300 "2013-12-23 21:35:27"):** Se te ocurriria como mandar whatsapps a un grupo despues de ser aceptado en él por supuesto?

**[Enrique](#301 "2013-12-23 21:36:16"):** Por cierto, perfecto el tutorial. Funcionando 100%

**[Jaime Davis](#304 "2013-12-27 15:22:51"):** Hola , mira acabo de configurar la aplicación y enviar la solicitud, ya han pasado mas de 24 horas, esto es normal? cuanto tiempo puedes pasar antes de recibir la confirmación.

**[admin](#305 "2013-12-29 11:52:22"):** A mi no me tardó tanto... prueba a solicitarlo de nuevo

**[German](#311 "2014-01-13 00:55:22"):** el cc para colombia es 57

**[israel](#326 "2014-03-02 21:02:25"):** Buenas noches: Han quitado la web de git, sabes si te sigue funcionando. En el caso de si, puedes pasar el archivo zip de yowsup a mi email personal. Te lo agradezco de ante mano, no es para ningún fin comercial, simplemente para enviarme a mi mismo cuando hayan acabado procesos del ordenador.

**[admin](#327 "2014-03-04 19:46:39"):** Hola Israel, lo quitaron porque dejó de funcionar después de un cambio en los protocolos de WhatsApp. Si es para uso personal, puedes probar con Telegram, que tiene un API abierto y es bastante más fácil de utilizar. Un saludo!

**[Xoan](#353 "2014-06-14 10:27:02"):** Yo he intentado ejecutar esta línea "python yowsup-cli -c config.example --requestcode voice" en lugar de la otra porque yo estoy intentando registrar mi número fijo de casa, pero cuando lo hago me dice lo siguiente: status: fail param: number reason: bad_param A que se puede deber ? Habré configurado algo mal ?

**[Jose Miguel Cabrera](#362 "2014-07-25 04:51:18"):** Funciona. Para conseguir un número de prueba sugiero conseguir uno con la App heywire luego puedes ver tus SMS tanto por la App como por su web (heywire.com) Intente con un numero de Bolivia y el mensaje nunca llego. Intente con ese número de EEUU que se generó y llegó al instante.

**[Mauricio Martinez](#381 "2014-11-20 05:58:46"):** Y si nos platicas como lo hiciste funcionar en Android con yowsup-cli



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
