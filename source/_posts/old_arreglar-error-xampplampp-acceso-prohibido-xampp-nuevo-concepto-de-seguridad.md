---
title: (ES)Arreglar error Xampp/Lampp "Acceso prohibido! XAMPP nuevo concepto de seguridad"
date: 2013/02/05 11:23:31
layout: post
path: "/Acceso-prohibido-xampp-lampp/"
category: "Imported from my older blog"
---

En las últimas versiones de Xampp, se ha introducido un nuevo método de seguridad que impide acceder a determinadas partes de la configuración (Por ejemplo, a PHPMyAdmin). En internet hay gente que dice de arreglarlo modificando el archivo de configuración httpd-xampp.conf. Hacer esto arregla el problema, pero deja tu servidor al aire, perfecto para que algún indeseable acceda sin tu consentimiento. ![](http://recursostic.educacion.es/observatorio/web/images/stories/xampp.jpg) La forma correcta de arreglarlo es ejecutando en un terminal: 
    
    
    sudo /opt/lampp/lampp security

Luego te pide que cambies las contraseñas por defecto o que añadas contraseñas a usuarios que no la tengan. Estos usuarios son: 

  * Usuario pma y root de phpmyadmin
  * Usuario nobody de ftp
  * Y usuario de MySQL

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Mariano](#70 "2013-05-16 23:15:22"):** Funciono!! Se agradece! PD: Probado en Linux Mint 14, Xampp 1.8.1. Argentina =)

**[admin](#71 "2013-05-17 08:07:56"):** Me alegro de que te haya sido útil Mariano! Espero verte más por aquí. Saludos desde España :D

**[Armando Mac.](#267 "2013-10-24 21:54:03"):** Que podrias decir si este problema es en windows.

**[admin](#269 "2013-10-26 08:24:35"):** Pues supongo que tendrás que correr el ejecutable "lamp" pasandole como parámetro "security", pero el ejecutable se encontrará en otro directorio. Pero no te lo puedo asegurar porque hace como 3 años que no utilizo windows :S

**[anono](#283 "2013-11-16 02:52:27"):** Amigo aveces no es la configuracion del XAMPP, tambien tienen que crear una DMZ en su modem. A mi me pasaba que no podia acceder ami red de xampp desde internet solo desde la LAN pero configure una DMZ en mi modem de telmex y me funciono.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
