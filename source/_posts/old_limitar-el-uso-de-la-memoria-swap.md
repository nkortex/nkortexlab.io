---
title: (ES)Limitar el uso de la memoria SWAP
date: 2016/03/15 10:48:19
layout: post
path: "/limit-swap-memory/"
category: "Imported from my older blog"
---
La memoria de intercambio o swap es útil en aquellos ordenadores con una memoria RAM limitada. Sin embargo en máquinas modernas con 4GB de memoria o más también suele estar presente. Su funcionamiento se basa en copiar a disco duro la información que no cabe en la RAM, pero con la configuración que suelen traer las distribuciones por defecto, esta copia se hace mucho antes de que sea realmente necesario. El parámetro que tiene en cuenta el kernel para decidir cuando empezar a usar la SWAP es el swappiness, un valor entre 0 y 100 y que por defecto suele estar en 60. A más bajo esté, mas llena ha de estar la RAM para que se empiece a usar (con 0 no se utilizará nunca) por lo que desde aquí recomiendo bajarlo a una valor entre 5 y 10. Notarás un incremento del rendimiento de tu máquina, pues las lecturas/escrituras en disco son varios ordenes de magnitud mas lentas que en memoria. Para consultar el valor de swappiness que tenemos configurado actualmente hacemos: 
    
    
    cat /proc/sys/vm/swappiness

y para modificar este valor: 
    
    
    sudo sysctl -w vm.swappiness=10

Una vez hecho esto, podemos probar el rendimiento del sistema durante un tiempo para ver si mejora o no. Si estamos contentos con el cambio (ya os adelanto que lo estaréis) podemos hacerlo permanente modificando el archivo de configuración de sysctl: 
    
    
    su -c "echo 'vm.swappiness=10' >> /etc/sysctl.conf"

[Fuente](http://geekland.eu/optimizar-el-uso-de-la-memoria-swap/)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
