---
title: (ES)Resaltar código en LibreOffice con COOoder
date: 2013/04/17 17:49:34
layout: post
path: "/code-highlighter-libreoffice/"
category: "Imported from my older blog"
---
Una de las cosas que le falta a Libreoffice y que, según mi opinión, debería traer de serie, es un resaltador de código, para poner el código bonito, con colores y tal. Pero esto se puede solucionar gracias a la extensión COOoder. Esta extensión se basa en el sistema GeSHi (Generic Systax Highlighter o Resaltador de sintaxis genérico); y, por lo tanto, soporta todos los lenguajes soportados por este: C++, bash, java, perl, PHP, python,... la lista completa la podéis ver aquí: <http://qbnz.com/highlighter/>. Para instalar esta extensión la descargamos desde el repositorio de extensiones de LibreOffice: <http://extensions.libreoffice.org/extension-center/coooder> . Luego, le damos doble click, confirmamos que hemos leído los términos de uso e instalamos. Luego, para usarlo, seleccionamos el código y vamos a: **tools > add-ons > COOoder** Seleccionamos el lenguaje en el que está escrito y ok. De este modo pasaremos de un código feo a algo molón: ![2](/wp-content/uploads/2013/04/2.jpg)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
