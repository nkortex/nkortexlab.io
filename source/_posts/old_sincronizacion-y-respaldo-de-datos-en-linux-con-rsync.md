---
title: (ES)Sincronización y respaldo de datos en linux con rsync
date: 2013/09/15 10:52:51
layout: post
path: "/rsync-tutorial/"
category: "Imported from my older blog"
---

Se por experiencia propia que hacer copia de seguridad de nuestros datos es una buena idea. Últimamente vengo usando** rsync** para hacer el respaldo. Hoy os voy a enseñar el potencial de este programa. ![](http://backupvalencia.es/sistema-backup-online/valencia/imagenes/backup1.png) Primero instalamos el paquete **rsync** con rpm o apt-get, según la distribución que uses. Luego, para utilizarlo simplemente: 
    
    
    $ rsync -av ORIGEN DESTINO

La opción **-a**, preserva los permisos, los tiempos de modificación, hace la sincronización recursiva,... Y la opción **-v** hace que el programa muestre todos los detalles de la operación. El programa tiene muchas más opciones, consultad el **\--help** o el** man** para verlas todas. Para recuperar el respaldo cuando haga falta, simplemente se intercambia el origen y el destino. Para darle un uso más práctico a esto, se me ocurre hacer un **alias** o programarlo con **cron** cuando más convenga. Espero que sea de utilidad y os recuerdo: Comentar es gratis :D [Fuente](http://etccrond.blogspot.com.es/2013/09/respaldar-datos-linux-facilmente.html)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[José Luis](#232 "2013-09-16 10:05:24"):** ¿Y un pen, no vale? :-). Saludos Linuxeros.

**[admin](#233 "2013-09-16 11:57:17"):** Por supuesto, vale cualquier soporte

**[Charly](#257 "2013-10-11 04:43:20"):** Interesante, espero poder aplicarlo!!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
