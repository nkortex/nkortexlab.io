---
title: (ES)Como configurar un Joystick en Fedora 18
date: 2013/01/19 15:34:20
layout: post
path: "/configure-joystick-fedora/"
category: "Imported from my older blog"
---

En mi anterior entrada os explicaba como instalar en Linux los mejores emuladores de las mejores consolas clásicas (Aqui: <http://023.es/?p=156>). Ahora os voy a enseñar a configurar un Joystick para jugar con esos emuladores o para usarlo en otra cosa. 

  1. Antes de nada, mirad en esta página si vuestro Joystick es compatible: <http://www.linuxcompatible.org/compatdb/vendors/hardware_linux,11.html>. Que no aparezca en la lista no quiere decir que no vaya a funcionar, pero si aparece, funciona seguro(si el tuyo no está en la lista, pero es compatible con Linux, no sería mala idea que avisaras a los mantenedores de esa página para que lo añadan)
  2. Instalamos el paquete Joystick: 
    
        sudo yum install joystick

  3.     sudo modprobe joydev

  4. Ahora configuramos el método de entrada. Si tu Joystick está conectado por USB ejecuta: 
    
        sudo modprobe iforce

Si está conectado por un "gameport" ejecuta: 
    
         sudo modprobe ns558

  5. Ya está todo configurado y podemos pasar a probarlo. ejecuta lo siguiente: 
    
         jstest /dev/input/js0

Esto solo funciona si es el primer joystick que conectas. Si no es el primero, tendrás que cambiar el comando un poco. Una vez ejecutado te tiene que salir en la terminal algo parecido a esto:![sasasa](/wp-content/uploads/2013/01/sasasa.jpg)Los datos que aparecen irán cambiando conforme muevas el joystick o pulses los botones.
  6. Ya puedes usarlo en tus juegos. En el emulador osmose de la sega master system, funciona sin ninguna configuración adicional

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Arturo](#393 "2014-12-23 00:08:47"):** Fedora 21. Útil. Qué parte del codigo debo alterar para instalar más joysticks? ... O solo aplica para probarlos? (Porque no pude probar el mío -- Marca Perfect Choice, pero funcionó a la perfección)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
