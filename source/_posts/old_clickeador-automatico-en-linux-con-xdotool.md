---
title: (ES)Clickeador automático en Linux con xdotool
date: 2013/03/10 10:08:11
layout: post
path: "/auto-clicker-bash/"
category: "Imported from my older blog"
---

El otro día estaba haciendo un evento en el Tuenti y quería invitar a todos mis colegas, en plan SPAM. Había dos formas de hacer eso, la normal y la geek. La normal era poner el cursor encima del primer contacto y hacer click. Una vez que se pasara a la lista de invitados, hacer otro click. Acto seguido otro, luego otro y así hasta más de 300 contactos. Como era un royo, me pasé a la forma geek que os explico en este post. ![](http://www.marketingpilgrim.com/wp-content/uploads/2012/10/click1.jpg) Cabe destacar que esto vale para cualquier cosa que se os ocurra, no solo spamear a la gente sino también votaciones automáticas, ... el límite es vuestra imaginación. El programa que usaremos es **xdotool**. Seguramente esté en los repositorios de vuestra distro, así que lo instaláis con un **yum install**, ** apt-get** o **pacman**. En la ayuda podéis ver que tiene muchas funciones que seguramente explique por aquí en otro momento (parece que el programa tiene mucho potencial) pero ahora nos vamos a centrar en la opción **click**. Esta opción, al contrario de lo que podría indicar su nombre, hace un click en la pantalla XP. Si le pasamos un 1 como argumento, hace un click con el botón primario. Un 2 lo hace con el botón del centro (El de la ruleta) y un 3 lo hace con el botón derecho. Sólo con esta herramienta y unas conocimientos básicos de bucles en bash podemos armarnos nuestro clickeador automático básico. He aquí el código: 
    
    
    #!/bin/bash
    
    for x in `seq 1 300`; do
       xdotool click 1
    #  sleep 0.5
    done

En este caso hace 300 clicks en la posición actual del cursor. Sería fácil adaptarlo a la cantidad de clicks que quieres cambiando el 300 por** $1 **y pasándole el número entero por parámetro al llamar al script. También se podría añadir un pequeño retraso entre clicks descomentando la línea "sleep" y cambiando 0.5 por los segundos que quieres que espere. Espero que sea útil y ya sabes, cualquier duda, coméntala :)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[fil7ron](#379 "2014-11-17 13:02:36"):** Me ha molado este post. Aquí te dejo mi humilde comentario y te animo a seguir adelante. :)

**[admin](#380 "2014-11-17 17:28:03"):** Gracias apañao!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
