---
title: (ES)Conectarnos a un servidor mediante SSH sin introducir la contraseña
date: 2013/10/01 10:20:41
layout: post
path: "/connect-ssh-with-public-key/"
category: "Imported from my older blog"
---
![](http://2.bp.blogspot.com/-WH4msmSBpLk/USyNgurM0KI/AAAAAAAAAw4/Tdr4u0Ckuls/s1600/SSH_logo.png)

Si siempre nos vamos a conectar mediante** ssh** al servidor desde el mismo ordenador (o la mayoría de las veces) podemos establecer una configuración adicional para que solo nos pida la contraseña una primera vez. Esto puede usarse para otorgar una seguridad extra, ya que podemos establecer una contraseña muy larga sin el aburrimiento de tener que introducirla cada vez que conectemos. Lo primero que tenemos que hacer es generar una** clave pública**. En nuestro ordenador local ejecutamos los siguiente: 
    
    
    ssh-keygen -b 4096 -t rsa

Luego nos preguntará varias cosas. Solo pulsamos **enter** sin escribir nada, un total de tres veces ( A no ser que queramos cambiar el directorio donde lo guarda o establecer una contraseña). Al final nos devolverá algo parecido a esto: ![Captura de pantalla de 2013-09-29 16:42:18](/wp-content/uploads/2013/10/Captura-de-pantalla-de-2013-09-29-164218.png) Una vez tenemos la clave generada, se la tenemos que dar al servidor remoto, de este modo: 
    
    
    ssh-copy-id user_remoto@12.34.56.78

(teniendo en cuenta que la ip del server es 12.34.56.78 y el usuario es user_remoto) Una vez introducida la contraseña que nos pide, el servidor ya tiene la llave pública de la máquina local. Ahora, probamos a conectarnos a la máquina remota normalmente y vemos que no nos pide ningún password.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
