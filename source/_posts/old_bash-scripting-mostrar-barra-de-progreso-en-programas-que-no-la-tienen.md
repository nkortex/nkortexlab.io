---
title: (ES)(Bash scripting) Mostrar barra de progreso en programas que no la tienen
date: 2015/08/09 11:48:13
---

Para grabar una iso en un dispositivo para hacerlo booteable se suele utilizar **dd**, una herramienta sencilla pero que no ofrece una barra de progreso, lo cual es incómodo si estamos trabajando con archivos de varios gigas y tenemos que esperar a que acabe sin tener la certeza de que va a acabar algún día. Para solucionar esto os traigo la herramienta **pv **(Normalmente disponible en los repositorios). Básicamente lo que hace es copiar la entrada estándar a la salida estándar, pero mostrando algo de Feedback por pantalla, como el tiempo transcurrido, el porcentaje completado, o el tiempo estimado. Por lo tanto, redirigiendo entradas y salidas, lo que antes hacíamos asi: 
    
    
    dd if=Fedora-Live-Workstation-x86_64-22.iso of=/dev/sda

Ahora lo hacemos asi: 
    
    
    dd if=Fedora-Live-Workstation-x86_64-22.iso | pv | dd of=/dev/sda

Además de dd, pv se puede utilizar con cualquier otro comando, como copias de datos vía nc,... 

[Fuente](http://www.cyberciti.biz/open-source/command-line-hacks/pv-command-examples/)
<!-- PELICAN_END_SUMMARY -->
#<!-- PELICAN_END_SUMMARY -->
## Comments

**[Juangmuriel](#447 "2016-03-10 20:14:00"):** Muy bueno!! yo uso un script en bash que usa rsync para copia de seguridad diaria al NAS que tengo. Estaba buscando algo que muestre el proceso de copia incremental, lo voy a probar, pero me temo que puesto que la copia de seguridad va programada en el crontab, el proceso se realiza en segundo plano y no tengo forma de hacer visible lo que ocurre, es correcto?? Saludos, te sigo.

**[admin](#450 "2016-03-11 13:11:32"):** Podrías hacerlo con zenity o con yad. Creo que ya has encontrado la entrada en la que explico una aproximación a zenity y yad es muy parecido, pero aporta algunas opciones extra. Como bien dices, pv no te valdría, pues se ejecuta en background

**[erm3nda](#465 "2016-08-21 02:25:43"):** Yo lanzo dd con normalidad "dd comando &" y luego "kill -USR1 pgrep '''^dd'" en un bucle o usando "watch -n1 comando", que nos dice el progreso que lleva. A mí pv no me ha funcionando como espero, pero es ciertamente útil, y quien sabe, lo mismo también usa el comando kill con la señal -USR1 para leer y crear la barra con los datos extraídos.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
