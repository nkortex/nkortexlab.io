---
title: (ES)Como instalar OpenShot en Fedora y Ubuntu/Linux mint
date: 2013/01/30 12:32:20
layout: post
path: "/intro-openshot/"
category: "Imported from my older blog"
---
![](http://www.openshot.org/images/logo.png) OpenShot es un editor de video open source muy potente y con una interfaz de usuario muy amigable. A grandes rasgos, las características que trae son: 

  * Tracks ilimitadas
  * Integración con la función Arrastrar/soltar de gnome
  * Transiciones de video con previsualización en tiempo real
  * Marcas de agua
  * Soporte para efectos 3D (necesita blender)
  * Compatible con imágenes vectoriales .svg
  * Soporte para muchos formatos de video/imagen/musica (todos los comatibles con ffmpeg)
  * Opción para añadir créditos y marcas de agua
  * Edición y mezcla de audio
![](http://www.openshot.org/images/slider_pic01.jpg) Instalarlo es muy fácil: Fedora 
    
    
    sudo yum install openshot

Ubuntu/Linux mint 
    
    
    sudo apt-get install openshot

Si vas a usar opciones que necesiten 3D, instala también** Blender** (Entrada dedicada a Rocío, que se que le gustan estas cosas)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
