---
title: (ES)Que hacer despues de instalar Fedora 18
date: 2013/01/16 22:19:25
layout: post
path: "/what-to-do-afer-install-fedora18/"
category: "Imported from my older blog"
---

![](https://lh5.googleusercontent.com/-RGyPEX2aB7M/ULU8zEa_SfI/AAAAAAAAaqc/ynH7oSTYB9k/s400/fedora18beta.jpg) Pues por fin ha salido la nueva versión de Fedora: Fedora 18 Spherical Cow y os traigo un tutorial de lo que hay que hacer tran instalar Fedora 18. Este tutorial trae cosas básicas que todo el mundo debería hacer y cosas más específicas que pongo mas que nada para acordarme yo. Que cada uno haga lo que quiera, no me responsabilizo del resultado de este tuto :) Lo primero que hay que hacer es entrar en un terminal como root. La pass es la que pusisteis en la instalación y conociéndoos, seguramente sea la misma que la de Facebook x) Una vez somos root, actalizamos el sistema con el comando: 
    
    
     yum -y update

Si os da algún problema, probad con este otro y estará solucionado: 
    
    
    yum -y update --nogpgcheck

Después de actualizarlo todo y dejarlo todo bonito, pasamos a añadir los repositorios de RPM Fusion: 
    
    
    yum -y localinstall --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm
    
    yum -y localinstall http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/russianfedora-free-release-stable.noarch.rpm
    
    yum -y localinstall http://mirror.yandex.ru/fedora/russianfedora/russianfedora/nonfree/fedora/russianfedora-nonfree-release-stable.noarch.rpm

Estos repositorios son básicos y tienen programas que no se pueden añadir en los oficiales por tema de licencias. Un Fedora sin RPM Fusion es como un cepillo sin pelos. Luego hay que instalar códecs no-libres para poder escuchar música y ver pelis normalmente. AVISO: estos codecs no son libres, si te sientes sucio usando software privativo no los instales, pero no vas a poder reproducir ni un mísero MP3 
    
    
    yum install gstreamer-plugins-bad gstreamer-plugins-bad-free-extras gstreamer-plugins-bad-nonfree gstreamer-plugins-ugly gstreamer-ffmpeg ffmpeg mencoder ffmpeg2theora mplayer

En ese comando se incluyen para instalar el ffmpeg, el mencoder y el mplayer, bastante importantes también, por tema de dependencias. Si no sabes de lo que hablo, tu hazme caso e instálatelo. Lo sé, 248MB de descarga, pero tienes el sistema nuevo, ya es hora de empezar a meterle morralla no? x) Si programas, compilas, o usas tu ordenador para tareas de desarrollo, te harán falta estos paquetes: 
    
    
    yum -y install kernel-headers
    
    yum -y install kernel-devel
    
    yum -y groupinstall "Development Tools"
    
    yum -y groupinstall "Development Libraries"

Recuerda instalarlos porque Fedora no trae ni compiladores ni ná y luego cuando te hacen falta tienes que ponerte a instalarlos (lo digo por experiencia propia XD) Si tienes un kernel PAE instala esto: 
    
    
    yum -y install kernel-PAE-devel

**Solo y exclusivamente si tienes kernel PAE. Sino la lias pardísima.** Si quieres ver algún vídeo de Yutuf, te hará falta el flash así que añade el repositorio de adobe específico para tu arquitectura (32 o 64) : **32:**
    
    
    yum -y localinstall http://linuxdownload.adobe.com/adobe-release/adobe-release-i386-1.0-1.noarch.rpm

**64:**
    
    
    yum -y localinstall http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm

Y luego instalar: 
    
    
    yum -y install flash-plugin

Java: (versión de pega open source OpenJDK) 
    
    
    yum -y install java

Y para extraer ficheros comprimidos: 
    
    
    yum -y install unrar p7zip p7zip-plugins

Con eso ya tienes un sistema la mar de bonito y de usable, pero te harán falta mas programas no? aquí pongo la lista de los que voy a instalarme yo: 

  * gimp
  * pinta
  * vlc (the best player in the universe)
  * clementine
  * skype
  * chrome
  * gnome-tweak-tool (IMPORTANTISIMO)
  * Filezilla
También es buen momento para decir que Gnome 3 es genial (para mi gusto) siempre y cuando se personalice con las extensiones adecuadas. Desde <http://extensions.gnome.org> se pueden instalar dándole a un botón. Las que yo siempre pongo son: 

  * <https://extensions.gnome.org/extension/230/battery-remaining-time/>
  * <https://extensions.gnome.org/extension/156/quit-button/>
  * <https://extensions.gnome.org/extension/55/media-player-indicator/>
  * <https://extensions.gnome.org/extension/5/alternative-status-menu/>
  * <https://extensions.gnome.org/extension/112/remove-accesibility/>
  * <https://extensions.gnome.org/extension/82/cpu-temperature-indicator/>
  * <https://extensions.gnome.org/extension/358/activities-configurator/>
  * <https://extensions.gnome.org/extension/153/nothingtodo/>
  * <https://extensions.gnome.org/extension/120/system-monitor/>
  * <https://extensions.gnome.org/extension/442/drop-down-terminal/>
  * <https://extensions.gnome.org/extension/365/transmission-daemon-indicator/>
  * <https://extensions.gnome.org/extension/561/desktop-scroller-all-sides-for-gnome-36/>
[en proceso]

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Javi](#12 "2013-03-08 11:51:56"):** Hola... Recien aterrizado en Fedora tras casi cinco anyos con Ubuntu...ufffff...el hecho es que me regalaron un netbook usado y lo estoy usando como conejillo de indias para ver que decido instalar en el de sobremesa actualmente con Ubuntu 10.04....no me gusta Unity y creo que a Canonical se le esta yendo la pinza...:( Pues probe Ubuntu 10.04 con Gnome3 y no corria bien ademas de no poder sacarle todo el partido y me acorde de Fedora de un pinio

**[admin](#13 "2013-03-09 10:32:39"):** Gnome3, según mi experiencia, va mejor en Fedora que en cualquier otra distro. Ya me contarás tu experiencia con Fedora, espero que te quedes aquí 5 años por lo menos x)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
