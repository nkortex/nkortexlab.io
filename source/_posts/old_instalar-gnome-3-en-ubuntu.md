---
title: (ES)Instalar Gnome 3 en Ubuntu
date: 2013/01/20 12:41:36
layout: post
path: "/gnome3-on-ubuntu/"
category: "Imported from my older blog"
---
El entorno de escritorio de Ubuntu es una basura, todo hay que decirlo. No cabe duda de que unity acabará siendo algo grande, pero ahora mismo no supera a Gnome ni KDE, según mi opinión, así que os voy a enseñar a instalar Gnome3 en Ubuntu. ![](http://www.tribulinux.com/wp-content/uploads/2010/03/gnome.png) Es muy sencillo, abrís un terminal y escribís: 
    
    
    sudo apt-get install ubuntu-gnome-desktop ubuntu-gnome-default-settings gnome-tweak-tool

Cuando nos pregunte el gestor de inicio de sesión, seleccionamos GDM. Esto instalará los paquetes básicos para usar gnome3 y el editor de configuraciones.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
