---
title: (ES)Introducción a Git con BitBucket
date: 2014/02/16 11:30:26
layout: post
path: "/bickbucket-git-intro/"
category: "Imported from my older blog"
---
Si vuestros proyectos empiezan a crecer más de la cuenta hay que empezar a llevar un control de las versiones y de los cambios que vamos haciendo. Para ello os recomiendo utilizar un sistema Git y hoy os voy a enseñar a empezar un proyecto con Bitbucket. ![](http://git-scm.com/images/logos/1color-orange-lightbg@2x.png) He elegido Bitbucket y no Github porque me permite tener repositorios privados sin una cuenta premium, pero todas las plataformas Git funcionan básicamente igual. Si me queréis agregar, [soy admin](https://bitbucket.org/admin). Un vez creada una cuenta en[ https://bitbucket.org](https://bitbucket.org) , nos instalamos git en nuestro ordenador con yum o con el manejador de paquetes correspondiente. Ahora tenemos que ir a [https://bitbucket.org/repo/create ](https://bitbucket.org/repo/create)y crear un nuevo repositorio con las opciones según nos interesen. (Voy a dar por hecho que ya tenéis archivos creados y no empezáis de 0 el proyecto. Si empezáis de 0 tampoco vais a tener mucho problema en adaptar el tuto) Bien, ya tenemos nuestro repositorio creado. Ahora tenemos que inicializar los archivos que Git necesita para funcionar: vamos al directorio de nuestro proyecto y hacemos: 
    
    
    git init

Esto creará el directorio .git/ que es necesario para que el sistema mantenga la pista de todos los archivos que conforman el proyecto. Una vez hecho esto, tenemos que "informar" al servidor de BitBucket de nuestras intenciones, por así decirlo: 
    
    
    git remote add origin https://user@bitbucket.org/user/repo.git

Cambiando la url por la de nuestro repositorio (Te la proporciona BitBucket) Ahora añadimos todos los archivos de nuestro proyecto al "staging area": 
    
    
    git add .

Si lo que queremos es añadir archivos concretos, cambiamos el punto por el nombre de estos, como en cualquier otro comando. Con esto ya tenemos todos los archivos necesarios listos para añadirlos al repositorio: 
    
    
    git commit -m "Mensaje que describe los cambios que se han hecho" -a

Perfecto, ya solamente nos queda sincronizarlos con el servidor de BitBucket: 
    
    
    git push -u origin master

De este modo ya podemos acceder a la web de BitBucket y ver que efectivamente, están ahí todos los archivos. Para actualizar la copia local hacemos: 
    
    
    git pull --rebase

Y para descargar todo un repositorio ya creado: 
    
    
    git clone https://user@bitbucket.org/user/repo.git

Con esto ya se puede ir tirando con Git, aunque este tiene muchísimas más opciones que iré contando por aquí cuando me vaya adentrando en el mundillo. Espero que sea útil :)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
