---
title: (ES)Usar y Automatizar Telegram desde Terminal en Linux
date: 2014/05/23 09:44:55
layout: post
path: "/telegram-from-cli/"
category: "Imported from my older blog"
---

Hoy, tras meses sin escribir por falta de tiempo, vuelvo para explicar como utilizar telegram desde terminal. Parto de que tenéis un número de telefono ya registrado en telegram. Sino, el proceso varía un poco.   Bueno, empecemos descargando el cliente desde github: 
    
    
    git clone https://github.com/vysheng/tg.git

Lo instalamos, bien con un ./configure make o bien con el paquete rpm. Si todo va bien, lo lanzamos: 
    
    
    telegram

nos pide un número de teléfono (con el + y el prefijo). Si ya está registrado os llegará de forma casi instantánea un SMS con un código que introducimos también. Si no estás registrado, te pide tus datos de registro. Ahora te muestra una lista de tus conversaciones y los mensajes que tengas y te sale un prompt para introducir comandos. Los comandos disponibles, y su utilidad se pueden consultar con el comando help. Pero si lo que queremos hacer es automatizar su uso, lo mejor es evitar este tipo de prompts utilizándolo exclusivamente desde línea de comandos: 
    
    
    echo -e "msg $user $mensaje \nquit" |telegram -W     #Importante la opción -W

A partir de ese código nos podemos crear alias y funciones y scripts según necesitemos.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[fran](#388 "2014-12-13 09:57:05"):** Me gustaría saber cómo puedo configurar el script en un servidor web (apache) para poder acceder a través de un navegador y enviar mensajes con Telegram desde una página en el servidor web. ¿Se podría uar Whatsapp en lugar de Telegram? Gracias

**[fran](#389 "2014-12-13 09:58:16"):** donde pone ¿Se podría uar Whatsapp en lugar de Telegram?, es usar (no uar)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
