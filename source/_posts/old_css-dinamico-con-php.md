---
title: (ES)CSS dinámico con PHP
date: 2014/04/09 10:40:24
layout: post
path: "/dinamic-css-with-php/"
category: "Imported from my older blog"
---
![](http://webstudio316.com/wp-content/uploads/2012/04/css-dreamweaver.png) Como sabéis, PHP nos permite la creación de páginas dinámicas. Lo que no todos saben es que se puede incluir código PHP en el documento CSS de una web para conseguir que el estilo también sea dinámico. Lo único que hay que hacer es: 

  * Cambiar la extensión del archivo de .css a .php
  * Incluir la cabecera: <?php header("Content-type: text/css"); ?>
  * Incluir el código php que necesitemos
  * Cambiar las referencias al nuevo archivo acabado en .php
En el siguiente ejemplo elije una imagen al azar entre 3: 
    
    
    <?php header("Content-type: text/css"); 
    
    $imagen[0]='imagen_34.jpg';
    $imagen[1]='img_12.jpg';
    $imagen[2]='foto3.jpg';
    
    $ran= rand(0,2);
    
    ?>
    [...]
    
    background: url('<?=$imagen[$ran]?>');
    
    [...]

También se puede trabajar con variables pasadas por get.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
