---
title: (ES)Como descargar archivos m3u o m3u8 desde Linux con ffmpeg
date: 2015/08/01 11:15:51
layout: post
path: "/download-m3u8-files-with-ffmpeg/"
category: "Imported from my older blog"
---
m3u es un formato de archivo que define la localización de uno o varios archivos multimedia y que últimamente se está usando en los servicios de vídeo en streaming como powvideo y parecidos. En mi proyecto [streaming-dl](https://github.com/admin/Streaming-dl) necesitaba interpretar este tipo de archivo para descargar el vídeo al que referencia y la forma más sencilla que he encontrado es con **ffmpeg** ( Instalado en cualquier ordenador que se precie ) de este modo: 
    
    
    ffmpeg -i http://url.com/archivo.m3u8 -c copy "Archivo-de-salida"

Esto detectará el formato de entrada y descargará correctamente el archivo en el directorio actual. [Fuente](http://stream-recorder.com/forum/downloading-m3u8-using-ffmpeg-t18574.html)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
