---
title: (ES)Instalar OwnCloud en tu servidor
date: 2012/12/31 19:01:16
layout: post
path: "/install-owncloud/"
category: "Imported from my older blog"
---

Owncloud es un sistema parecido a Dropbox que sirve para almacenar tus archivos en la nube y acceder a ellos de una forma sencilla. La gran desventaja con respecto a Dropbox es el hecho de que no tiene sincronización con carpetas locales (de momento). Sin embargo tiene varias ventajas como que no tienes límite de almacenamiento(puedes usar todo el disponible en tu hosting o en tu pc) y es open source, por lo que irá mejorando rápidamente. Además, tiene una versión temprana de una aplicación para Android. ![](http://www.muylinux.com/wp-content/uploads/2012/05/owncloud-4.jpg) Para instalarlo en tu servidor de hosting, primero necesitas descargarlo desde aqui: <http://owncloud.org/support/install/> . Luego descomprimes el .tar y subes la carpeta entera mediante tu programa FTP favorito (recomiendo filezilla) a tu server. Mientras se sube, ve a por un café, pasea al perro o llama a tu madre. Son muchos archivos y va a tardar un rato. Pero para no teneros aburridos, mientras tanto vamos a instalar el cliente para sincronizar archivos. En mi caso lo voy a hacer en Fedora, pero también está disponible para CentOS, Ubuntu, OpenSUSE y Debian, así como para Windows y Mac. Si tu intención es instalarlo en un Windows aquí no vas a encontrar respuestas a tus preguntas, vete a un foro o a otro blog, gracias. Si por el contrario quieres instalarlo en linux, es muy sencillo: 

  1. Ve a esta página: [http://software.opensuse.org/download/package?project=isv:ownCloud:devel&package=owncloud-client](http://software.opensuse.org/download/package?project=isv:ownCloud:devel&package=owncloud-client)
  2. Selecciona tu distro; en mi caso, Fedora
  3. Logueate en el terminal como root (comando su + tu pass)
  4. introduce los comandos que te indican
Cuando acabemos de instalarlo, ya habrá acabado todo de subirse, podremos continuar. Accedemos por el navegador a la carpeta donde lo hayamos metido. Por ejemplo: http://tu-server.com/owncloud. Nos aparecerán errores que hay que solucionar antes de seguir; la mayoría porque el servidor no tiene permisos de escritura en ciertos archivos. Lo solucionamos también por FTP y refrescamos. ![Captura de pantalla de 2012-12-31 13:23:52](/wp-content/uploads/2012/12/Captura-de-pantalla-de-2012-12-31-132352.png)   Ahora nos pide un usuario y una contraseña, que será la que usaremos para acceder como admin; un directorio de almacenamiento, que puedes dejar el que viene por defecto; y los datos de la base de datos(valga la redundancia) que se supone que ya has creado. Si no sabes crear una base de datos, es simplemente acceder al PhpMyAdmin y crearla, hay miles de tutoriales en internet y no viene al caso así que no lo voy a explicar aquí. Hecho esto ya podrás subir tus archivos fácilmente. Y tenerlo todo centralizado. No ha sido tan difícil no? x) PD: El espacio por defecto para subir un archivo es 512 MB, es decir, medio giga. Para solucionar esto (Aumentar o disminuir esta cantidad dependiendo de tus necesidades) sólo tienes que modificar el archivo "php.ini" Que encontrarás en la carpeta /etc de tu server. PD2: Para más información aquí tenéis un buen post: <http://www.mclibre.org/consultar/webapps/lecciones/webapps_owncloud_1.html>

<!-- PELICAN_END_SUMMARY -->
## Comments

**[BigByte](#349 "2014-05-31 12:50:50"):** Hola,al cabo de instalar owncloud en mi hosting. empece con 1.5 TB, cosa que me parecio guay, pero ahora mi espacio disminuye cada dia, hasta hoy que esta en 50 gb, de los cuales esta usado el 2%. ¿por que disminuye el espacio de almacenamto en el cloud? ¿habre hecho algo mal durante la instalación? por favor, contestame. gracias



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
