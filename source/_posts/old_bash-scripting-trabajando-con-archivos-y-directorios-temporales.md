---
title: (ES)(Bash Scripting) Trabajando con archivos y directorios temporales
date: 2014/04/12 09:48:49
---
Si estamos trabajando con archivos en nuestros scripts y no es necesario mantenerlos entre ejecuciones, lo correcto es utilizar archivos temporales en la carpeta /tmp. Esta carpeta se elimina tras cada reinicio y, según el estándar, ningún programa debe esperar encontrar esos archivos en una ejecución posterior. Para crear archivos temporales se utiliza mktemp de esta manera: 
    
    
    archivo_temporal=$(mktemp)       # mktemp crea el archivo y devuelve su path
    echo $archivo_temporal           #+que se almacena en archivo_temporal
    echo Hola Mundo > $archivo_temporal
    cat $archivo_temporal
    rm $archivo_temporal             # Se puede borrar manualmente, pero no es necesario

También podemos crear directorios temporales con la opción -d: 
    
    
    directorio_temporal=$(mktemp -d)

Y archivos FIFO: 
    
    
    fifo_temporal=(mktemp -u)        # Con la opción -u devuelve un nombre válido de archivo pero no lo crea. 
    mkfifo $fifo_temporal            # mkfifo crea el archivo físico en el path pasado por argumento

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
