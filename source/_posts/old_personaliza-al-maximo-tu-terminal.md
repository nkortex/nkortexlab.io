---
title: (ES)Personaliza al máximo tu Terminal
date: 2012/12/27 17:29:34
layout: post
path: "/personalice-terminal/"
category: "Imported from my older blog"
---
De vez en cuando colaboro con el blog Usemoslinux.blogspot.com aportando tutoriales, para que no se pierdan, los voy a poblicar también en este blog. En el de hoy os enseñaré a personalizar vuestro terminal al máximo, para que lo dejéis a vuestro gusto y que deje de hacer que vuestro ordenador parezca de los años 70: La dirección original del post es: <http://usemoslinux.blogspot.com/2012/09/personaliza-al-maximo-tu-terminal.html> Este tutorial va a constar de 4 partes, desde la más básica, de modificar las opciones del terminal directamente desde el menú del terminal "Preferencias del perfil" hasta la modificación del archivo .bashrc . ¡Empecemos! 

### Menú "perfiles" y "preferencias del perfil"

Linux nos da opciones para crear perfiles. Cada uno de estos perfiles tendrá unas opciones determinadas (color de fondo, tipografía, etc.) Para la mayoría de los usuarios, basta con utilizar el perfil por defecto, pero si necesitais crear más, podéis hacerlo pinchando en Editar > Perfiles... > Nuevo. En esa misma ventana podréis seleccionar qué perfil utilizar en cada momento. Una vez que tengáis el perfil que queráis cambiar, vais a Editar > Preferencias del perfil, donde se os mostrarán varias pestañas: 

  * **General:** Aquí podréis cambiar el tipo y el tamaño de letra a utilizar, el tamaño de la consola, el tipo de cursor, activar o desactivar el sonido, etc.
  * **Título y comando:** Aquí es posible personalizar el título del terminal, definir la acción a realizar cuando termina una orden, etc.
  * **Colores:** Esta es la pestaña que más juego da. Podéis cambiar el color del texto y del fondo, además de la paleta de colores que las diferentes órdenes pueden utilizar. Aunque vienen esquemas de color ya incluidos, os recomiendo que probéis la combinación que más os guste.
  * **Fondo:** En vez de tener un fondo de un solo color, esta pestaña os permite cambiarlo por una imagen de fondo. Escoged bien la foto, ya que si tiene colores parecidos a los que usáis en el texto, os resultará difícil de leer luego. Yo os recomiendo una imagen simple, de pocos colores y pocas figuras. También podéis poner cierto grado de transparencia.
  * **Desplazamiento:** Para cambiar la posición de la barra de desplazamiento y la cantidad de líneas que se pueden retroceder. Si utilizáis la terminal con programas que ejecutan muchos comandos os recomiendo que no pongáis desplazamiento ilimitado, vuestra memoria os lo agradecerá ;)
  * **Compatibilidad:** No toquéis nada de esta pestaña si no sabéis exactamente que estáis haciendo, pues cualquier cambio puede hacer que todo deje de funcionar como debería. Si todo te funciona bien hasta ahora, no cambies nada.

### Mostrar tu nombre o cualquier otro mensaje al abrir el terminal

En el último tutorial ([aquí](http://usemoslinux.blogspot.com/2012/09/copiar-archivos-en-el-terminal-en-forma.html)) mucha gente me pidió que les enseñara a mostrar unas letras en grande cada vez que se abra el terminal. Para conseguirlo, primero tendremos que instalarnos el programa **Figlet**. El cometido de este programa es transformar el texto que le pasemos como parámetro en un texto más interesante. Aquí os dejo un ejemplo: ![](http://4.bp.blogspot.com/-YWTDdjMCnW0/UFw-dqBOQTI/AAAAAAAABQw/wyTTipOAmzw/s640/Captura+de+pantalla+de+2012-09-21+12:16:09.png)

Para empezar tendremos que instalar varios programas de la siguiente manera:

Ubuntu:
    
    
    sudo apt-get install figlet cowsay fortune fortunes-es fortunes-es-off

Fedora: 
    
    
    sudo yum install figlet cowsay fortune fortunes-es fortunes-es-off

Una vez se haya instalado correctamente, vamos a nuestro directorio principal: 
    
    
    cd $HOME

y abrimos con gedit (o el editor de texto que prefieras) el archivo de configuración del terminal .bashrc: 
    
    
    gedit .bashrc

Ahora es muy importante que lo que contenga ese archivo no lo modifiqueis para nada. Todo lo que vamos a hacer es ir añadiendo filas al final. Para que os salga vuestro nombre con letras grandes escribid: 
    
    
    figlet Vuestro Nombre

Para que os salga una bonita vaca diciendo un mensaje escribid: 
    
    
    cowsay Vuestro mensaje

Mirad las opciones del comando cowsay (cowsay --help) para cambiar la apariencia de la vaca. Para que os salga un mensaje como en las galletas de la suerte chinas: 
    
    
    fortune

Guardad el archivo y abrid un terminal nuevo para ir comprobando el resultado. 

### Cambiar los colores del prompt

El prompt es el mensaje que da el terminal para indicarnos que está a la espera de órdenes (el clásico usuario@máquina:~$ ) Linux nos da la opción de personalizarlo, para ello tenemos que seguir editando el .bashrc, para saber como hacerlo, consulta este viejo artículo: <http://usemoslinux.blogspot.com/2010/11/terminales-con-estilo-personaliza-tu.html>

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
