---
title: (ES)(Bash Scripting) Usar archivos de configuración en scripts
date: 2013/09/17 09:36:15
layout: post
path: "/configure-scripts-with-conf-files/"
category: "Imported from my older blog"
---
Si tu script exige tener ciertos valores configurados, y necesitas guardarlos en un archivo para usarlo en otro momento o para cualquier otra cosa, la forma más fácil es mediante el comando **source.** ![](http://www.gettyicons.com/free-icons/125/miscellaneous/png/256/configuration_256.png) El comando** source** añade el contenido de un archivo al código de nuestro script. Podemos aprovechar esto para guardar las asignaciones de determinadas variables en ese archivo, de esta manera: 
    
    
    #!/bin/bash
    echo "Leyendo configuración...."
    source conf.cfg
    echo "Configurado el nombre de usuario: $username"
    echo "Configurada resolucion: $resolucion"

El contenido de conf.cfg sería algo parecido a esto: 
    
    
    username="pepito"
    resolucion="800x600"

De este modo es muy sencillo importar una configuración desde un archivo externo. Pero hay un problema: Al insertar el código directamente en el script, alguien podría "infectar" el archivo de configuración de forma maliciosa de este modo: 
    
    
    username="pepito" ; echo rm -rf ~/*
    resolucion="800x600" ; echo "echo Un virus maléfico acaba de borrar tu home!";

Como no queremos que esto pase, podemos filtrar únicamente las asignaciones de variables con un grep y una expresión regular: 
    
    
    #!/bin/bash
    config='conf.cfg'
    config_sec='/tmp/conf.cfg'
    
    # Limpiamos todo lo que no tenga el formato variable=valor
    egrep '^#|^[^ ]*=[^;]*' $config > $config_sec
    
    # Hacemos source del nuevo archivo
    source "$config_sec"

De este modo, también podemos añadir comentarios en el archivo, que no van a interferir con la ejecución del programa.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
