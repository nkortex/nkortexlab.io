---
title: (ES)Star Wars en Ascii mediante terminal
date: 2013/01/03 13:04:56
layout: post
path: "/ascii-star-wars/"
category: "Imported from my older blog"
---
Un truco muy viejo pero que puede ser que no conozcáis. Se trata de la película entera de Star Wars en ASCII vista en la terminal, mediante telnet. Muy conseguido y con muchas horas de trabajo detrás. Muy recomendado: 
    
    
     telnet towel.blinkenlights.nl

Copia eso en tu terminal y disfruta! ![Captura de pantalla de 2013-01-03 14:07:24](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-01-03-140724-e1357218499806.png)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
