---
title: (ES)(Tip) Actualizar el historial de bash justo después de ejecutar un comando
date: 2016/06/01 22:35:03
layout: post
path: "/inmediately-update-history/"
category: "Imported from my older blog"
---
Hoy os traigo un truco para forzar la actualización de historial de comandos de la terminal para que se realice justo después de ejecutar cualquier comando. Por defecto hay que cerrar la terminal y abrir otra nueva, lo cual es bastante molesto cuando sueles trabajar con muchas terminales a la vez. Para hacer esto añadimos a nuestro .bashrc la siguiente línea: 
    
    
    export PROMPT_COMMAND='history -a'

Esto ejecutará el comando history -a justo antes de imprimir el prompt por pantalla, lo que viene a significar que siempre tendremos el history actualizado. **BONUS:** Aumentar tamaño del historial: Actualmente el tamaño de almacenamiento es demasiado barato como para almacenar solo los 500 ultimos comandos. Si queremos almacenar más, debemos modificar la variable $HISTSIZE y establecerla al valor que estimemos oportuno. **BONUS2: **No guardar comandos repetidos: Si queremos cambiar el molesto comportamiento de guardar varias veces el mismo comando cuando lo ejecutamos muchas veces seguidas podemos añadir la línea: 
    
    
    export HISTCONTROL=erasedups

**BONUS3:** ignorar algunos comandos: Por último, si añadimos comandos separados por punto y coma en la variable $HISTIGNORE, no los almacenará en el historial al ejecutarlos. Por ejemplo, no nos sirve guardar los history así que podríamos hacer: 
    
    
    export HISTIGNORE="history"

Espero que sea útil ;) [Fuente](http://www.aloop.org/2012/01/19/flush-commands-to-bash-history-immediately/)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
