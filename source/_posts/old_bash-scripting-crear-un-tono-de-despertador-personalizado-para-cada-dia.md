---
title: (ES)(Bash Scripting) Crear un tono de despertador personalizado para cada día
date: 2013/02/11 22:24:31
---

Este post es algo largo, pero quería dejar constancia de el potencial que tienen nuestros aparatos de uso cotidiano (ordenador o móvil) y lo desperdiciados que los tenemos por no saber usar las herramientas adecuadas. Os voy a mostrar como podemos hacer que nuestro ordenador genere cada día un tono de despertador y que nos suene en el móvil a la hora deseada. Antes de empezar, unos avisos: 

  1. A la primera no va a salir, pero no os desaniméis
  2. Esta forma no es eficiente, pero es sencilla y cualquier persona sin grandes conocimientos puede  llevarla a cabo
  3. Si eres Cubero, mi antiguo profesor de programación, no sigas leyendo. El código tiene muchísimos defectos de formato que me harían suspender la asignatura que aprobé tiempo atrás.
![](http://www.lacoctelera.com/myfiles/anilusion/despertador-homero.jpg) Dicho esto, empezamos. Antes de nada, voy a hacer un resumen de lo que queremos conseguir: 

  1. Generar un mp3 que nos despierte con el mensaje que queramos, la hora, la temperatura actual y las temperaturas máxima y mínima de hoy. Se pueden añadir todos los mensajes que se quieran.
  2. Subirlo automáticamente a un servidor FTP. Hace falta tener uno. Además de para esto, tener un servidor propio es bastante aconsejable y no son muy caros. Si no queréis contratar uno, hay algunos gratuitos.
  3. Configurarlo para que se ejecute los días y las horas a las que quieres que suene.
  4. Configurar tu móvil Android para descargar todas las mañanas el mp3 resultante(opcional, puedes hacer que suene desde el ordenador)
  5. Configurar el despertador.
Manos a la obra: Nos creamos un script con gedit o con tu editor de texto favorito. Después del #!/bin/bash de rigor, vamos a descargarnos los datos de la temperatura desde Yahoo, pues tiene una API gratuita y bastante resultona. Esto lo hacemos con el código: 
    
    
    curl --silent "http://xml.weather.yahoo.com/forecastrss?w=761766&u=c" > tiempo

Ten en cuenta que el código 761766 corresponde a Granada, España. Si estás en otra ciudad tienes que cambiarlo. Puedes consultar tu código aqui: <http://es.tiempo.yahoo.com> . Una vez encuentres tu ciudad, el código lo tienes en la URL, por ejemplo: <http://es.tiempo.yahoo.com/espa%C3%B1a/andaluc%C3%ADa/granada-761766/> Otra idea que se me ocurre, sería descargar las noticias del día por RSS, los últimos tweets, lo correos electrónicos nuevos, los asuntos que tengamos planeados en Google Calendar,... en fin, esto ya lo dejo a vuestra imaginación. Una vez que tenemos los datos, creamos las cadenas de texto que luego pasaremos a mp3. En mi caso quedan así: 
    
    
    saludo="Buenos días Martín. Despierta que es de día!"
    hora="$(date +%I) y $(date +%M)"
    cadena1=$(echo La temperatura actual en Granada es de $(grep 'yweather:condition' tiempo | head -1 | cut -d\" -f6)ºC.) 
    cadena2=$(echo La temperatura mínima de hoy es de $(grep 'yweather:forecast' tiempo | head -1 | cut -d\" -f6)ºC) 
    cadena3=$(echo Y la máxima será de $(grep 'yweather:forecast' tiempo | head -1 | cut -d\" -f8)ºC)

Esto almacena en la variable **saludo** una frase estándar que sonará siempre. Luego guarda en la variable **hora** la hora a la que se ejecuta; y en las otras 3 variables, la temperatura máxima, mínima y actual, buscando en el archivo que nos bajamos al principio y recortando con cut para quedarnos con el trozo que nos interesa. Si vamos a usar mas datos, se meterían aquí. Ahora vamos a convertirlo a mp3. Para ello vamos a usar el traductor de Google. Es un método un poco rudimentario, pues no está pensado para este propósito y no acepta cadenas de texto excesivamente largas (ojo con esto) pero el resultado de voz es de lo mejorcito que hay. También se podría hacer con el programa "espeak" pero cada vez que te despiertes pensarás que ha habido una invasión robot, pues el resultado no queda muy humano. El código sería, mas o menos este: 
    
    
    wget -F -U "Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:2.0) Gecko/20110307 Firefox/4.0" "http://translate.google.com/translate_tts?tl=es&q='$saludo'" -O saludo.mp3;
    wget -F -U "Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:2.0) Gecko/20110307 Firefox/4.0" "http://translate.google.com/translate_tts?tl=es&q='$hora'" -O hora.mp3;
    wget -F -U "Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:2.0) Gecko/20110307 Firefox/4.0" "http://translate.google.com/translate_tts?tl=es&q='$cadena1'" -O cadena1.mp3;
    wget -F -U "Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:2.0) Gecko/20110307 Firefox/4.0" "http://translate.google.com/translate_tts?tl=es&q='$cadena2'" -O cadena2.mp3;
    wget -F -U "Mozilla/5.0 (X11; U; Linux x86_64; pl-PL; rv:2.0) Gecko/20110307 Firefox/4.0" "http://translate.google.com/translate_tts?tl=es&q='$cadena3'" -O cadena3.mp3

Le pasamos la cadena al traductor, y guardamos el resultado en mp3. Ahora, mediante sox, unimos todos los mp3 generados en uno nuevo llamado despertador.mp3 asi: 
    
    
    sox saludo.mp3 hora.mp3 cadena1.mp3 cadena2.mp3 cadena3.mp3 despertador.mp3

Si sox te da un error en el formato, pásate por este post que te digo como se arregla: <http://023.es/error-sox-fail-formats-no-handler-for-file-extension-mp3-como-solucionarlo/> (opcional)Podemos cambiar las etiquetas ID3 del mp3 resultante para que nos sea más fácil de encontrar luego: 
    
    
    id3v2 -a"Artista" -t"Titulo" despertador.mp3

Con esto, ya tendríamos la base terminada. Pasamos al segundo paso: Subirlo a nuestro servidor ftp. Para esto vamos a usar wput. Creo que no viene instalado de primeras en ninguna distribución, así que hacemos un yum o un apt-get y lo instalamos. Ahora hacemos: 
    
    
    wput despertador.mp3 ftp://USUARIO:PASS@123.123.123.123:1234 /despertador/

Cambiando Usuario y pass por lo que corresponda, 123.123.123.123 por la IP del server y el 1234 por el puerto, así como el directorio donde se subirá (en este caso a despertador) Luego borramos todos los archivos que hemos usado, para no ir acumulando basurilla poco a poco: 
    
    
    rm saludo.mp3 cadena1.mp3 cadena2.mp3 cadena3.mp3 despertador.mp3 hora.mp3 tiempo

Una vez hecho esto, vamos a hacer una tarea cron para ejecutarlo de lunes a viernes a las 8 (por ejemplo). 
    
    
    0 8 * * 1-5 bash /home/admin/scripts/despertador

(modificar a gusto del consumidor) Ok, a partir de ahora, todos los días de Lunes a Viernes a las 8 y unos minutos, tendremos un mp3 esperándonos en nuestro servidor FTP. Ahora nos bajamos [BotSync](https://play.google.com/store/apps/details?id=com.botsync) desde Google Play. Esta aplicación nos permite sincronizar una carpeta por ftp cada cierto tiempo. Lo configuramos correctamente (no es difícil)  y establecemos la melodía del despertador a esa que nos acabamos de bajar. Como esta canción no cambia de nombre, la aplicación del despertador siempre la encuentra y no nos quedaremos dormidos (En caso de que se nos haya caído internet, nos levantaremos con la del día anterior). Si en vez de en el movil prefieres que suene en el ordenador directamente, es fácil: añade al final del script una llamada a mplayer: 
    
    
    mplayer despertador.mp3

Os dejo el código completo, por si os habéis perdido o no tenéis ganas de programar nada: 
    
    
    #!/bin/bash
    
    #Basado en el trabajo de Juan Mol y Rumoku
    
    #Descargo datos de Yahoo Weather
    curl --silent "http://xml.weather.yahoo.com/forecastrss?w=761766&u=c" > tiempo
    #Creo las cadenas con frases. No pueden ser largas o sino no funcionan debido a la longitud de cabecera.
    saludo="Buenos días Martín. Despierta que es de día!"
    hora="$(date +%I) y $(date +%M)"

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Rubén López](#146 "2013-07-26 06:47:49"):** Esta bien ese script! Tanto, qye me suena mucho... anda! Claro, de un blig de Raspberry. Podrias como minimo poner de donde li has sacado y dar las gracias, no? Bueno. Es cierto que tu pusiste lo del ftp y Rumoku puso lo del weather yahoo. Pero no esta de mas dar las gracias.

**[admin](#148 "2013-07-26 09:04:46"):** Hola Rubén, la verdad es que me basé en varias fuentes, y la idea del despertador ya la tuvo alguien. Suelo especificar la fuente de donde saco el contenido ( De hecho en el script pone "basado en el trabajo de Juan Mol"), pero si me pasas un enlace del blog que me comentas lo añado inmediatamente. Un saludo y gracias por tu visita ;)

**[Rubén López](#197 "2013-08-08 08:01:04"):** Es cierto! No vi que si haces scroll, ahi escondidito sale basado en el trabajo de Juan Mol Es cierto. El post es como sabras este, pero la gran parte que has copiado no es de Juan Mol sino del comentario de Rumoku. Has dejado hadta el "despierta que es de dia!". Celebro wue te sirviera, pero es de bien nacido ser agradecido y deberias añadir a Rumoku en esas "gratitudes" que saken si haces scroll d

**[Rubén López](#198 "2013-08-08 08:03:20"):** Olvidé pegar link por si alguien le interesa ver el original y vea si es cierto lo que digo. http://rsppi.blogspot.com.es/2012/06/generador-mp3-para-despertador.html?m=1

**[admin](#201 "2013-08-10 09:46:09"):** Gracias por el enlace, tienes razón. Ya he añadido la fuente. Una saludo ;)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
