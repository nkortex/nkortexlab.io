---
title: (ES)Añadir usuario desde terminal y añadirlo a sudo
date: 2013/05/01 11:38:58
layout: post
path: "/create-sudo-user-from-cli/"
category: "Imported from my older blog"
---
![](http://images.all-free-download.com/images/graphiclarge/user_add_5006.jpg) Hay veces que no tenemos acceso a interfaz gráfica en un sistema(por ejemplo, si accedemos mediante ssh) y necesitamos crear un nuevo usuario. Para ello usamos este comando: 
    
    
    sudo adduser nombre-del-usuario

Luego, para asignarle una contraseña escribimos: 
    
    
    sudo passwd nombre-del-usuario

Escribimos la nueva contraseña 2 veces y listo. Ahora vamos a añadirlo a la lista de usuarios que tienen permitido tener acceso root mediante el comando sudo. Para ello editamos el archivo /etc/sudoers: 
    
    
    sudo vi /etc/sudoers

Buscamos la línea: 
    
    
    root ALL=(ALL) ALL

Y añadimos debajo: 
    
    
    nombre-del-usuario ALL=(ALL) ALL

Cambiando nombre-del-usuario por el nombre asignado.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
