---
title: (ES)Sincronización multiplataforma con BitTorrent Sync
date: 2013/09/14 08:46:24
layout: post
path: "/sync-with-bittorent/"
category: "Imported from my older blog"
---
Hace ya unos meses, los creadores de **Bittorrent** publicaron una nueva herramienta que permite sincronizar carpetas y archivos entre varios ordenadores: **Bittorrent Sync**. La principal diferencia con otros servicios como Dropbox, Copy, Drive, etc... es que no hay servidor central; es decir: no subes tus archivos a un servidor para bajarlos luego, sino que se transmiten los datos vía **torrent** directamente entre los ordenadores conectados. ![](http://www.eliassierra.com/wp-content/uploads/2013/07/original.png) Para los obsesionados con la privacidad, puede ser una buenísima opción. Además, tiene versión Android y no tiene límite de transferencia, el único límite es la capacidad de tu disco duro. Para usarlo, descargamos el archivo adecuado desde aquí: <http://labs.bittorrent.com/experiments/sync.html> No necesita instalación. Simplemente lo ejecutamos, abrimos el navegador y vamos a la dirección: 0.0.0.0:8888 Esta es la dirección de la interfaz por defecto, se puede cambiar. Para compartir una nueva carpeta pinchamos en "**Add Folder**", seleccionamos la carpeta a compartir, generamos la clave pulsando en "**generate**" y finalizamos con **Add**. Luego vamos al ordenador que sincronizará la carpeta, abrimos la misma dirección, le damos a "**add folder**" y, en vez de generar la clave, introducimos la que nos generó antes. Seleccionamos la carpeta donde se copiará, y listo! Como veis, alcanza velocidades de sincronización que difícilmente las encontrarás en dropbox y compañía

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
