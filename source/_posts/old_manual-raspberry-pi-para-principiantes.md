---
title: (ES)Manual Raspberry Pi para principiantes
date: 2013/01/06 11:18:55
layout: post
path: "/raspberry-tutorial/"
category: "Imported from my older blog"
---
Hoy os traigo un PDF escrito por profesores de "computing at school" en el que se enseña a usar la RaspberryPi  para diversos fines. Te enseña a programar en Phyton, a programar y a animar en Scratch, a usar la línea de comandos, ... ![Captura de pantalla de 2013-09-14 10:04:16](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-09-14-100416.png) Muy recomendable, pero está en inglés. Aquí lo tenéis: <http://pi.cs.man.ac.uk/download/Raspberry_Pi_Education_Manual.pdf> Se distribuye bajo una licencia CC así que disfrútalo y compártelo.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
