---
title: (ES)Como borrar partes de fotografías de manera inteligente desde Gimp
date: 2013/02/06 20:16:33
layout: post
path: "/erase-elements-from-images-with-gimp/"
category: "Imported from my older blog"
---

Una extensión muy interesante de Gimp es Gimp Resynthesizer, que trata de borrar partes sobrantes de una fotografía de manera inteligente, de manera que no se note el cambio. El resultado puede llegar a ser espectacular: ![](http://www.jesusda.com/blogpics/howto-gimp/19-gimp-resynthesize/gimp-resynthesizer-03.jpg) Y como conseguimos llegar a hacer esto? Pues muy fácil, hacemos un yum install ( o apt-get install si estás en ubuntu o derivados) e instalamos: 
    
    
    gimp-resynthesizer

Ahora abrimos la imagen que queramos, seleccionamos la parte a eliminar (no hace falta ser muy precisos) vamos a filtros -> Realzar -> Smart Remove Selection. Luego pulsamos ok y esperamos... Y magia! :D Yo lo he probado con esta foto: Antes: ![Captura de pantalla de 2013-02-06 21:27:17](/wp-content/uploads/2013/02/Captura-de-pantalla-de-2013-02-06-212717-e1360188671265.png) Después: ![Captura de pantalla de 2013-02-06 23:08:30](http://023.es/wp-content/uploads/2013/02/Captura-de-pantalla-de-2013-02-06-230830-e1360188703486.png)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[eckelon](#212 "2013-08-16 09:57:24"):** Madre mía, y yo haciendo estas cosas con el tampón de clonar y la tecla control, como un troglodita! Tengo que probar esta brujería XD

**[eckelon](#213 "2013-08-16 09:58:11"):** Madre mía, y yo usando el tampón de clonar y la tecla control para hacer esas cosas. Tendré que probar esta brujería XD

**[eckelon](#214 "2013-08-16 09:59:14"):** Ups, el captcha este me ha jugado una mala persona y lo he publicado dos veces, sorry!

**[admin](#220 "2013-08-22 07:46:51"):** La verdad es que por momentos asusta bastante que sea capaz de borrar de esa forma x)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
