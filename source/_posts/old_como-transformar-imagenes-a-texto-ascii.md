---
title: (ES)Como transformar imágenes a texto Ascii
date: 2013/04/02 22:03:04
layout: post
path: "/image-to-asciiart/"
category: "Imported from my older blog"
---
Bueno, el tutorial de hoy se puede definir de muchas maneras, pero creo que la más acertada es: inútil. Aún y así, os voy a explicar como pasar vuestras imágenes a texto gracias al terminal y podréis obtener unos resultados tan asombrosos como estos: ![1](/wp-content/uploads/2013/04/1.jpg)![2](http://023.es/wp-content/uploads/2013/04/2.png) Quien puede vivir sin su caquita de Whatsapp en formato Ascii? Nadie verdad? pues os voy a enseñar como hacerlo. Para ello vamos a usar la librería **libcaca** y el paquete **caca-utils **. Suena a broma, pero no lo es, de verdad se llaman así, por eso he escogido esa imagen de ejemplo. La librería libcaca suele venir con todas las distros, y sino estará en los repositorios. Sino la tienes, se instala así: 
    
    
    sudo yum install libcaca caca-utils

Y su uso básico es: 
    
    
    img2txt -W 60 -x 3 -y 5 -f html fotooriginal.jpg > htmlresultante.html

donde: 

  * W es el ancho resultante (en letras, no en pixeles)
  * -x y -y ancho y alto de cada letra
  * -f formato de salida (consultar más formatos en la ayuda)
Además de esto, este programa tiene más usos, probad a ejecutar en un terminal estos comandos: 
    
    
    cacademo

(Enter para ver más ejemplos) 
    
    
    cacafire

Uno de los muchos usos que le veo es juntar esto mas nuestro .bashrc . Pueden salir cosas interesantes pero ahora no se me ocurre nada. Cuando piense algo lo publicaré :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
