---
title: (ES)(Bash Scripting) Capturar señal ctrl+C y cambiar su comportamiento
date: 2013/06/29 11:11:29
---
Muchas veces (la mayoría) los script utilizan archivos y otras herramientas del sistema para funcionar, y es necesario limpiar esos archivos una vez acabado el script. Pero, ¿que pasa si el usuario fuerza el cierre del script antes de que acabe con un ctrl+c? pues el truco es capturar la señal de finalización y cambiar su comportamiento. De este modo se ejecutará una última función antes de acabar. Veamoslo en un ejemplo: 
    
    
    #!/bin/bash
    
    #-------
    #Este "util" script cuenta ovejas y las almacena en archivos separados
    #-------
    #Funcion que elimina los archivos generados
    function limpia(){
       rm -f archivo_oveja*.txt
       echo "Saliendo..."
       exit 0
    }
    
    for x in `seq 1 100`
    do
       echo "Oveja numero $x" > archivo_oveja_$x.txt
       cat archivo_oveja_$x.txt
       sleep 2
    done
    
    limpia

Como se puede ver, una vez cuenta hasta 100 y genera los 100 archivos correspondientes, el script llama a la función limpia(), que elimina los archivos generados, indica que va a salir y hace un exit 0, pero si el usuario se cansa de esperar y decide salir, todos los archivos se quedarán, y pueden interferir en una ejecución posterior. Para evitar esto incluimos un trap que "atrape" la señal enviada por el usuario y que llame a la funcion limpia() antes de tiempo: 
    
    
    #!/bin/bash
    
    #Este util script cuenta ovejas y las almacena en archivos separados
    
    function limpia(){
    	rm -f archivo_oveja*.txt
    	echo "Saliendo..."
    	exit 0
    }
    
    # atrapa ctrl-c y llama a la funcion limpia()
    trap limpia INT
    
    for x in `seq 1 100`
    do
    	echo "Oveja numero $x" > archivo_oveja_$x.txt
    	cat archivo_oveja_$x.txt
    	sleep 2
    done
    
    limpia

Ejecutad ambos y veis la diferencia. Hasta la próxima :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
