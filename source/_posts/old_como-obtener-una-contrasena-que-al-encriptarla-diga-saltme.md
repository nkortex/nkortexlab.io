---
title: (ES)Como obtener una contraseña que, al encriptarla diga SaltMe
date: 2014/04/23 08:59:53
layout: post
path: "/saltme-password/"
category: "Imported from my older blog"
---

Dándome una vuelta por StackOverflow y derivadas, me he encontrado con una pregunta de lo más interesante: [Como crear una contraseña que, al encriptarla diga: SaltMe](http://security.stackexchange.com/questions/56371/how-can-i-create-a-password-that-says-salt-me-when-hashed) El usuario quiere conseguir una contraseña que , al introducirla en algún servicio online poco seguro (Que no aplique Salt a los passwords antes de encriptarlos con MD5) el resultado de la encriptación empiece por los caracteres SALTME. De este modo, si un hipotético  administrador viera esa contraseña en la base de datos se decidiría a cambiar el tipo de seguridad. Para conseguir esto hay que generar muchísimas posibles contraseñas, encriptarlas, codificarlas en Base64 y comprobar si el resultado es bueno. Si quisiéramos tener en los 16 bytes que componen el hash una frase completa sería muuy largo de calcular, pero si solo queremos que coincidan los primeros 8 bytes, la cosa se hace más fácil. Un usuario lo consiguió en unas horas: 
    
    
    infjfieq  -> SALTMEnBrODYbFY0c/tf+Q==
    lakvqagi -> SaltMe+neeRdUB6h99kOFQ==

Ahora, me pongo a pensar y esto tiene varias desventajas: Primera: A partir de ahora las contraseñas que generen este tipo de mensajes se incluirán en los diccionarios de fuerza bruta, lo que es contraproducente para nuestra seguridad. Segunda: Nadie en sus sano juicio se pone a leer las contraseñas encriptadas de sus usuarios para ver si tienen mensajes ocultos. Pero, en fin, todo sea por saber que se puede hacer, aunque no valga para nada xD

<!-- PELICAN_END_SUMMARY -->
## Comments

**[erm3nda](#463 "2016-08-21 01:05:35"):** Nadie en sus sano juicio se pone a leer las contraseñas encriptadas de sus usuarios para ver si tienen mensajes ocultos. Los que están en su sano juicio usan grep y un loop para encontrar cosas.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
