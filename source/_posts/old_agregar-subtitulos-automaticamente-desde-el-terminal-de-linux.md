---
title: (ES)Agregar subtítulos automáticamente desde el Terminal de Linux
date: 2012/12/23 12:35:58
layout: post
path: "/add-subtitles-from-cli/"
category: "Imported from my older blog"
---

Hay gente, demasiada gente, que sigue teniéndole miedo al terminal. No lo comprendo, ya que te puede ayudar a automatizar tareas para que acabes mucho antes. Hoy os voy a enseñar a poner subtítulos a los videos con mencoder y a crearos un script para que los añada automaticamente a todos los capítulos de una serie. Empecemos: Antes de nada, tenemos que tener instalado el programa "mencoder". Este se encuentra en los repositorios de las distros más importantes. Una vez que lo tengamos, hacemos un cd a la carpeta donde tengamos el vídeo y los subtítulos y ejecutamos: 

```bash
mencoder "Pelicula.avi" -sub "Subtítulos.srt" -oac copy -ovc lavc -o "Salida.avi" -subcp utf-8 -subfont-text-scale 2.8; 
```

Donde: 

  * "Pelicula.avi" es la película sin subtítulos (Formato .avi o cualquier otro que acepte mencoder)
  * "Subtitulos.srt" es el archivo de subtítulos
  * "Salida.avi" es el archivo que devolverá el comando, con los subs incrustados
  * utf-8 es la codificación que usará (Si se ven caracteres extraños prueba con latin1 o latin2)
  * Y 2.8 es el tamapo que tendrá la fuente

![Screenshot from 2012-12-23 13:21:04](/wp-content/uploads/2012/12/Screenshot-from-2012-12-23-132104-300x168.png)

Hasta aquí creo que es bastante fácil. Ahora viene construir un Script para que se nos peguen los subtítulos a todos los episodios de una serie. Yo voy a usar de ejemplo la primera temporada de Breaking Bad. Abrimos un editor de texto y pegamos lo siguiente: 
    
    
    #!/bin/bash
    
    cd /run/media/admin/EXTERNO/Series/Breaking\ Bad/Breaking\ Bad\ s01
    
    for x in `seq 1 7`; do
    
    mencoder "Breaking Bad.s01e0$x*.avi" -sub "Breaking Bad - S01E0$x*.srt" -oac copy -ovc lavc -o "episodio$x.avi" -subcp utf-8 -subfont-text-scale 2.8;
    
    done

El funcionamiento del script es básico: 

  1. Cambia de carpeta para situarnos en la carpeta donde están tanto la serie como los subtítulos
  2. Entra en un bucle desde 1 hasta , en mi caso 7. En tu caso, cambia este número por la cantidad de capítulos que tengas.
  3. Ya dentro del bucle ejecuta el comando que vimos antes pero modificando tanto las entradas como las salidas según el numero de interacción en la que nos encontremos. Esto se consigue cambiando el numero del capitulo por $x
Los mas expertos podrían hacer un programa usando esto y Zenity, pidiendo el numero de capitulos que tendrá, la carpeta donde están los capitulos y tal. Si tengo tiempo lo hago y os lo subo :) En el próximo post, hablaré de como añadir subtítulos automáticamente con el programa VLC y un plugin adicional

<!-- PELICAN_END_SUMMARY -->
## Comments

**[dirtykk000](#115 "2013-06-29 22:23:04"):** hola compañero, utilizare este metodo para incrustar mis pelis con subtitulos, ya que uso DEVEDE y al crear el ISO para quemarlo, me manda error del maldito "spumux" , googleando encontre que era un problema con los subtitulos, si es así entonces los incrustare con tu metodo. saludos! !

**[admin](#117 "2013-06-30 09:59:37"):** Me alegro de que te sea útil la información, cuéntame como te ha ido cuando lo hagas. Un saludo y gracias por la visita :)

**[dirtykk000](#123 "2013-07-02 00:59:28"):** hola camarada, he incrustado con exito los subtitulos a la pelicula, pero la calidad del video se redujo, ya que se ve un poco pixeleada la imagen, el formato original del video mp4 y pesa 1.8gb , y de salida le puse avi y termino pesando 3.5gb; cambie una opcion al incrustar los subtitulos: -oac copy por: -oac pcm Con la primera decia que no era compatible el audio del video y me sugirio el mismo progama que intentara con pcm. como ves valedor?



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
