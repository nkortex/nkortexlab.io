---
title: (ES)Utilizar Twitter desde un terminal con Ttytter
date: 2013/05/12 09:02:39
layout: post
path: "/twitter-from-cli/"
category: "Imported from my older blog"
---

Ttytter es un programa escrito en perl que permite utilizar todas las características de twitter (Mandar Tweets, DM, ver tu TL,...) directamente desde el terminal. No es estético, pero puede servir si quieres automatizar de algún modo el uso que haces de twitter. Lo primero que tenemos que hacer es descargarlo: 
    
    
    wget http://www.floodgap.com/software/ttytter/dist2/2.1.00.txt

Lo ejecutamos (si queréis cambiarle el nombre por otro más descriptivo, podéis hacerlo): 
    
    
    perl 2.1.00.txt

Ahora nos dará una introducción y nos guiará por el proceso de loggeo en twitter. Esto solo se hace la primera vez (Una vez por cada cuenta que tengas). Solo accedes a una web que te indica desde el navegador. Copias el código de acceso que te facilita Twitter y lo pegas en el terminal. Ya está configurado. Ahora vamos al lío: Publicar nuestro primer tweet. Para ello volvemos a ejecutar el script perl igual que antes. Ahora cogerá los credenciales automáticamente y empezará a mostrar tu TL. Para publicar un tweet solo tienes que escribirlo y darle a enter: ![Captura de pantalla de 2013-05-12 10:57:46](/wp-content/uploads/2013/05/Captura-de-pantalla-de-2013-05-12-1057461.png) ![Captura de pantalla de 2013-05-12 10:58:05](http://023.es/wp-content/uploads/2013/05/Captura-de-pantalla-de-2013-05-12-105805.png)   Otra forma de publicar tweets sería pasándole el contenido como parámetro: 
    
    
    echo "Probando, probando" | perl ttytter.txt

Si vas a usar varios usuarios, al iniciar, tienes que indicar el usuario con la opción -keyf="archivokey" Espero que os sea útil :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[obochaman](#263 "2013-10-21 17:15:23"):** Gracias! me sirvio :D

**[admin](#264 "2013-10-21 17:56:45"):** Me alegro! Gracias por tu comentario!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
