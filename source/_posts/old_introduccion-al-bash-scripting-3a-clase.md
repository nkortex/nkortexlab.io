---
title: (ES)Introducción al Bash Scripting, 3ª Clase
date: 2013/09/16 15:15:32
layout: post
path: "/bash-intro-3/"
category: "Imported from my older blog"
---

Nueva lección del PDF “Introducción al Bash Scripting”, y ya vamos por la tercera. En este caso, hablo sobre los comandos básicos que debemos conocer para usar la terminal correctamente y para utilizarlos en nuestros scripts, con sus respectivos ejemplos de uso. Contenido: • Comandos básicos (I): ◦ man ◦ pwd ◦ cd ◦ mkdir ◦ ls ◦ cp/mv/rm ◦ echo ◦ cat ◦ head/tail ◦ grep ◦ cut ◦ sort Este PDF, al igual que los anteriores, tiene una licencia Creative Commons Reconocimiento-NoComercial-CompartirIgual 3.0 Unported. Con esta licencia tienes permiso para copiar y distribuir el contenido como quieras, siempre que cites al autor original y no lo uses con fines comerciales: http://www.mediafire.com/view/5bgzzz8j89ytcjd/3.pdf

 Enlaces a lecciones anteriores: 

  * Primera: <http://www.mediafire.com/view/rg3y7m686yxab3p/1.pdf>
  * Segunda: <http://www.mediafire.com/view/fcpg1bp7x4ubafm/2.pdf>

<!-- PELICAN_END_SUMMARY -->
## Comments

**[RoNiStark](#235 "2013-09-16 23:51:29"):** Aunque ya tengo algunas bases de programación shell, siempre se agradece el reforzar los conocimientos. Gracias!!!

**[admin](#236 "2013-09-17 09:08:23"):** Gracias a ti por la descarga! Seguiré publicando lecciones poco a poco

**[José Luis](#237 "2013-09-17 10:58:01"):** A ver a hasta donde llego con esto. Solo he hecho algunas cositas.

**[admin](#238 "2013-09-17 11:18:12"):** Poco a poco! x)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
