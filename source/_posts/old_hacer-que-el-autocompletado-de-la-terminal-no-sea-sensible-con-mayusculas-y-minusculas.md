---
title: (ES)Hacer que el autocompletado de la terminal no sea sensible con mayúsculas y minúsculas
date: 2013/01/09 19:50:30
layout: post
path: "/disable-uppercase-sensibility-terminal/"
category: "Imported from my older blog"
---

No es algo que marque un antes y un después en vuestras vidas, pero es un tip muy práctico. Hasta ahora si estábamos en nuestro home y queríamos movernos a descargas hacíamos: 
    
    
    cd Des + tabulador

pero sin embargo, si hacíamos esto: 
    
    
    cd des + tabulador

no nos autocompletaba el nombre de la carpeta, pues esta empieza con mayúscula y nosotros la escribíamos en minúscula. Lo único que tenemos que hacer para solucionar este problema es ejecutar este comando: 
    
    
    cd ~ && echo "set completion-ignore-case on" >> .inputrc

Ahora ya solo nos queda abrir un terminal nuevo y comprobar que funciona correctamente. (Tutorial de mis aportaciones a UsemosLinux)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Alejandro](#261 "2013-10-21 13:20:07"):** Excelente!! Me ahorrará muchas correcciones!

**[admin](#266 "2013-10-21 17:58:03"):** Me alegro que sea de utilidad! ;)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
