---
title: (ES)Administrar SQLite por medio de una interfaz web con PhpLiteAdmin
date: 2014/03/01 10:27:45
layout: post
path: "/sqlite-web-interface-phpliteadmin/"
category: "Imported from my older blog"
---
Para los que estáis acostumbrados a PHPMyAdmin para la gestión de bases de datos MySQL os traigo un proyecto que pretende facilitar la gestión de bases de datos SQLite mediante una interfaz web sencilla. Este proyecto es [PhpLiteAdmin](https://code.google.com/p/phpliteadmin/). ![Captura de pantalla de 2014-03-01 11:18:10](/wp-content/uploads/2014/03/Captura-de-pantalla-de-2014-03-01-111810.png) Para utilizarlo, simplemente descargamos el zip correspondiente a la última versión y extraemos tanto el fichero de configuración como el archivo phpliteadmin.php en la carpeta donde tengamos nuestra base de datos (o donde queramos crearla). Ahora tenemos que levantar un servidor web en nuestra máquina. Si no tenéis ninguno, recomiendo instalar lampp ([aquí tenéis un PDF](https://www.mediafire.com/?hhzhcqoahkhnjh9) donde explico como hacerlo en un servidor centOS y por internet hay muchos tutoriales Ya sólo nos queda visitar el archivo desde nuestro navegador: 
    
    
    http://localhost/carpeta/phpliteadmin.php

![Captura de pantalla de 2014-03-01 11:09:28](/wp-content/uploads/2014/03/Captura-de-pantalla-de-2014-03-01-110928.png) Como veis, nos pide una contraseña. Por defecto es admin, pero si vais a permitir el acceso remoto conviene que la cambiéis en el archivo de configuración. Ojo, este archivo almacena la contraseña en texto plano, así que cuidadico con los permisos. En este archivo podemos modificar otras opciones interesantes para según que usos. Una vez introducida la contraseña, nos permite crear nuevas bases de datos, editarlas, eliminarlas, incluir tablas, ejecutar SQL, consultar datos,... Espero que sea de utilidad. Nota: comentar es gratis ;)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
