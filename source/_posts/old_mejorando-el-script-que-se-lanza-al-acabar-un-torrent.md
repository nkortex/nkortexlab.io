---
title: (ES)Mejorando el script que se lanza al acabar un torrent
date: 2013/02/03 13:10:51
layout: post
path: "/script-for-finishing-torrents/"
category: "Imported from my older blog"
---

En un post anterior ( aqui: <http://023.es/notificacion-por-mail-o-sms-cuando-se-descarga-un-torrent/> ) os expliqué como podíamos hacer para que cada vez que acabara de bajarse un torrent, nos mandara un correo electrónico y un SMS al teléfono. Ahora, partiendo de la base de ese script, voy a mejorarlo para que de mas detalles en el mensaje enviado. Hasta ahora mandaba solo el nombre del torrent, ahora voy a añadir unas cuantas mejoras. El script que venía usando hasta ahora, no era una pieza compleja de software, como podéis ver: 
    
    
    #!/bin/bash
    echo "$TR_TORRENT_NAME descargado ok" | mutt -s "Nuevo Torrent Descargado" ejemplo@023.es

por lo tanto, esta mejora tampoco va a ser muy difícil. Lo primero que hacemos es saber que datos queremos que nos incluya en el mensaje: 

  * Hora a la que terminó de bajarse
  * Tamaño del archivo
  * El ratio de descarga total
La hora local cuando se lanza el script la tiene transmission en la variable: **TR_TIME_LOCALTIME **, así que podemos usarla directamente como lo hicimos con el nombre. Para el tamaño del archivo, podemos averiguarlo con la instrucción: 
    
    
    ls -l -s -k

Para el ratio de descargas, captamos los datos desde el fichero stats.json, lo modificamos para que sea más fácil de leer y dividimos bytes enviados entre bytes recibidos.   El resultado final sería este: 
    
    
    #!/bin/bash
    
    #~/.config/transmission/aviso_torrent
    
    ############################## CONTROL DEL TAMAÑO ###########################################
    
    #Calculamos el tamaño
    tam=$(ls -s -k -h $TR_TORRENT_DIR | grep $TR_TORRENT_NAME | cut -d " " -f 1);
    
    ############################## CONTROL DEL RATIO ###########################################
    
    #Preparamos el archivo de configuración para que nos sea más fácil trabajar con el quitando comas, texto y dejando solo los datos
    cat $TRANSMISSION_HOME/stats.json | cut -d ":" -f 2 | cut -d "," -f 1 | cut -d " " -f 2 > stats.txt
    
    #Guardamos los bytes recibidos/enviados. En este caso no es necesario pasarlo a otra medida, porque vamos a calcular el ratio
    #Recibidos -> linea 2 Enviados -> linea 6
    
    enviados=$(cat stats.txt | head -n 6 | tail -n 1);
    recibidos=$(cat stats.txt | head -n 2 | tail -n 1);
    
    ratio=$(echo "$enviados / $recibidos" | bc -l);
    
    echo "$TR_TORRENT_NAME ( $tam ) descargado a las $TR_TIME_LOCALTIME . Ratio: $ratio ." | mutt -s "Nuevo Torrent Descargado" ejemplo@023.es
    
    rm stats.txt

Lo cual manda un mensaje con este formato: ![Captura de pantalla de 2013-02-03 16:53:55](/wp-content/uploads/2013/02/Captura-de-pantalla-de-2013-02-03-165355.png) Que lo disfrutéis!

<!-- PELICAN_END_SUMMARY -->
## Comments

**[adrian](#337 "2014-04-02 05:23:24"):** Saludos amigo, disculpa queria preguntarte sobre las variables que podria usar en mi script, lo que pasa es que tengo un script que me manda un correo una vez terminado un torrent pero el problema es que cuando manda el correo, jamas me escribe lo que contiene la variable del nombre del torrent, es como si las variables estuvieran vacias porque la del directorio tampoco, no entiendo si tengo que agregar algo en la configuracion del transmission-daemon para usarlas o que tengo que hacer. gracias. FECHA=$(date) TMPFILE="/tmp/transmission_mail" echo -e "To:*****@gmail.com\nSubject: torrent\n\nTermino un torrent\n\n/* $FECHA*/\nTransmission termino de descargar \ $TR_TORRENT_NAME \ en $TR_TIME_LOCALTIME" > $TMPFILE ssmtp -vvv *******@gmail.com < $TMPFILE Tienes alguna idea de como corregir que si tengan el contenido las variables.

**[rafa](#440 "2016-01-18 10:10:47"):** pa k kieres saber eso jaja saludos



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
