---
title: (ES)Como desinstalar los Drivers privativos de Nvidia en Fedora 18 sin morir en el intento
date: 2013/01/22 16:03:06
layout: post
path: "/remove-drivers-nvidia-fedora/"
category: "Imported from my older blog"
---

Bueno, pues como todo el mundo sabe, los drivers de nVidia para linux son una patata, que estropean mas que arreglar. Aún y así, intenté darles una oportunidad a ver si en Fedora 18 habían mejorado algo. Fallo mio. Al reiniciar no iniciaba el servidor X, dándome 1000 millones de errores. Al final, he encontrado como solucionarlo, así que os lo pongo aquí por si a vosotros os pasa lo mismo (o si me pasa a mi, acordarme de como se hace xD) ![NVidiaLogo](/wp-content/uploads/2013/01/NVidiaLogo.jpg) Que quede claro que no me responsabilizo de los destrozos que esto pueda ocasionar, pero lo más probable es que funcione. Para empezar, arrancamos el ordenador hasta que se quede pillado porque no puede iniciar el servidor X. Pulsamos **Ctrl+Alt+F3 **y entramos como root. Ahora cambiamos al nivel de ejecución 3 con: 
    
    
    init 3

Eliminamos todos los paquetes instalados que tengan algo que ver con nVidia: 
    
    
    sudo yum remove *nvidia*

Borramos el archivo /etc/X11/xorg.conf: (puede que ya se haya borrado, pero así nos aseguramos) 
    
    
     sudo rm /etc/X11/xorg.conf

Instalamos los drivers de mesa: 
    
    
     sudo yum install mesa*

Reiniciamos y listo! :D (Aunque no lo creáis, la imagen del artículo la he modificado yo, no he contratado a ningún diseñador gráfico para ese trabajo. Lo digo porque como ha quedado tan bien, podríais pensar lo contrario xD)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Miguel](#15 "2013-03-17 04:11:40"):** hola mi hermano muchisimas gracias por tu post me sirvio mucho gracias de nuevo hermano

**[Miguel](#16 "2013-03-17 04:16:17"):** hola mi hermano muchas felicidades eres un gran ingeniero hermano gracias

**[admin](#17 "2013-03-17 20:44:53"):** Gracias a ti por visitarme! Veo que has puesto 2 comentarios. Perdón si te ha parecido que no funcionaba el sistema. Lo que ocurre es que para que aparezcan al publico los tengo que validar primero, para evitar el Spam. X)

**[Guille](#33 "2013-04-17 00:24:24"):** hola admin, muchas gracias, funciona de maravilla tu método :D

**[admin](#35 "2013-04-17 09:23:24"):** Gracias a ti por tu visita y tu comentario. Me alegro de que te haya servido, espero verte más por aquí! :)

**[jorge](#439 "2015-12-24 17:34:58"):** muy buen artículo lo he probado con Fedora 23 y me ha funcionado claro remplazando yum por dnf, gracias por compartir



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
