---
title: (ES)(Bash scripting) Trabajar con archivos fifo (pipes)
date: 2014/02/04 09:32:32
layout: post
path: "/working-with-pipes-bash/"
category: "Imported from my older blog"
---
Los archivos fifo (también llamados pipes o tuberías) son un tipo de archivo en el cual se escriben bloques de datos. Al leerlos, los bloques se devuelven en siguiendo un orden fifo ( el primero en entrar es el primero en salir ). Además, una vez leído un bloque, este se elimina. ![fifolifo](/wp-content/uploads/2014/02/fifolifo.jpg) Son muy útiles cuando haya que comunicar procesos, pero que el mensaje que se manda no es necesario almacenarlo. Utilizar fifos en vez de archivos regulares en estos casos aumentará la eficiencia, pues realmente los datos no pasarán siempre por disco. Vamos a ver un script y luego la explicación: 
    
    
    #!/bin/bash
    #http://023.es
    
    # Creamos el Fifo
    FIFO=$(mktemp -u)
    mkfifo $FIFO
    
    # Lo asociamos al descriptor de archivo 3
    exec 3<>$FIFO
    
    # Borramos el fifo (Si no lo vamos a abrir desde otro sitio)
    rm $FIFO
    
    # Si escribimos cualquier cosa en el descriptor de archivo 3, se escribirá en el fifo, no en pantalla
    echo 'Hola holita!' >&3
    sleep 3
    
    # 3 segundos después, leemos a pantalla la primera línea desde el descriptor de archivo 3
    head -n1 <&3
    
    # Al acabar, se cierra el descriptor de archivo 3 (opcional)
    exec 3>&-

Como mktemp no tiene opción de crear fifos temporales, le indicamos que nos devuelva un nombre de archivo temporal válido, pero que no lo cree (opción -u). Luego, utilizamos mkfifo para crear un fifo con el nombre devuelto. (también le podemos dar un nombre fijo) Lo asociamos al descriptor de archivo 3, porque por defecto están ocupados el 0 (entrada estándar), el 1 (salida estándar) y el 2 (salida de error). Una vez asociado, podemos borrar el fifo, pero si lo vamos a leer desde otro proceso tenemos que guardarlo. Ahora, otro ejemplo en el que se comunican dos procesos mediante un fifo: 
    
    
    #maestro.sh
    ###########
    
    #!/bin/bash
    #http://023.es
    
    # Creamos el Fifo
    FIFO=$(mktemp -u)
    mkfifo $FIFO
    
    # Ejecuto el esclavo
    bash esclavo.sh $FIFO &
    
    # Lo asociamos al descriptor de archivo 3
    exec 3<>$FIFO
    
    # Escribimos los 100 primeros pares en el fifo
    for x in $(seq 2 2 100); do
    	echo "$x" >&3
    done
    echo "FIN" >&3
    
    # Al acabar, se cierra el descriptor de archivo 3 (opcional)
    exec 3>&-
    
    #esclavo.sh
    ###########
    
    #!/bin/bash
    #http://023.es
    
    # Asociamos el fifo pasado por parámetro al descriptor de archivo 3
    exec 3<>$1
    
    # leemos del fifo
    while read x; do
    	if [[ $x == "FIN" ]] ; then exit; fi
    	echo "Par recibido: $x"
    done <&3
    
    #Ya lo podemos borrar:
    rm -f $1

Espero que sea útil ;D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
