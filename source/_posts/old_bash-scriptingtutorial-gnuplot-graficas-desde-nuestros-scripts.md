---
title: (ES)(Bash Scripting)Tutorial GnuPlot - Gráficas desde nuestros scripts
date: 2013/10/15 18:51:53
layout: post
path: "/gnuplot-tutorial/"
category: "Imported from my older blog"
---

Ayer dí una clase vía Internet en la que explicaba como hacer uso de Gnuplot, y creo que es interesante compartirlo. Para hacer gráficas fácilmente desde la terminal hacemos uso de **Gnuplot**. Esta es una herramienta que permite crear gráficas para una rápida visualización de datos. Tiene una cantidad de opciones importante, pero por ahora vamos a ver tan solo las mas importantes. Lo primero que vamos a hacer es instalarlo desde los repositorios: 
    
    
    sudo yum install gnuplot #Fedora
    sudo apt-get install gnuplot #Ubuntu

Se puede utilizar desde el prompt propio de gnuplot (ejecutando únicamente gnuplot) o a partir de scripts. Mediante scripts es más fácil, puesto que puedes editarlos y reutilizarlos. La estructura de el script es muy sencilla. Primero se establecen las opciones que tendrá el gráfico y luego se crea indicando la fuente de los datos y el formato que deben tener. También acepta comentarios con el símbolo **#**. Vamos con un ejemplo: 
    
    
    set terminal jpeg                     #Formato de salida
    set title "Grafica1"                  #Titulo
    set xlabel "Dia"                      #Etiqueta eje x
    set ylabel "Valor"                    #Etiqueta eje y
    set yrange [0:30]                     #Rango eje y
    #Dibujo de la gráfica (columnas 1 y 2) tipo de línea 4 y ancho de línea 3:
    plot 'datos.txt' using 1:2 with linespoints linetype 4 linewidth 3 title "Ventas"

Introducimos los datos en el archivo datos.txt: 
    
    
    #dia valor
    1 1
    2 2
    3 4
    4 2
    5 7
    6 9
    7 3
    8 8
    9 17
    10 9
    11 10
    12 2
    13 0
    14 6
    15 6
    16 7

Y procesamos el script con gnuplot: 
    
    
    cat script_gnuplot | gnuplot > grafica.jpg

![Captura de pantalla de 2013-10-15 20:50:19](/wp-content/uploads/2013/10/Captura-de-pantalla-de-2013-10-15-205019.png) También podemos ejecutar los scripts a través del prompt de gnuplot: 
    
    
    gnuplot
    gnuplot> load "script"

Sin embargo esto imprimirá la imagen codificada como caracteres a través de la terminal. Para establecer un archivo como salida cuando utilicemos este método, usaremos la opción** output "grafica.jpg"** Otras opciones muy usadas: 
    
    
    set xdata time               #usa datos de tipo fecha
    set timefmt "%d-%m-%y-%H-%M" #formato de fecha
    set autoscale                #Gnuplot determinará los rangos

Para ver los formatos de salida permitidos, podemos ejecutar el comando: 
    
    
    help set term

Si necesitamos dibujar dos gráficas juntas, para comparar valores, podemos hacerlo así: 
    
    
    #datos1.txt
    1 234
    2 345
    3 435
    4 700
    5 450
    6 100
    #datos2.txt
    1 256
    2 143
    3 342
    4 800
    5 250
    6 210
    
    
    set terminal jpeg
    set title "Grafica2"
    set xlabel "Dia"
    set ylabel "Valor"
    set autoscale
    plot 'datos.txt' using 1:2 with lines linetype 4 linewidth 3 title "Enero", \
    'datos2.txt' using 1:2 with points title "Febrero"

![grafica](/wp-content/uploads/2013/10/grafica.jpg) Para ver los distintos tipos de línea y los estilos de dibujo, podemos usar el comando **test**, cuyo resultado será algo parecido a esto: ![Captura de pantalla de 2013-10-15 20:47:07](http://023.es/wp-content/uploads/2013/10/Captura-de-pantalla-de-2013-10-15-204707.png)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Luis](#430 "2015-10-21 15:51:58"):** Muchas gracias!

**[admin](#431 "2015-11-02 20:42:47"):** A ti por el comentario ;)

**[Lolis](#454 "2016-04-08 02:44:26"):** Mil Gracias! Me has ayudado mucho, apenas comienzo a usar GNUPLOT y me tiene encantada.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
