---
title: (ES)(Bash Scripting) Comprobar si una variable está vacía o instanciada
date: 2013/07/07 11:45:47
---
![](https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRdVFFi8g379ZRKKRkaVsLXimMz7E4te-B-o6KMjdQyDAYOYKW2Rg) Hoy os traigo un método muy sencillo de comprobar si, en un momento dado en la ejecución del script una variable dada está vacía o no. También podremos saber si está instanciada o no; es decir, si existe aunque no tenga ningún dato. 
    
    
    #!/bin/bash
    
    echo "variable no existe"
    echo "${variable-NO_INSTANCE}"
    echo "${variable:-NO_VALUE}"
    
    echo "variable igual a nada (Existe pero no contiene nada)"
    variable=""
    echo "${variable-NO_INSTANCE}"
    echo "${variable:-NO_VALUE}"
    
    echo "variable igual a dato (existe y está llena)"
    variable="dato"
    echo "${variable-NO_INSTANCE}"
    echo "${variable:-NO_VALUE}"

El resultado de ejecutar esto sería: 
    
    
    $ bash comprobar_variable.sh 
    variable no existe
    NO_INSTANCE
    NO_VALUE
    variable igual a nada (Existe pero no contiene nada)
    
    NO_VALUE
    variable igual a dato (existe y está llena)
    dato
    dato

Como véis, al principio, cuando no existe, el programa nos devuelve "NO_INSTANCE" y "NO_VALUE". Cuando si existe pero está vacía, solo devuelve "NO_VALUE" y cuando está llena, devuelve el propio valor de la variable. Esta información puede servir, por ejemplo, para comprobar si la salida de otro subscript ha devuelto algo o no y actuar en consecuencia. Doy las gracias a composer, de [http://camposer-techie.blogspot.com.es](http://camposer-techie.blogspot.com.es/), que es la fuente de estos datos y que me ha salvado la vida en más de una ocasión. Enlace a fuente: <http://camposer-techie.blogspot.com.es/2011/03/barajita-premiada-23-variables-vacias-o.html>

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
