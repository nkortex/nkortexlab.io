---
title: (ES)Obtener cabeceras HTML con wget o curl
date: 2013/01/31 23:20:37
layout: post
path: "/get-html-headers-wget-curl/"
category: "Imported from my older blog"
---
Las cabeceras HTML son fragmentos de información que se mandan entre el servidor y el cliente, de manera totalmente transparente para el usuario y que pueden darnos información interesante de la máquina que hospeda alguna página web. Para conseguir las cabeceras de una página determinada podemos ejecutar wget de esta manera: 
    
    
    wget -S google.com -O - >/dev/null

o curl así: 
    
    
    curl -I google.com

La salida que dan esos 2 comandos será algo parecido a esto: ![Captura de pantalla de 2013-02-01 00:17:34](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-02-01-001734.png)   Pero con diferentes datos, por supuesto. Para el usuario de a pie no es muy útil, pero seguro que vosotros le encontráis alguna utilidad :)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
