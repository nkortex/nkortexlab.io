---
title: (ES)Bajar una web entera (recursivamente) con bash
date: 2013/02/03 20:42:38
---
Si queréis tener una copia local de cualquier sitio web, lo que debéis hacer es bajar una copia a vuestro ordenador para poder acceder offline (entre otros muchos propósitos). Gracias a la terminal, esto es muy sencillo. Para ello vamos a utilizar **wget** de la siguiente manera: 
    
    
    wget http://023.es
    

Esto bajará la página sin mas. Con el siguiente comando podemos descargar de forma recursiva, con las imágenes y demás datos que utilice la web: 
    
    
    wget -r http://023.es
    

Si queremos especificar hasta cuantos niveles de recursividad va a descargar lo hacemos con la opción -l 
    
    
    wget -r -l2 http://023.es
    

(esto descenderá dos niveles) Wget tiene muchas más opciones, si quieres verlas todas ejecuta: 
    
    
    wget --help
    

o 
    
    
    man wget
    

Espero que sirva de ayuda

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
