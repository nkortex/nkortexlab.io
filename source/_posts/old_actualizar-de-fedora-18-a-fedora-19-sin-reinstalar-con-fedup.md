---
title: (ES)Actualizar de Fedora 18 a Fedora 19 sin reinstalar con FedUp
date: 2013/07/02 16:50:13
layout: post
path: "/update-fedora-without-reinstall/"
category: "Imported from my older blog"
---

![](http://fedoraproject.org/static/images/f19_fpo_screenshot.png) Como sabréis, hoy día 2 de julio, ha salido la nueva **Fedora 19** con bastantes mejoras con respecto a la versión anterior (podéis verlas aquí: <https://fedoraproject.org/wiki/Releases/19/FeatureList> ). Hoy os voy a mostrar el método para actualizar sin tener que reinstalar gracias a **FedUp** en 4 pasos: 
    
    
    sudo yum update
    sudo yum install fedup
    sudo fedup-cli --network 19 --debuglog /root/fedupdebug.log
    sudo yum distro-sync

Va a tardar bastante, pero mientras puedes jugar a lo que sea para entretenerte. Si tienes que parar en mitad del proceso, ejecuta esto para que no haya problemas: 
    
    
    sudo fedup-cli --clean

Disfruta de Fedora 19 :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[ricardo yasinski](#127 "2013-07-06 07:36:10"):** hola!!! me gustaria saber si al actualizar el sistema con FedUp me desabilitara las extensiones de gnome shell que ya tengo. Te lo digo porque intente instalar la version 19 en mi pc y me eran incompatibles el 99% de las extensiones de gnome shell!!! Desde ya muchas gracias!! Ricardo.

**[admin](#128 "2013-07-06 09:41:48"):** Hola Ricardo, al actualizar, no te eliminará ninguna de las extensiones de Gnome (Ni ningún otro dato). Pero, al actualizar, algunas que no sean compatibles se deshabilitarán automáticamente. Si necesitas alguna en especial, puedes editarla para que indique que es compatible con la última versión de Gnome y, así, el sistema te permita habilitarla, pero seguramente fincione de forma errónea. Un saludo y gracias por el comentario :D

**[Hugoz76](#255 "2013-10-04 19:03:47"):** amigo intente actualizar mi fedora 18 a 19 pero no me lo permite me envia las siguientes leyandas virtualbox/19/x86_64 | 951 B 00:00 virtualbox/primary | 3.0 kB 00:00 Error: can't get boot images. The installation repo isn't available. You need to specify one with --instrepo. alguna sugerencia?

**[admin](#256 "2013-10-05 09:31:08"):** Hola Hugo, antes de nada gracias por el comentario. Has probado a eliminar el repo de VirtualBox?

**[robert](#313 "2014-01-18 19:54:17"):** Hay alguna manera de actualizar con el live de 19 que tengo a la mano y sin internet, ya que tengo el win en la otra particion saludos.

**[daopz](#342 "2014-04-23 15:07:34"):** buenas amigo al realizar la actualizacion desde fedup se borran archivos como imagenes o documentos de texto? puedo hacer esto para fedora 18 a fedora 20?

**[admin](#344 "2014-04-25 11:42:11"):** Hola daopz. Este método no borra tu carpeta home. Y para actualizar a Fedora 20, es posible que tengas que pasar antes por la 19, pero todo es probar.

**[Alex Requeno](#345 "2014-04-30 19:47:12"):** y afecta a programas instalados?



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
