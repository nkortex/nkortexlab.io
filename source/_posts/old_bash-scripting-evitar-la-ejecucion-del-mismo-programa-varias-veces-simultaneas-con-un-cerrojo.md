---
title: (ES)(Bash Scripting) Evitar la ejecución del mismo programa varias veces simultaneas con un "cerrojo"
date: 2014/08/27 21:28:19
---
Si tenemos scripts que se ejecutan a intervalos regulares y que no duran lo mismo todas las ejecuciones, es bueno asegurarse de que no se ejecuten varias instancias del programa a la vez. Para ello he desarrollado esta solución simple pero resultona: 
    
    
    #lock
    randa=$(($RANDOM%3+1))
    pid_ejecutando=$(ps -eo pid,comm | grep $0 | egrep -o '[0-9]+' )
    if [[ "${pid_ejecutando:-NO_VALUE}" != "NO_VALUE" ]] ; then 
       echo "$0 $@" | at now + $randa min
       exit
    fi

También se podría utilizar wait, pero solo funciona con procesos hijos de la misma shell. Este método funciona con cualquier proceso. Espero que el tip sea de utilidad ;)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
