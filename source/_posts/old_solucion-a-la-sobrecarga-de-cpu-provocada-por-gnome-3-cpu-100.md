---
title: (ES)Solución a la sobrecarga de cpu provocada por Gnome 3 (CPU > 100%)
date: 2013/04/16 07:49:15
layout: post
path: "/gnome3-cpu-overhead-solution/"
category: "Imported from my older blog"
---
Tras una temporada probando Xfce en mi Fedora 18, he decidido volver a Gnome 3 porque, admitámoslo, la versión Xfce no está todavía lista para un uso habitual. El caso es que una vez instalado, Gnome usa una cantidad desproporcionada de cpu, sobre todo si estoy moviendo ventanas o viendo efectos gráficos. Yo suponía que era cosa de la tarjeta gráfica así que me dispuse a arreglarlo, y ahora te explico como lo hice. ![](http://www.otakufreaks.com/wp-content/uploads/2011/12/vegeta-its-over-9000.jpg) Aviso de que esto no es un tutorial. Solo pongo los comandos que hice y que funcionaron. Pero los hice porque tenía el sistema recién instalado y no tenía nada que perder, pero no tengo ni idea de porque funcionó; el caso es que uno de estos comandos lo arregla. USA ESTA INFORMACIÓN BAJO TU RESPONSABILIDAD Abre un terminal e introduce estos comandos uno a uno: 
    
    
    sudo yum update
    su -c 'yum localinstall --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm'
    sudo yum -y install mesa-dri-drivers mesa-libGLU
    yum install libtxc_dxtn --enablerepo=rpmfusion-free-updates-testing
    sudo yum install libtxc_dxtn --enablerepo=rpmfusion-free-updates-testing
    sudo yum -y install mesa-dri-drivers.i686 mesa-libGLU.i686 
    sudo yum install libtxc_dxtn.i686 --enablerepo=rpmfusion-free-updates-testing
    sudo yum -y update
    reboot
    sudo yum install mesa-li*
    xdg-user-dirs-update
    tracker-control -rs
    sudo yum update
    /usr/libexec/plymouth/plymouth-update-initrd
    sudo /usr/libexec/plymouth/plymouth-update-initrd
    sudo yum install nouveau
    sudo yum install nouveau*
    su
    startx
    init 3
    sudo init 3
    sudo yum remove *nvidia*
    sudo rm /etc/X11/xorg.conf
    sudo yum install mesa*
    reboot

Aviso de que algunos de estos comandos dan error. Aviso de que esto solo funcionará si tienes la misma verión de Fedora que yo. Aviso de que esto solo funcionará si el causante del problema es el mismo que a mi. Si aún y así quieres hacerlo y te funciona, coméntalo! :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
