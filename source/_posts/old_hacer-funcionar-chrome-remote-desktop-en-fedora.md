---
title: (ES)Hacer funcionar Chrome-Remote-Desktop en Fedora
date: 2016/04/17 10:29:08
layout: post
path: "/chrome-remote-desktop-fedora/"
category: "Imported from my older blog"
---
Chrome-Remote-Desktop es una herramienta de Google, aún en beta pero usable, que nos permite compartir la pantalla y el control de nuestra máquina con otros dispositivos desde Chrome. Lo malo es que para Linux sólo ofrecen un .deb y ninguna información sobre como instalarlo en sistemas que manejen RPM, pero vamos a ver que no es tan difícil. Primero descargamos la extensión de Chrome: [Chrome-Remote-Desktop ](https://chrome.google.com/webstore/detail/chrome-remote-desktop/gbchcmhmhahfdphkhkmpfmihenigjmpp) Luego, cuando intentamos compartir nuestro escritorio, nos redirige a la descarga del .deb. Guardamos el archivo en lugar seguro. Ahora hay que transformarlo en .rpm con alien: 
    
    
    sudo dnf install dpkg fedora-packager python-psutil xorg-x11-server-Xvfb
    alien -r chrome-remote-desktop_current_amd64.deb
    

Por último, instalamos el rpm generado. Si con dnf install da problemas, se puede forzar la instalación: 
    
    
    sudo rpm -Uvh --force chrome-remote-desktop-50.0.2661.22-2.x86_64.rpm

No es lo más recomendable pero funciona.Ahora tenemos que hacer un nuevo grupo e incluirnos, además de crear carpetas y archivos de configuración necesarios: 
    
    
    sudo groupadd chrome-remote-desktop
    sudo usermod -G chrome-remote-desktop admin    # Cambiar por tu user
    sudo chkconfig chrome-remote-desktop on
    echo 'exec startxfce4' >> ~/.chrome-remote-desktop-session
    mkdir ~/.config/chrome-remote-desktop
    sudo mkdir -p /etc/chromium-browser/native-messaging-hosts
    sudo ln -s /etc/opt/chrome/native-messaging-hosts/* /etc/chromium-browser/native-messaging-hosts/
    

Una vez hecho esto, lo demás es seguir las instrucciones de la aplicación: ![Captura de pantalla de 2016-04-17 12-20-05](/wp-content/uploads/2016/04/Captura-de-pantalla-de-2016-04-17-12-20-05.png)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
