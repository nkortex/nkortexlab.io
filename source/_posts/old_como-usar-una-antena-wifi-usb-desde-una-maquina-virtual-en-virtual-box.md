---
title: (ES)Como usar una antena wifi USB desde una máquina virtual en Virtual Box
date: 2013/06/18 17:46:03
layout: post
path: "/wifi-antenna-from-virtualbox-virtual-machine/"
category: "Imported from my older blog"
---

Ayer mismo me llegó una antena wifi que pedí a eBay. La verdad es que está bastante bien, es una Blueway BT-N9500. Aunque a primera vista el conjunto antena+embalaje+complementos pueda parecer cutre, la verdad es que por 17€ no está nada mal. La recomiendo. Pero bueno, dejo de haceros SPAM. El caso es que una vez probada en Fedora y todo iba bien, quería darle un uso mas "pro" en una distro específica de seguridad, tanto por los scripts tan molones que traen como por que tiene los drivers parcheados de fábrica. La distro que elegí fue Wifislax ( <http://www.wifislax.com/> ) Aunque tengo a backtrack, kali linux y alguna otra a la espera x) El caso es que el VirtualBox no me detectaba mi antena nueva. Bueno, ni la nueva, ni la vieja, ni ninguna. Para que la detecte tenemos que irnos a la configuración de la máquina virtual. Nos vamos a la sección USB, a la derecha le damos al simbolito de un usb con un signo + en verde y que dice "agregar filtro desde el dispositivo". Ahí seleccionamos la tarjeta. Acto seguido vamos a la sección Red. En conectado a seleccionamos "Adaptador puente" y en nombre, el nombre de la tarjeta, que será algo como wlanX. Os dejo unas capturas que no os liéis: ![Captura de pantalla de 2013-06-18 19:43:43](/wp-content/uploads/2013/06/Captura-de-pantalla-de-2013-06-18-194343.png)![Captura de pantalla de 2013-06-18 19:43:36](http://023.es/wp-content/uploads/2013/06/Captura-de-pantalla-de-2013-06-18-194336.png)   Ahora debería de funcionar todo. Recuerda que si la estás usando en VIrtualBox, no funciona en la máquina anfitrión.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[David](#248 "2013-09-23 17:43:17"):** Hola, he leido tu artículo sobre la Blueway BT-N9500 y tengo un problema con ella. Me detecta las redes pero no me conecta con ninguna (estoy probando con la del vecino de abajo que me dió la clave del wifi pero nada, solo lo doy conectado mediante el movil (zona wifi android AP) Si me puedes decir como resuelvo el problema te lo agradezco un montón. Gracias y un saludo.

**[admin](#249 "2013-09-23 18:04:03"):** Hola David, En que sistema operativo la estás usando??

**[David](#250 "2013-09-23 18:11:47"):** Windows 7 Ultimate

**[admin](#251 "2013-09-23 19:04:55"):** En verdad en Windows también le daba fallo a un colega, pero porque detectaba una seguridad errónea. Por ejemplo, si la red era WPA, la veía como una WEP. Comprueba eso antes de seguir

**[enric](#260 "2013-10-17 18:37:58"):** Hola david, tengo un problema y creo que es con la misma antena, cuando pongo a detectar redes desde la maquina virtual con wifislax no me detecta ninguna. Y cuando booteo desde usb al detectar redes pone " bad FCS, spiking.."

**[Paco](#268 "2013-10-25 19:24:45"):** Hola a todos, tengo un problema con mi wifi, me cambié de compañía telefónica a ONO, me instalaron un modem wifi y me compré una antena blueway BT-N9500, lo cierto es que mi conexión a internet se corta, a proximadamente a la hora en la tieda APP donde compré la antena me dicen que es cosa del modem y en la compañía telefónica me dicen que ellos me estan dando el servicio sin problemas, pero lo cierto es que me quedo sin conexión a internet y al reiniciar o apagar el ordenador se queda colgado un buen rato, mi sistema es windows 7, y me gustaría obtener ayuda para solventar mi problema pero a se posible es un leguaje para tontos pues no tengo ni idea de informática. Gracias.

**[admin](#270 "2013-10-26 08:26:36"):** Hola Paco, te respondo por aquí en vez de por correo para generar debate y llegar entre todos a una posible solución. Lo primero sería instalar los drivers. En el miniCD que traía había drivers para windows?

**[Martin](#273 "2013-11-05 08:06:21"):** Que tal, funciona con Kali?

**[admin](#274 "2013-11-05 18:22:49"):** Si, debería funcionar perfectamente

**[Nathu](#293 "2013-12-06 19:49:56"):** Hola he probado tu método y aún la máquina virtual no reconoce mi usb WIFI... es el modelo D-Link DWA-125.

**[admin](#294 "2013-12-07 10:30:11"):** Puede ser por los drivers del sistema que estés virtualizando

**[Oscar](#307 "2013-12-29 14:27:45"):** Que tal genio! Olle, vi esto de virtualbox y ya se me ocurrió preguntarte una cosa que llevo unos días con la cabeza como loca :S Te explico mi propósito con virtualbox: Bueno, tengo 2 proveedores de internet (pongamos Movistar y Ono), lo que quiero hacer, es que mi pc(mi sistema operativo sin virtualbox) este conectado a Movistar mediante Wi-Fi, y que en un sistema montado en virtualbox me coja ono, mediante el cable de red. Podiendo así disfrutar de las 2 conexiones a la vez! Asi que te agradecería que me echaras una mano en esto, estuve viendo tus entradas y creo que vales para esto, estas echo un crack! sigue así! :D Saludos

**[admin](#314 "2014-01-27 23:06:48"):** Pues supongo que habrá alguna manera de hacerlo, pero no sé para que podría servir, la verdad x)

**[Angel](#329 "2014-03-17 04:57:49"):** Hola. Como estas amigo no se si eso me funcione a mi. Veras tengo una antena TP-link WN723N que viene con un disco con el driver , cuando lo compre lo hice desesperadamente sin consultar en ningun lado y mi pc tenia ubuntu pero la antena solo es compactible con windows asi que cambie mi sistema operativo por windows 7. Funcionara igual con ubuntu en virtual box.

**[admin](#331 "2014-03-17 09:17:09"):** Hola Angel Si la antena solo es compatible con Windows, no va a funcionar en otro sistema. Lo que pasa es que dudo mucho que solo sea compatible con Windows. Antes de cambiar a W7 probaste simplemente a conectarla a Ubuntu?

**[Angel](#332 "2014-03-20 05:23:01"):** Hola si ya lo habia probado y no prendia el foco verde que prende ahora con el driver instalado. Estoy feliz de que tengas bastante interes en ayudarnos.

**[oscar](#354 "2014-06-14 17:25:19"):** Se puede conectar la antena blueway a una tableta con androides? En caso afirmativo como? Gracias!

**[ib](#357 "2014-06-20 21:18:49"):** Buenas, Me he comprado la antena blueway bt-N9500 y no se como se utiliza. Tengo un router inalambrico en casa y me he comprado la antena para recibir la señal de Internet en un portátil que esta en un segundo piso, mientras que el router esta en un primer piso. Conecto la antena al portátil mediante USB y cuando intento conectarme a la red wifi de casa o no la encuentra o la intensidad de la señal es muy débil. Tengo windows vista basic. Gracias por la ayuda.

**[francisco](#359 "2014-07-05 02:20:35"):** Buenas ,yo tengo un problema tengo un adaptador wifi USB concretamente TP-LINK WN72VN y en un sistema wifislax virtualizado en virtualbox funciona pero es como si la red fuera cableada no entiendo estoy to rallao jejeje

**[rodrigo medina](#360 "2014-07-08 16:57:45"):** tengo el mismo problema con la antena tl-wn722n no la detecta y no entendi eso de conectarla por ubuntu, me podrias explicar. gracias

**[Alexis Ruiz](#364 "2014-08-09 18:48:39"):** Hola amigo, mira he probado tu guia, pero el virtualbox no me reconoce la tarjeta usb que tengo, esta es compatible con linux, tengo ubuntu y parece que si la reconoce automaticamente ya que me reconoce las redes y me conecto con ella, pero a la hora de probar con kali.. nada, no la reconoce, tengo el cd de instalación pero en windows, me estoy quemando la cabeza buscando soluciones, agradecería mucho que pudieras ayudar, buen día.

**[pedro baños](#405 "2015-01-14 10:02:51"):** HOLA.TENGO UN PROBLEMA CON LA CONEXION WIFI DE ONO Q NO ME LLEGA BIEN LA SEÑAL EN CASA.MI PREGUNTA ES¿ME LLEGARIA MEJOR LA SEÑAL CON LA INSTALACION DE UNA ANTENA WIFI?

**[alvaro](#411 "2015-02-11 08:58:28"):** Hola buenas me gustaria que me contentaras a esta duda que tengo llevo unos dias probando aver si me coje mi maquina virtual de kali linux en virtual box mi tarjeta red usb porque por lo visto la que viene insetarada con el pc virtual box no la reconoce. Mi duda es que si la tarjeta usb que quiero utilizar en kali linux para ponerla en modo monitor tiene que ser compatible con linux y a la vez con mi sistema operativo windons . O solamente con que sea compatible con linux y instalando los drivers en la maquina virtual bastaria? Gracias porfabor need respuesta :S

**[julio jaramillo](#427 "2015-09-29 10:06:47"):** No consigo que se conecte el usb wifi con wifislax

**[johnatan](#429 "2015-10-20 22:08:30"):** No me funciono lo que tienes publicado en el post, tengo una antena usb wireless tp-link 7200, y aun no me detecta mi s.o linux kali linux. por Favor necesito ayuda, gracias... te agradezco ante mano



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
