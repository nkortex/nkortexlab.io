---
title: (ES)Detectar routers con WPS activado sin usar wash
date: 2013/06/13 10:59:44
layout: post
path: "/detect-wps-without-wash/"
category: "Imported from my older blog"
---

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC6zEqhau5FaQDJJfxGe8nk_RRkikCVZOtdwazPsURd-j8zf2j) Para detectar redes wifi cercanas que soporten el protocolo WPS lo más fácil es utilizar wash, que viene incluida con reaver. Sin embargo, a mi no me funciona esta aplicación y, por lo que he leído por Internet, no soy el único. Para conseguir una lista de redes podemos usar la utilidad "iw" de esta manera: 
    
    
    sudo iw wlan0 scan

Esto devolverá una lista de todas las redes a nuestro alrededor de esta manera: (información sensible falseada) 
    
    
    BSS 6a:c5:54:35:25:14 (on wlan0)
    TSF: 216812349874 usec (2d, 12:14:03)
    freq: 2472
    beacon interval: 100
    capability: ESS Privacy ShortSlotTime (0x0411)
    signal: -83.00 dBm
    last seen: 28 ms ago
    SSID: bodaphon6523
    Supported rates: 1.0* 2.0* 5.5* 11.0* 9.0 18.0 36.0 54.0 
    DS Parameter set: channel 13
    ERP: Barker_Preamble_Mode
    Extended supported rates: 6.0 12.0 24.0 48.0 
    HT capabilities:
    Capabilities: 0x118e
    HT20/HT40
    SM Power Save disabled
    TX STBC
    RX STBC 1-stream
    Max AMSDU length: 3839 bytes
    DSSS/CCK HT40
    Maximum RX AMPDU length 65535 bytes (exponent: 0x003)
    Minimum RX AMPDU time spacing: 4 usec (0x05)
    HT RX MCS rate indexes supported: 0-15, 32
    HT TX MCS rate indexes are undefined
    HT operation:
    * primary channel: 13
    * secondary channel offset: below
    * STA channel width: any
    * RIFS: 0
    * HT protection: no
    * non-GF present: 1
    * OBSS non-GF present: 0
    * dual beacon: 0
    * dual CTS protection: 0
    * STBC beacon: 0
    * L-SIG TXOP Prot: 0
    * PCO active: 0
    * PCO phase: 0
    WPA: * Version: 1
    * Group cipher: TKIP
    * Pairwise ciphers: TKIP CCMP
    * Authentication suites: PSK
    WMM: * Parameter version 1
    * BE: CW 15-1023, AIFSN 3
    * BK: CW 15-1023, AIFSN 7
    * VI: CW 7-15, AIFSN 2, TXOP 3008 usec
    * VO: CW 3-7, AIFSN 2, TXOP 1504 usec
    WPS: * Version: 1.0
    * Wi-Fi Protected Setup State: 2 (Configured)
    * Response Type: 3 (AP)
    * UUID: bc329e00-1dd8-11b2-8601-6ac714356510
    * Manufacturer: Huawei Technologies Co., Ltd.
    * Model: Huawei Wireless Access Point
    * Model Number: RT2860
    * Serial Number: 12345678
    * Primary Device Type: 6-0050f204-1
    * Device name: HuaweiAPS
    * Config methods: Label, PBC
    * RF Bands: 0x0
    Extended capabilities: HT Information Exchange Supported
    Country: ES Environment: Indoor/Outdoor
    Channels [1 - 13] @ 16 dBm
    [...]

Toda esa información PARA CADA RED a nuestro alcance. Como podéis ver, es muy poco práctico buscar ahí si la red tiene o no WPS (en el ejemplo, la red si que lo tiene). Para ayudarnos a filtrar vamos a usar a nuestro amigo grep de esta manera: 
    
    
    sudo iw wlan0 scan | egrep 'WPS|BSS|SSID' -w

Este comando devolverá solamente los BSSID (mac) el ESSID(nombre de la red) y WPS si lo tiene activado, así: 
    
    
    BSS 50:67:f0:11:e9:dd (on wlan0)
    SSID: WLAN_11
    BSS 74:88:8b:11:5d:05 (on wlan0)
    SSID: WLAN_5D11
    WPS: * Version: 1.0
    BSS 00:b0:0c:11:be:c3 (on wlan0)
    SSID: ONO11
    BSS 00:19:70:11:5d:b1 (on wlan0)
    SSID: Orange-11e2
    WPS: * Version: 1.0
    BSS 74:31:11:af:9a:34 (on wlan0)
    SSID: Orange-9112
    WPS: * Version: 1.0

Como podéis ver, en este caso vemos de un vistazo que las redes WLAN_5D11, orange-11e2 y orange 9112 tienen WPS activado, y tenemos la dirección del AP para pasárselo a reaver. Esto se podría automatizar más aún mediante un script que se lanzara cada podo tiempo y que atacara a las redes vulnerables que tuviera al alcance, pero eso sería ilegal, así que no lo hagáis Espero que sea de ayuda :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[ANGELO PITT](#277 "2013-11-09 18:13:30"):** VIENTOS HERMANO ESTA MUY PRACTICO BIEN RIFADO

**[tot](#338 "2014-04-10 00:05:14"):** ailoviu tio mas solucionao la papeleta xdd

**[admin](#339 "2014-04-11 10:47:42"):** Jaja, me alegro de que sea útil, tot. Gracias por tu comentario

**[Aitor](#340 "2014-04-21 12:55:04"):** Buenas tardes. Que alguien me corrija si me equivoco, pero por lo que he leido en otros foros acerca de este comando, para saber si tiene o no activado el WPS, no basta con que aparezca en el listado que se indica, que por cierto, o el comando no es correcto o algo no hago bien, ya que metiendo el comando, no me aparece en el listado unicamente el BSSID o el SSID, sino que me aparece una parrafada de la leche, pero bueno, aún asi, siguiendo con el tema del WPS, para saber si está activado o no, debe de aparecer escrito algo asi como "wifi protected setup state: Configured (0z02)" o "wifi protected setup state: Configured 2)". Siendo el 2 el indicativo de que esta activado el WPS. Si estuviese en 1, es que no está activado o está en modo manual por lo que sería imposible sacar el PIN. Saludos

**[Aitor](#341 "2014-04-21 12:57:45"):** Me corrijo, es 0x02 no 0z02. Y añado que el indicativo de estar activado aparece en la columna correspondiente al WPS version 1.0 o la que sea. Un saludo.

**[admin](#343 "2014-04-23 18:02:18"):** Hola Aitor, a mi siempre que lo he probado me ha funcionado este método, sin darme falsos positivos, quizás sea suerte... De todos modos últimamente si que me funciona wash, por lo que hace tiempo que no lo utilizo. Con respecto a la parrafada, acuérdate de añadirle el grep al final para que filtre la información importante

**[Paco](#361 "2014-07-19 08:22:10"):** Gracias Cortex. Eres un artista.

**[pedro j ramirez](#377 "2014-10-19 14:13:06"):** si no te funciona el comando .: wash -i mon0 poner esto: wash -i mon0 --ignore - fcs

**[Jorge](#395 "2015-01-01 12:08:39"):** hola buenas a todos tengo una dudilla y uso wifiway 3.4 y tengo una encriptacion en un router wmm/wme (0x02). con el metodo reaver es posible dar la con la contraseña? Aun asi se asocia al essid pero no avanza...

**[admin](#398 "2015-01-05 12:19:56"):** Hola Jorge. Reaver no es ni mucho menos infalible. Intenta aprovechar una vulnerabilidad de WPS pero hay routers que están preparados para defenderse por lo que puede no funcionar siempre.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
