---
title: (ES)Error " sox FAIL formats no handler for file extension `mp3' " como solucionarlo
date: 2013/02/07 10:38:47
layout: post
path: "/sox-fail-formats-handler-mp3-error/"
category: "Imported from my older blog"
---

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCcIxrZQpHGL9xQi8WkjdKryeFcUQAIh2kZGPRvWR1rVy7bU3J) Esto más que una entrada es un mini recordatorio para acordarme yo mismo dentro de un tiempo como he conseguido arreglar este error de sox. En verdad es muy sencillo, pero en internet no hay apenas información. Para arreglarlo ejecuta: En Fedora: 
    
    
    yum install sox-plugins-freeworld

En Ubuntu: 
    
    
    sudo apt-get install libsox-fmt-mp3

Si en Fedora no te encuentra el paquete es porque no tienes los repositorios RPMFusion, instálalos así: 
    
    
    yum install --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-stable.noarch.rpm
    yum install sox-plugins-freeworld

Espero que sirva de ayuda :)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Domingo](#81 "2013-06-01 22:22:49"):** Muchas gracias. No conseguía que me funcionara Imagination y era justo por esto. Un saludo, Domingo.

**[admin](#82 "2013-06-02 12:17:52"):** Hola Domingo! Me alegro de que te haya sido útil esta información. Espero verte pronto por aquí :)

**[tatica](#334 "2014-03-26 14:05:37"):** Igual que Domingo, Imagination dando ataques de crisis y tu post solucionandolo. Muchas gracias por el tuto!

**[jmanuelcool](#335 "2014-03-26 14:07:46"):** Compa, un montón, yo no lo necesité; pero justo le estaba hablando a una amiga sobre imagination y le saltó el problema del mp3, gracias a tu ayuda lo pudo solucionar



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
