---
title: (ES)Como instalar/reinstalar/actualizar GRUB2 desde un liveCD
date: 2013/01/20 10:41:03
layout: post
path: "/install-update-grub-from-livecd/"
category: "Imported from my older blog"
---

En este tutorial, dedicado expresamente al Agustinaso, os voy a mostrar como recuperar el grub después de haberlo borrado(por una instalación posterior de Windows) o a instalarlo si por cualquier otro motivo, no os funciona. ![](http://img.ubunlog.com/wp-content/uploads/2012/09/como-recuperar-el-grub-de-linux-en-ubuntu-12-04.jpg) Lo vamos a hacer desde un liveCD de Linux. La distribución da igual, pero tenéis que tener uno a mano (un LiveUSB también debería valer, pero os recomiendo mejor un CD o un DVD) Primero tenemos que saber la partición donde está instalado nuestro Linux. En un terminal ejecuta el comando: 
    
    
     sudo fdisk -l

Este comando te devuelve una tabla con las particiones de tu PC. Algo parecido a esto: ![Captura de pantalla de 2013-01-20 11:25:54](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-01-20-112554.png) En tu caso, seguramente saldrá en la columna "sistema" una fila Linux, una fila Windows y algunas filas extra. Apunta el dispositivo de la primera columna que coincide con la partición de Linux (/dev/sda1 , /dev/sda4 , /dev/hda2 ,...) El que sea en tu caso. Apúntalo en un papel. Ahora montamos la partición con el comando (sustituyendo /dev/sda2 por lo que has apuntado antes): 
    
    
    sudo mount /dev/sda2 /mnt

Montamos también el resto de los dispositivos: 
    
    
    sudo mount --bind /dev /mnt/dev

Una vez montada la partición (el comando anterior no ha dado ningún error) procedemos a reinstalar el Grub. Para ello ejecutamos estos comandos uno a uno: 
    
    
    sudo chroot /mnt

**OJO! ATENCIÓN! IMPORTANTÍSIMO! en el siguiente comando, es /dev/sda o /dev/sdb dependiendo de tu disco, pero sin número, no pongas sda1 o sdb3. solo sda**
    
    
    grub-install --recheck /dev/sda
    
    
     sudo update-grub

Hecho esto, ya puedes reiniciar el ordenador y tendrás un bonito Grub con todos los sistemas. :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Humberto](#457 "2016-04-29 09:29:07"):** Gracias me sirvio mucho la parte en rojo. "OJO! ATENCIÓN! IMPORTANTÍSIMO! en el siguiente comando, es /dev/sda o /dev/sdb dependiendo de tu disco, pero sin número, no pongas sda1 o sdb3. solo sda"



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
