---
title: (ES)(Bash Scripting)Obtener hash sha1, md5,... en Linux
date: 2014/03/03 13:04:08
---
![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUr3xqIy_MxwLQArHmLirY1ljkSNE8aoN_qf-M8Xz4fOPY7aKf) Si en alguna ocasión necesitáis obtener el hash o suma de verificación de un archivo o cadena de caracteres podéis hacerlo de este modo: 
    
    
    echo -n "Cadena de caracteres" | sha1sum         # o sha256sum sha512sum md5sum...
    sha1sum archivo                                  # o sha256sum sha512sum md5sum...

Encauzaremos la salida con un programa u otro dependiendo del hash que queramos obtener. En el primer caso, es necesaria la opción -n para que echo no incluya el salto de línea al final, puesto que esto provocaría una salida errónea.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
