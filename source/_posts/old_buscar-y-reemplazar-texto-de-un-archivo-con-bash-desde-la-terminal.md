---
title: (ES)Buscar y reemplazar texto de un archivo con bash desde la terminal
date: 2013/02/04 16:41:19
layout: post
path: "/find-reemplace-bash/"
category: "Imported from my older blog"
---

Este tip es muy útil si estás desarrollando scripts que leen desde ficheros y quieres manipularlos. Por ejemplo si lees números decimales que usan coma en vez de punto. con un único comando puedes buscar y reemplazar el texto que quieras: 
    
    
    sed s/ abc / xyz /g archivo1  > archivo2

En este ejemplo, se sustituirá las palabras "abc" que encuentre en archivo1 por "xyz" y escribirá el resultado en archivo2. Para sustituir puntos por comas: 
    
    
    sed s/,/./g precioscoma > preciospunto

Espero que os sea útil. Edit: Si queréis trabajar con variables (sustituir el valor de una variable por el de otra) podéis hacerlo así: 
    
    
    sed "s|$var1|$var2|" archivo1  > archivo2

<!-- PELICAN_END_SUMMARY -->
## Comments

**[oswaldo](#278 "2013-11-13 07:19:25"):** HOla, estoy utilizando sed "s|$var1|$var2|" archivo1 > archivo2 pero quiero que sobreescriba el archivo original, intento con sed "s|$var1|$var2|" archivo1 > archivo1 pero archivo1 se queda en blanco, sabes como podría sobreescribir el archivo? gracias.

**[admin](#279 "2013-11-13 10:33:41"):** Hola Oswaldo, Modificar un archivo de esa manera suele dar muchos errores. Te recomiendo que utilices un archivo auxiliar. Algo así: sed “s|$var1|$var2|” archivo1 > archivo2 mv -f archivo2 archivo1 Lo que hará que tras escribir el resultado en archivo2, este sobreescriba a archivo1 Gracias por el comentario y por la visita!

**[oswaldo](#282 "2013-11-14 01:52:24"):** Lo intentaré de esa forma, gracias por la información.

**[Diomesles](#425 "2015-07-27 08:02:00"):** Buena entrada!: Por si queremos hacer el "buscar-reemplazar" en mas de un fichero: http://www.sysadmit.com/2015/07/linux-reemplazar-texto-en-archivos-con-sed.html



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
