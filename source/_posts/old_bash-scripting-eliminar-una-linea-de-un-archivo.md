---
title: (ES)(Bash Scripting) Eliminar una línea de un archivo
date: 2013/06/16 12:15:07
---

Esta es una función que echo muchísimo de menos cuando estoy trabajando con script. Estoy 100% seguro de que alguien ha hecho esta función ya, pero no la encuentro y tampoco es difícil de programar: 
    
    
    #!/bin/bash
    
    #Made in http://023.es
    
    #cuenta las lineas del archivo
    numlineas=$(cat $1 | wc -l);
    
    #lineas a guardar
    porarriba=$(($2-1))
    porabajo=$(($numlineas-$2))
    
    #guarda todas las lineas
    cat $1 | head -n $porarriba >> temp
    cat $1 | tail -n $porabajo >> temp
    
    #sobreescribe
    rm $1
    mv temp $1

Es uso no puede ser más fácil: 
    
    
    ./eliminador archivo linea a borrar

<!-- PELICAN_END_SUMMARY -->
## Comments

**[ariel](#426 "2015-09-17 23:10:03"):** Es posible hacerlo mas fácil con sed Para eliminar la línea 20 de un archivo $ sed -i '20d' archivo o para borrar cualquier línea linea=10 sed -i "${linea}d" archivo



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
