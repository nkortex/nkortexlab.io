---
title: (ES)(Bash Scripting)Como subir archivos mediante FTP desde terminal con wput
date: 2013/09/20 12:30:48
---
Bastante a menudo me surge la necesidad de subir los resultados de algún **script** a un servidor mediante **FTP**. Para resolverlo utilizo el programa **wput**. Me sorprendí al ver que aún no había escrito acerca de este programa en mi blog, así que allá voy. ![](http://4.bp.blogspot.com/-vJWRTloxyW4/T3NuUya4zXI/AAAAAAAAAJo/Xz17n55_tKw/s320/ftp-logo.jpg) **wput** permite subir archivos a servidores mediante **FTP**. Para usarlo, primero lo instalamos desde los repositorios de tu distribución con un **apt-get** o un **yum install**. Una vez lo tengamos, su uso básico es: 
    
    
    wput archivo_a_subir ftp://USUARIO:PASS@123.123.123.123:1234 /directorio/del/servidor/remoto/

Recomiendo ver el manual, pues tiene opciones útiles para según que casos ;)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
