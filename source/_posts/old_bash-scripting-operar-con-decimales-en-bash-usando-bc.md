---
title: (ES)(Bash Scripting) Operar con decimales en bash usando bc
date: 2014/05/05 11:05:52
---
Hoy os traigo un pequeño truco para trabajar con decimales en bash gracias a nuestro amigo bc. Si hacemos una operación no entera mediante las operaciones incluidas en bash así: 
    
    
    $ $((25.768/24))
    bash: ((: 25.768/24: error sintáctico: operador aritmético inválido (el elemento de error es ".768/24")

nos dará un error. Y si lo hacemos con bc: 
    
    
    $ echo '25.768/24' | bc
    1

Nos devolverá solamente la parte entera del resultado; en este caso 1. Para evitar este comportamiento debemos cambiar la precisión decimal de bc, que por defecto es 0, a la que deseemos mediante la variable 'scale': 
    
    
    $ echo "scale = 10; 25.768/24" | bc          # 10 decimales
    1.0736666666
    $ echo "scale = 20; 25.768/24" | bc          # 20 decimales
    1.07366666666666666666
    $ echo "scale = 200; 25.768/24" | bc          # 200 decimales
    1.073666666666666666666666666666666666666666666666666666666666666666\
    66666666666666666666666666666666666666666666666666666666666666666666\
    666666666666666666666666666666666666666666666666666666666666666666

Como véis, se le puede pasar un número arbitrario de decimales.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
