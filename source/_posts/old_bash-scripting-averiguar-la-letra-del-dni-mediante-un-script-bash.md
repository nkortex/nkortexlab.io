---
title: (ES)(Bash Scripting) Averiguar la letra del DNI mediante un script bash
date: 2013/02/14 09:36:04
---

Otra entrega de los mini-tutoriales de programación bash! Este es un ejemplo bastante más simple que el anterior, pero como no van ordenados según dificultad, sino según se me van ocurriendo pues me lo perdonáis. Este tuto nos va a enseñar a sacar la letra del NIF a partir de un DNI pasado por parámetro. No es dificil, pero antes hay que saber la teoría sobre como se averigua esta letra. La letra del DNI se obtiene haciendo el módulo 23 del DNI (Esto es, dividir entre 23 y quedarse con el resto) y luego se ve la asignación del número resultante con la letra en la siguiente tabla (Extraída de Wikibooks): ![Captura de pantalla de 2013-02-14 10:38:42](/wp-content/uploads/2013/02/Captura-de-pantalla-de-2013-02-14-103842.png) Ahora vamos con el código, muy facilito de entender: 
    
    
    #!/bin/bash
    
    #Guardamos el módulo 23 del DNI (pasado por parámetro) en la variable $modulo
    modulo=$1%23
    
    #Asignamos cada número a su letra correspondiente siguiendo la tabla en un array $letra
    letra[0]=T
    letra[1]=R
    letra[2]=W
    letra[3]=A
    letra[4]=G
    letra[5]=M
    letra[6]=Y
    letra[7]=F
    letra[8]=P
    letra[9]=D
    letra[10]X
    letra[11]=B
    letra[12]=N
    letra[13]=J
    letra[14]=Z
    letra[15]=S
    letra[16]=Q
    letra[17]=V
    letra[18]=H
    letra[19]=L
    letra[20]=C
    letra[21]=K
    letra[22]=E
    #En la tabla hay 23 campos (del 0 al 22) porque el modulo 23 de cualquier número siempre nos da un numero mayor de -1 y menor que 23 
    
    #Mostramos la letra resultante por pantalla
    echo ${letra[$modulo]}

Para probarlo, abrimos un terminal en la carpeta donde lo hayamos guardado y ejecutamos: `bash ScriptDNI.sh 57849625` Lo que dará de resultado: W Si tenéis algún problema, comentadlo :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[sushisan](#27 "2013-03-30 23:16:22"):** La lista se puede cargar de una sola vez: letra=(T R W A G M Y F P D X B N J Z S Q V H L C K E) Saludos

**[admin](#28 "2013-04-08 19:26:14"):** Muy cierto Sushisan! De este modo ahorramos muchas líneas de código y mucha redundancia. Gracias por tu aporte!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
