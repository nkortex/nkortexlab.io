---
title: (ES)Mejorar y comprimir imágenes de documentos digitalizados y pasarlas a pdf
date: 2013/03/07 17:01:30
layout: post
path: "/digitalice-images-and-convert-to-pdf/"
category: "Imported from my older blog"
---

Hoy os voy a enseñar algo que se sale un poco del tema que últimamente estoy tratando en el blog. En este tutorial vamos a ver como mejorar las imágenes escaneadas o fotografiadas de un documento para aumentar la legibilidad, luego comprimiremos dichas imágenes para disminuir el tamaño de los archivos y, por último, las pasaremos a PDF, para tenerlas organizadas. No vamos a hacer milagros, pero si el material del que partimos es bastante mediocre, el resultado puede sorprenderte. Para todo el proceso de mejora/compresión vamos a usar el comando **mogrify** incluido en el paquete ImageMagick. El uso del comando sería: 
    
    
    mogrify -verbose -rotate -90 -geometry 1200 -normalize -unsharp 0 -quality 83 imagen*.jpg

Donde: 

  * -verbose: Dice al programa que comente con todo detalle lo que vaya haciendo
  * -rotate -90: Gira la imagen 90 grados a la izquierda. Esta parte hay que cambiarla según estén nuestras imágenes tomadas.
  * -geometry 1200: Ajusta el ancho de la imagen a 1200px. La altura cambiará proporcionalmente.
  * -normalize: Cambia el contraste de la imagen haciendo que las partes claras se conviertan en totalmente blancas y las oscuras en totalmente negras. Esta es la parte que hace la magia.
  * -unsharp 0: Mejora levemente el resultado en imágenes desenfocadas. Aumenta considerablemente la carga de CPU.
  * -quality 83: Ajusta la calidad de la imagen resultante. A más calidad, mas tamaño. Varía este valor según tus necesidades de espacio y calidad.
  * imagen*.jpg: archivo de entrada, que será también el de salida. Es decir, los archivos serán sobreescritos.
Una vez que tenemos todas las imágenes mejoradas (Recuerda tener una copia de seguridad por si acaso) vamos a unirlas con el comando **convert**, que también forma parte de imagemagick: 
    
    
    convert *.jpg unidas.pdf

cambiando *.jpg por las imágenes que quieras juntar y unidas.pdf por el archivo resultante que quieras crear. Recomiendo tener las imágenes ordenadas (en plan img_01, img_02,...) para que luego se unan bien. Alguna duda, coméntala! :D Este tutorial está basado en el trabajo de[ ciberdroide](http://ciberdroide.com/fotos/2011/02/simple-postprocesado-para-digitalizacion-de-documentos/). Dicho trabajo tiene una licencia CC-CompartirIgual (Igual que este blog! xD)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Addikevykem](#67 "2013-05-14 08:41:43"):** pretty nice post, i undoubtedly love this website, maintain on it



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
