---
title: (ES)Descargar con wget o curl anónimamente desde terminal a través de TOR
date: 2013/07/07 18:28:40
layout: post
path: "/wget-curl-througt-tor/"
category: "Imported from my older blog"
---
Tanto si sois más paranoicos de la cuenta como si necesitáis descargar de forma anónima o cambiar de IP repetidas veces, este tip os va a interesar. ![](http://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Tor-logo-2011-flat.svg/140px-Tor-logo-2011-flat.svg.png) Vamos a aprender a descargar desde terminal a través de la red TOR. Esta red está formada por multitud de nodos que encriptan las comunicaciones de manera que te vuelves prácticamente anónimo (ojo, **no del todo**. Hay formas variadas de identificarte). En [Wikipedia](http://es.wikipedia.org/wiki/Tor) explican bastante bien como funciona la red TOR, echádle un vistazo. Para hacerlo vamos a descargarnos la aplicación **torsocks** desde yum o apt. Si no está en los repositorios se puede instalar fácilmente así: 
    
    
    cd ~
    git clone git://git.torproject.org/git/torsocks
    cd torsocks
    ./autogen.sh
    ./configure
    make
    sudo make install

También tenemos que descargar el **Tor** en si. Este seguro que está en los repos: 
    
    
    sudo yum install tor

Ahora, solo tenemos que lanzar wget, curl, o cualquier otro programa con el que queramos pasar inadvertidos con de este modo: 
    
    
    torsocks wget 023.es/ip.php

Como podéis ver, la IP que devuelve es distinta que si ejecutáis wget directamente así: 
    
    
    wget 023.es/ip.php

Ahora, si queréis cambiar vuestra IP pública, basta con reiniciar el servicio de tor para que seleccione unos nodos distintos a través de la red : 
    
    
    sudo service tor restart

Y recordad, la red Tor se basa en voluntarios que colaboran con nodos para otorgar anonimato a gente que lo necesite. No lo utilices para descargar torrents ni para cosas que necesiten de mucho ancho de banda, pues estarás perjudicando al resto ;)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
