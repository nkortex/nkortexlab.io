---
title: (ES)Como averiguar contraseña de una red Wifi WEP
date: 2013/01/25 16:33:11
layout: post
path: "/wep-crack-tutorial/"
category: "Imported from my older blog"
---
(Tutorial realizado para la auditoría de tu propia red wifi. Úsalo solamente para comprobar si tu red es segura. Entrar en redes wifi ajenas es delito) Antes de empezar con el tutorial voy a partir de la base de que tenemos los ingredientes necesarios: 

  * Un PC con alguna distribución linux. No hace falta ninguna en particular, pero si tienes alguna pensada específicamente para la auditoría Wifi, pues mejor.
  * Una tarjeta de red compatible con aircrack (con posiblilidad de ponerse en modo promíscuo y, si es posible, de inyectar tráfico). Puedes consultar la compatibilidad de tu tarjeta [aquí(Ingles)](http://www.aircrack-ng.org/doku.php?id=compatibility_drivers) o [aquí(Español)](http://www.aircrack-ng.org/doku.php?id=es:compatible_cards)
Ahora instalamos la suite de seguridad "aircrack-ng". Si usas Fedora o algún derivado, se instala rápidamente desde los repositorios: 
    
    
    sudo yum install aircrack-ng

Si estás en Ubuntu o derivados, puedes probar a buscar en los repositorios. No estoy seguro de si está pero prueba:(si funciona, déjame un comentario para confirmarlo) 
    
    
    sudo apt-get install aircrack-ng

Si resulta que no está en los repositorios, lo instalamos desde un .tar que nos bajamos desde <http://www.aircrack-ng.org/> . Una vez se baje(no tarda nada) vamos a la carpeta donde esté con una terminal y ejecutamos: 
    
    
    tar xfz aircrack-ng-0.7.tar.gz
    cd aircrack-ng-0.7
    make
    make install

Ok, si no ha dado ningún error es que se ha instalado bien. Ahora en el teminal hacemos un su para entrar como admin. A partir de ahora todas las órdenes serán con privilegios de administrador: 
    
    
    su

Antes de nada, tenemos que saber cual es la interfaz de la tarjeta de red. Para ello ejecutamos: 
    
    
     ifconfig

lo que nos dará una salida parecida a esta: ![Captura de pantalla de 2013-01-25 16:42:58](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-01-25-164258.png)   En mi caso, la interfaz de la tarjeta inalámbrica es "wlan0". En tu caso es posible que sea otra; apúntala para usarla más adelante. Como es posible que tu router tenga filtrado mac, vamos a maquillar la mac de nuestro ordenador para que sea más dificil entrar. Alguna persona sin escrúpulos y muy muy malvada podría usar esta herramienta para entrar en el router del prójimo , teniendo este filtrado mac, cambiando su mac por la del vecino; pero ya os digo que no lo uséis para ese propósito, pues es ilegal. Para cambial la mac de nuestra tarjeta de Red ejecutamos: 
    
    
     macchanger --mac 00:11:22:33:44:55 wlan0

Asegúrate de cambiar wlan0 por tu interfaz (en este y en todos los comandos). En este caso puedes poner la mac que quieras, que será la mac que el router al que te conectes verá. Ahora, ponemos la tarjeta en modo monitor, también conocido como modo promiscuo o modo putilla. Así la tarjeta aceptará todos los paquetes entrantes. Para ello ejecutamos: 
    
    
    airmon-ng start wlan0

Luego ejecutamos 
    
    
    airodump-ng wlan0

Este comando nos sacará por pantalla todas las redes que hay a nuestro alcance junto con otros datos muy valiosos. Tras unos segundos en ejecución nuestra pantalla será algo parecido a esto: ![Captura de pantalla de 2013-01-25 16:55:00](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-01-25-165500.png)   (las direcciones mac se han borrado) De esta tabla podemos sacar muchas cosas en claro sobre que red es mas vulnerable. Encima de la tabla sale el canal actual y el tiempo que lleva monitorizando.  Empecemos por la parte de arriba: 

  * El BSSID indica la dirección mac del punto de acceso
  * El PWR indica la calidad de la señal, donde, por ejemplo, -50 indica mejor señal que -80.
  * Beacons indica el numero de beacons frames recibidos. Estos beacons contienen la información básica de la red. Si tu tarjeta no te indica la fuerza de la señal, puedes estimarla con este numero.
  * Data: frames de datos recibidos. Cuanto más rápidamente suba este número, más uso tiene la red en estos momentos y más fácil es sacar el password.
  * #/s :data frames por segundo.
  * CH: canal del AP. va del 1 al 13 en Europa
  * ENC: encriptación -> encriptación WEP = contraseña fácil. Encriptación WPA = contraseña dificil. Sin encriptación = no contraseña
  * ESSID: nombre de la red, si no está oculto.
En la parte de abajo tenemos los clientes conectados a los distintos AP: 
  * BSSID: mac del punto de acceso al que está conectado
  * Station: mac del cliente conectado
  * framess: numero de frames de datos recibidos
  * probes: Essid de las redes a las que intentó conectarse
En el ejemplo de arriba, por ejemplo, podemos ver que una red vulnerable es la IIItusmuertosrobawifiIII, pues tiene la seguridad WEP. Ahora, para que el airodump nos coja solo los datos que nos interesan, lo filtramos así: 
    
    
    airodump -c 11 --bssid BSSID -w archivo wlan

De este modo solo capturará paquetes en el canal 11 del AP BSSID (cambiar según tu caso). Todos los datos los guardará en "archivo". Una vez tengamos un numero considerable de datos de la red, como 100000 o así, podemos intentar averiguar la contraseña con el comando: 
    
    
    aircrack-ng -b BSSID archivo.cap

Si tienes varios archivos .cap, puedes ponerlos todos separados de espacios. Al hacer esto, nuestro PC empezará a sacar la contraseña y te la mostrará. Si la red tiene poco tráfico, puede llevarte mucho tiempo conseguir ese número de paquetes. En otro post pondré como hacer que el número de paquetes suba más rápido con inyección de paquetes. Tener 100000 paquetes no te asegura que se vaya a sacar la pass. A veces es suficiente con menos, otras veces se necesitan más, eso es cuestión de suerte. Además, si durante el proceso se ha cambiado la contraseña no se sacará nunca. Pero eso no es problema porque en tu propia red sabes cuando se cambia la pass. Como véis, para tener una red segura, lo más improtante es tener una seguridad WPA. El filtrado de mac también ayuda, pero no es nada seguro, pues un atacante puede "maquillar" su mac para entrar.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
