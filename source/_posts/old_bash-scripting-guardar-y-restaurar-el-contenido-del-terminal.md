---
title: (ES)(Bash Scripting) Guardar y restaurar el contenido del terminal
date: 2013/09/13 11:35:01
---
De vez en cuando, puede ser interesante guardar el contenido que tenía el terminal antes de hacer alguna acción para luego restaurarlo, como hace el comando **man** o** vim**. Para esto nos vamos a ayudar del programa **tput** y sus opciones** smcup** y **rmcup** ![](http://www.mecambioamac.com/wp-content/uploads/2010/07/512-Terminal.png) **smcup** limpia la pantalla despues de guardar su contenido, y **rmcup** lo deja todo como estaba. Vamos a ver un script de ejemplo: 
    
    
    #!/bin/bash
    
    echo "Este es el primer mensaje"
    echo "Y este el segundo"
    sleep 2
    
    #Guarda el contenido del terminal y limpia.
    
    tput smcup
    
    echo "Mensaje numero 3"
    echo "El 4"
    echo "Y el 5"
    read -p "Pulse enter para continuar"
    
    #Restaura el contenido guardado
    
    tput rmcup
    
    echo "Mensaje final"

Tras imprimir los 2 primeros mensajes, limpia la pantalla e imprime el 3, 4 y 5, espera a que se pulse enter, restaura el contenido guardado e imprime el mensaje final. Espero que sea útil ;) Visto en: <http://wiki.bash-hackers.org/snipplets/screen_saverestore>

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
