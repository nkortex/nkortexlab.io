---
title: (ES)(Bash Scripting) Mejorando nuestro despertador
date: 2013/02/17 11:58:43
layout: post
path: "/despertador2/"
category: "Imported from my older blog"
---
En un post anterior, desarrollamos un despertador personalizado en un script bash. Hoy vamos a mejorarlo en algunos aspectos. Si no visteis el anterior post, podéis encontrarlo aquí: <http://023.es/bash-scripting-crear-un-tono-de-despertador-personalizado-para-cada-dia/> Las mejoras que propongo son: 

  * Reproducción de un capítulo aleatorio del podcast de El mundo today (o cualquier otro podcast)
  * Subir el volumen global del sistema, por si dejamos el volumen muy bajo o silenciado, para no quedarnos dormidos
Para bajarnos el capítulo del podcast, necesitamos saber la dirección rss. Luego buscamos los links a archivos mp3 y los guardamos en un archivo "linksmp3". Luego elegimos aleatoriamente un archivo de la lista y lo descargamos. 
    
    
    curl --silent "http://www.cadenaser.com/rssaudio/elmundotoday.xml" > podcast.dat
    #Hago una lista de todos los mp3 disponibles en el podcast
    grep '<guid>' podcast.dat | cut -d ">" -f2 | cut -d "<" -f 1 > linksmp3
    #Me bajo el mp3 con las noticias aleatoriamente
    numlineas=$(cat linksmp3 | wc -l);
    linea=$(($RANDOM%$numlineas +1))
    mp3=$(cat linksmp3 | head -n $linea | tail -1)
    wget $mp3 -O podcast.mp3

  Como posiblemente este podcast tenga un bitrate distinto a los archivos que nos descargamos anteriormente de Gogle Translator, sox nos va a dar errores y no nos va a dejar unirlos. Una posible solución a este problema sería cambiar el bitrate con** ffmpeg , **pero puede tardar un rato importante y nos acabaríamos despertando demasiado tarde. Así que lo que vamos a hacer es reproducir los 2 archivos por separado. Para asegurarnos de que tenemos un volumen adecuado para despertarnos, usaremos el código: 
    
    
    amixer -c 0 set Master 70%

Lo que dejará el volumen al 70%. Sobra decir que hay que añadirlo ANTES de reproducir los archivos Alguna otra mejora que se os ocurra? Comentadlo! :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
