---
title: (ES)arkOS, una nueva solución de auto-hosting en la Raspberry
date: 2013/11/12 18:00:59
layout: post
path: "/arkos-raspberry-distro/"
category: "Imported from my older blog"
---

Acabo de descubrir una nueva solución que se va a desarrollar (Si su croudfounding llega a buen puerto) que consiste en otra alternativa para autohostear archivos, webs o mails desde la raspberry (Aunque funcionará en gran cantidad de dispositivos). Su nombre es arkOS y aquí os dejo un vídeo explicativo:  Una de las mejores cosas que le veo es que está basado en software libre al 100%, por lo que la mejora de sus capacidades será rápida; apenas consume recursos; tendrá una interfaz amigable (Según ellos, no hay que tocar terminal) y estás alejado de la NSA, puesto que crifra todo. Os recomiendo que colaboréis aunque solo sea un poco con este proyecto, donando a la campaña de [crowdfounding](https://fund.arkos.io/), yo ya lo hice ;) En esa misma página tenéis toda la información del proyecto.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Joshelu](#280 "2013-11-13 17:02:34"):** Gracias por el aviso, la verdad es que tiene buena pinta. Hoy me ha llegado mi raspberry así que la probaré a ver qué tal... Un saludo!

**[admin](#281 "2013-11-13 17:37:08"):** Gracias a ti por la visita! Ya me contarás si te va bien

**[WSN](#417 "2015-06-11 17:26:48"):** A fecha de hoy no tenia ni idea de este S.O. estupenda noticia!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
