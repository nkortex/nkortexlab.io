---
title: (ES)Copiar archivos en terminal de forma detallada
date: 2012/12/27 17:37:38
layout: post
path: "/gcp-better-than-cp/"
category: "Imported from my older blog"
---
Sigo publicando los tutoriales que en su día escribí para UsemosLinux. Hoy toca el turno al comando gcp, para copiar archivos de forma detallada en el terminal. El artículo original lo puedes consultar aquí: Una de las razones que da alguna gente para explicar su rechazo al **terminal**de **Linux** es que ofrece poca **información** y que es difícil de **interpretar**. Y en algunos casos tienen razón. Para que sepais el cambio que vamos a hacer, os lo explico con un ejemplo en imágenes. Para copiar un archivo (libro1.pdf) a otra carpeta, hasta ahora lo hacíamos asi: 
    
    
    cp libro2.pdf otracarpeta

Y esta es la información que nos ofrecía el terminal: ![](http://2.bp.blogspot.com/-n-1ysXsmv7A/UFr9uSIEmiI/AAAAAAAABQA/tZHq7yM5jA8/s640/Captura+de+pantalla+de+2012-09-20+13:27:37.png) Como veis, esto nos deja bastantes dudas: cuanto ha tardado, cuanto le queda, cuanto pesa el archivo, etc. Nosotros lo vamos a solucionar instalando el programa gcp. Este programa hace básicamente lo mismo pero muestra bastante más información: ![](http://1.bp.blogspot.com/-rtVdwP_6y-0/UFr_72qsSLI/AAAAAAAABQI/P99TH5QfO6E/s640/Captura+de+pantalla+de+2012-09-20+13:37:08.png) Para poder usar este programa usad el siguiente comando: **Ubuntu**: 
    
    
    sudo apt-get install gcp -y && echo "alias cp='gcp'" >> $HOME/.bashrc

**Arch**: 
    
    
    yaourt -S gcp && echo "alias cp='gcp'" >> $HOME/.bashrc

Este comando lo que hará es descargar e instalar el programa y crear un alias para que cada vez que escribáis el comando "cp" lo sustituya por "gcp". A partir de ahora, vuestras copias serán mucho mas profesionales.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
