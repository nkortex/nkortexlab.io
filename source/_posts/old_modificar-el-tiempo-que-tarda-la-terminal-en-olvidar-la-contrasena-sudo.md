---
title: (ES)Modificar el tiempo que tarda la terminal en olvidar la contraseña sudo
date: 2013/01/08 11:35:31
layout: post
path: "/longer-sudo-password-remmembering/"
category: "Imported from my older blog"
---
Volviendo a la personalización del terminal, hoy vais a aprender a cambiar el tiempo en el que la terminal "recuerda" la contraseña de sudo. Esto es útil si sueles trastear mucho y te cansa poner la contraseña cada cierto tiempo. Los pasos a seguir son pocos y fáciles: 

  1. **s****udo visudo:**** **se abre el editor de texto vi en la terminal.
  2. Vamos al final y añadimos **Defaults timestamp_timeout = [minutos]**** **cambiando minutos por el numero de minutos que queremos que espere: 0= la olvida automáticamente.
  3. Guardamos y cerramos con un **:wq** y listo!

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
