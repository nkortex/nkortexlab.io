---
title: (ES)(Bash scripting) Deshabilitar dispositivos hardware
date: 2014/02/05 18:36:38
---
Os dejo un tip rápido visto aquí acerca  de como deshabilitar dispositivos hardware sin necesidad de ser root ( 0_o! ). Primero listamos los elementos disponibles: 
    
    
    $ xinput list
    ⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
    ⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
    ⎜   ↳ ETPS/2 Elantech Touchpad                	id=13	[slave  pointer  (2)]
    ⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
        ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
        ↳ Power Button                            	id=6	[slave  keyboard (3)]
        ↳ Video Bus                               	id=7	[slave  keyboard (3)]
        ↳ Video Bus                               	id=8	[slave  keyboard (3)]
        ↳ Sleep Button                            	id=9	[slave  keyboard (3)]
        ↳ ASUS USB2.0 WebCam                      	id=10	[slave  keyboard (3)]
        ↳ Asus WMI hotkeys                        	id=11	[slave  keyboard (3)]
        ↳ AT Translated Set 2 keyboard            	id=12	[slave  keyboard (3)]

Nos fijamos en el id y desactivamos de este modo: 
    
    
    xinput set-prop $ID "Device Enabled" 0  #Para activarlo, evidentemente, 1

Esto me abre un nuevo mundo de posibilidades para mi sección "Jugando a ser hacker" xD

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
