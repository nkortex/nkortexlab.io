---
title: (ES)(Bash scripting) Lanzar notificaciones en tus scripts
date: 2013/06/15 09:20:17
---
Si en algún momento de un script quieres darle información al usuario sobre algún aspecto en particular, como que ha acabado correctamente, que ha habido un error,... lo mejor es hacerlo mediante notificaciones. Lanzar estas notificaciones es de lo más fácil gracias al paquete **notify-send.** Este paquete nos permte enviar notificaciones fácilmente de esta manera: 
    
    
    notify-send "Texto de la notificación"

Además, tiene opciones para cambiar el icono que muestra y otros parámetros. Estas opciones podéis verlas con un --help. Para que veáis un ejemplo, el script de abajo comprueba la temperatura del PC usando acpi y, si la temperatura es mayor de un límite, envía una notificación informando de la temperatura actuual, y emite un pequeño "beep": 
    
    
    #!/bin/bash
    
    #Made in http://023.es
    
    tempmax=80
    tempactual=$(acpi -t | egrep '[0-9]+.[0-9]' -o | cut -d"." -f1)
    
    texto="Alerta: Fusión en el núcleo. Temperatura: $tempactual."
    if [ $tempactual -ge $tempmax ]; then
       notify-send "$texto"        #Notificación
       echo -e "\a"                #beep
    fi

Si añadís una entrada al crontab para que se ejecute cada x minutos, podéis controlar perfectamente la temperatura de vuestra máquina. ![Captura de pantalla de 2013-06-15 11:21:21](/wp-content/uploads/2013/06/Captura-de-pantalla-de-2013-06-15-112121.png)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
