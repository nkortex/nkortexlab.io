---
title: (ES)Juegos FPS obligatorios para Linux
date: 2013/01/20 13:55:39
layout: post
path: "/important-fps-games-linux/"
category: "Imported from my older blog"
---
Ya va siendo hora de eliminar el mito de que en linux no se puede jugar. Es cierto que los gráficos son mas pencos, pero eso es cosa de las tarjetas. Aun y así, hay juegos muy muy buenos a los que merece la pena darles una oportunidad. **Assault Cube:** Es un juego multijugador, de matarse los unos a los otros, lo típico. Perfecto para liberar tensiones. Lo podéis bajar de: <http://sourceforge.net/projects/actiongame/?source=recommended> ![](http://sourceforge.net/projects/actiongame/screenshots/ac_complex_DM_gren_large00.jpg) **Urban Terror:** Igual que el anterior, pero con mejores gráficos. Se baja desde: <http://www.urbanterror.info/downloads/> ![shot0010](/wp-content/uploads/2013/01/shot0010.jpg) Sauerbraten: cube2: La dinámica es la misma que los anteriores pero tiene modos de juego bastante interesantes, como el modo 1tiro 1muerto,... Se baja desde: <http://sauerbraten.org/> ![](http://sauerbraten.sourceforge.net/newerer/screenshot_1758308.jpg) Si queréis jugar conmigo, avisad!

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
