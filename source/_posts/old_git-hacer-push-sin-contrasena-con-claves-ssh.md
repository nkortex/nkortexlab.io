---
title: (ES)(git) Hacer push sin contraseña, con claves SSH
date: 2014/06/14 09:53:57
layout: post
path: "/make-push-with-public-key/"
category: "Imported from my older blog"
---
Últimamente vengo utilizando git como sistema de control de versiones en los proyectos que tengo entre manos. Una de las cosas que más tiempo me ahorra a lo largo del día* es el uso de claves ssh para autentificarme en Bitbucket al trabajar con los repositorios. Lo que conseguimos con este tutorial es no tener que introducir la contraseña ni una vez más: Primero generamos una clave: 
    
    
    cd ~
    ssh-keygen

Te pide la ruta donde lo guardará y una contraseña. La contraseña déjala en blanco las dos veces. Ahora tienes que copiar el contenido de tu clave pública (se ha generado un archivo .pub) en alguna parte de la configuración de tu servidor git. En Bitbucket es así: ![Captura de pantalla de 2014-06-14 11:28:27](/wp-content/uploads/2014/06/Captura-de-pantalla-de-2014-06-14-112827.png)   Pinchamos en agregar clave, le ponemos opcionalmente un nombre y pegamos la clave pública que será algo parecido a esto: 
    
    
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCu05RwgYRsFZcap60yxdjJow8W1bVN9jzyDIHGehizBupstxuVMnS1+oH[Contenido_Alterado]+lWVjiqfNUIc/eFAmWs3+DCH/vi1LhqgtDXXKJ8cD08RZFZ7TBq3sgxZ1eZRKG8cOFvV+bg2Bv7Pv4BLrtqLUL3KggUdwth9TPFD3rQZh54/DAXvCZHJ0D9WxUNY5zHtS4fq55tQ+pXBd3 user@user

Luego ejecutamos en local: 
    
    
    ssh-add ~/.ssh/clave_privada_rsa

Y cambiamos la url del repositorio para utilizar protocolo ssh en vez de http. Esta opción se encuentra en la configuración del repositorio (carpeta .git). Una vez hecho esto y guardados los cambios, podemos interactuar con el repositorio sin las malditas contraseñas. \-- * Quizás exageré un poco xD

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
