---
title: (ES)(Bash Scripting) Matrices y arrays multidimensionales en bash
date: 2014/04/24 20:05:06
---

Una de las carencias de bash es la imposibilidad de crear vectores de más de una dimensión. Algo que en cualquier otro lenguaje se puede hacer así de simple: matriz[2][4], en bash es imposible. Para paliar este asunto he creado una función que emula una matriz bidimensional. Su funcionamiento es sencillo: se aprovecha de que el tamaño de un vector en bash es virtualmente ilimitado para meter todos los datos de una matriz multidimensional en un vector tradicional. La función es esta: 
    
    
    function matrix2d
    {
    	if [[ $1 == "new" ]] ; then
    		MATRIX=0
    		MAX_X_MATRIX=$2
    		MAX_Y_MATRIX=$3
    	fi
    
    	X_MATRIX=$1
    	Y_MATRIX=$2
    	valor=$3
    
    	if [[ $# -eq 3 ]] ; then
    		MATRIX[$(($MAX_Y_MATRIX*$X_MATRIX+$Y_MATRIX))]=$valor
    	fi
    
    	if [[ $# -eq 2 ]] ; then
    		echo -n ${MATRIX[$(($MAX_Y_MATRIX*$X_MATRIX+$Y_MATRIX))]} 
    	fi
    }

Para utilizarla, debes crear un archivo "matrix2d" en el directorio del script que lo vaya a usar, copy-paste del código de arriba, e incluirla en el script así: 
    
    
    . matrix2d

Veamos el uso con un ejemplo: 
    
    
    . matrix2d      #Hacemos include a la función, 
    
    matrix2d new 10 10    #Creamos una nueva matriz 10 x 10 con la palabra reservada new
    
    #al pasarle 3 parametros a la matriz, introduce en la posicion $1 $2, el tercer parametro
    for X in $(seq 1 10) ; do
    	for Y in $(seq 1 10); do
    		matrix2d $X $Y $Y    
    	done
    done
    
    #al pasarle 2 parametros , devuelve el valor que tuviera.
    for X in $(seq 1 10) ; do
    	for Y in $(seq 1 10); do
    		matrix2d $X $Y
    	done
    	echo " "
    done

Esto se puede expandir de forma fácil a matrices de 3 o más dimensiones. No es de lo más eficiente, pero para sacarnos de un apuro puede valer. Ahora os pregunto: ¿se os ocurre alguna manera de permitir declarar varias matrices, de tamaños distintos, con una única función? Si tenéis algún problema o sugerencia, los comentarios están abiertos ;)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[manuel a.j.](#415 "2015-05-23 14:22:06"):** Para declarar varias matrices de tamaños distintos yo lo haría asi: Crear un array con la siguiente información: matrices=("A:x,y" "[a,b,c,d][f,j,h]" "B:u,v" "[d,f,g][j,k,l]") Asi de esa forma la matriz siempre ocuparía dos posiciones del array, la primera el nombre con su tamaño, y la segunda el contenido de la matriz. Luego es sencillo extraer los datos con 'cut'



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
