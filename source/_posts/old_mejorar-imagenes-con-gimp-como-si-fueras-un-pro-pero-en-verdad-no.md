---
title: (ES)Mejorar imágenes con Gimp como si fueras un pro, pero en verdad no
date: 2013/03/13 20:14:36
layout: post
path: "/gimp-tutorial/"
category: "Imported from my older blog"
---
Surfeando los foros más recónditos de Internet en busca de un sistema de reconocimiento facial para linux, he ido saltando de tema en tema (lo típico) y he acabado en un topic de uso de **gimp**. El 99% de lo que decía era muy básico, pero señala un truco que no conocía y que da muy buenos resultados en la mayoría de las imágenes. Se trata de realizar el balance automático de blancos. Para ello abrimos una imagen con **gimp**, nos vamos a color > auto > balance de blancos. Así de fácil! Aquí os dejo un ejemplo: (click para ampliar) ![mejora1](/wp-content/uploads/2013/03/mejora1.jpg)   Y la imagen mejora mucho si además le quito los cables (os expliqué como en [este post](http://023.es/como-borrar-partes-de-fotografias-de-manera-inteligente-desde-gimp/)): ![mejorada2](http://023.es/wp-content/uploads/2013/03/mejorada2-1024x577.jpg) Algún lector reconoce desde donde se ha hecho la foto? x)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
