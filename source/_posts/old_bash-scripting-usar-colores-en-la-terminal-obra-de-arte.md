---
title: (ES)(Bash Scripting) Usar colores en la terminal + Obra de arte
date: 2013/07/04 08:27:39
layout: post
path: "/terminal-colors/"
category: "Imported from my older blog"
---
Muchas veces es bueno utilizar colores en los script para hacer que los mensajes mostrados den más información. Por ejemplo, los errores en rojo, las advertencias en naranja y los mensajes estándar en azul. Para cambiar estos colores hay que añadir unos códigos antes de la cadena que quieras imprimir. Por ejemplo: ![Captura de pantalla de 2013-07-04 10:00:26](/wp-content/uploads/2013/07/Captura-de-pantalla-de-2013-07-04-100026.png)   En la imagen podéis ver como, cambiando el número del código, cambia el color. Fijaos también en que si no añadís la opción** -e** a echo, imprimirá el contenido literalmente, con el color por defecto. Los códigos de color son estos: (Tabla extraída de [aquí](https://wiki.archlinux.org/index.php/Color_Bash_Prompt_\(Espa%C3%B1ol\))) 
    
    
    Negro       0;30     Gris Obscuro  1;30
    Azul        0;34     Azul Claro    1;34
    Verde       0;32     Verde Claro   1;32
    Cyan        0;36     Cyan Claro    1;36
    Rojo        0;31     Rojo Claro    1;31
    Purpura     0;35     Fiuscha       1;35
    Café        0;33     Amarillo      1;33
    Gris Claro  0;37     Blanco        1;37

Cambiando el código en la cadena **\e[(numero)m** se establece el color correspondiente. El 1 o el 0 cambia entre claro y oscuro respectivamente: ![Captura de pantalla de 2013-07-04 10:07:30](/wp-content/uploads/2013/07/Captura-de-pantalla-de-2013-07-04-100730.png)   La terminal también es capaz de cambiar el color de fondo de cada carácter, usando otros códigos: ![Captura de pantalla de 2013-07-04 10:11:06](http://023.es/wp-content/uploads/2013/07/Captura-de-pantalla-de-2013-07-04-101106.png)   Algunos códigos de fondo son: 
    
    
     f_negro=40
     f_rojo=41
     f_verde=42
     f_amarllo=43
     f_azul=44
     f_rosa=45
     f_cyan=46
     f_blanco=47

Y ahora, como ejemplo de uso de los colores, os dejo un script que genera arte de forma aleatoria :D 
    
    
    #!/bin/bash
    #FONDOS
    fondo[0]="\e[40m"
    fondo[1]="\e[43m"
    fondo[2]="\e[44m"
    fondo[3]="\e[45m"
    fondo[4]="\e[46m"
    fondo[5]="\e[47m"
    fondo[8]="\e[41m"
    fondo[9]="\e[42m"
    
    for y in $(seq 1 2000)
    do
       fon=$(($RANDOM % 10))
       echo -e -n "${fondo[$fon]} · \e[49m"
    done

Y el resultado es: ![Captura de pantalla de 2013-07-04 10:29:40](/wp-content/uploads/2013/07/Captura-de-pantalla-de-2013-07-04-102940.png) Tachan!

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
