---
title: (ES)Abrir como root en Nautilus
date: 2013/01/08 17:46:28
layout: post
path: "/open-as-root-nautilus/"
category: "Imported from my older blog"
---
El otro día le arreglé el portátil a mi hermana como regalo de Navidad. Le borré el Windows Vista y le instalé una distro linux simple, potente y capaz de realizar todas las tareas que ella suele hacer (música, vídeos, ofimática...): **Linux Mint. **Y la verdad es que me dio envidia que en su explorador de archivos, thunar, había una opción de abrir la carpeta actual como root directamente desde el menú contextual; así que me he puesto a investigar y he averiguado como hacerlo en Nautilus (sin la chapuza del "sudo nautilus"): 

  1. Creamos un archivo nuevo: 
    
        sudo gedit .gnome2/nautilus-scripts/Open\ as\ root

  2. En el archivo abierto escribimos: 
    
         for uri in $NAUTILUS_SCRIPT_SELECTED_URIS; do 
    
         gksudo "gnome-open $uri" & 
    
         done

  3. Guardamos y cerramos
  4. Vamos al terminal y le damos permisos de ejecución a ese archivo, escribiendo: 
    
         sudo chmod +x .gnome2/nautilus-scripts/Open\ as\ root

  5. Reiniciamos Nautilus y en el menú contextual se nos habrá creado una nueva entrada para abrir la carpeta como root
Veis que fácil? x)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
