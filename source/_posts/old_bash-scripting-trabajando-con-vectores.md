---
title: (ES)(Bash Scripting) Trabajando con vectores
date: 2014/01/26 09:30:34
---

Hoy os traigo un mini-tutorial que puede ser muy útil a los que programan en bash normalmente, acerca de como trabajar con vectores o arrays. Antes de nada, para quien no lo sepa, la forma de declarar vectores en bash es la siguiente: 
    
    
    $ nombre_vector[2]=6          #Introduce 6 en la posición 2 del vector
    $ nombre_vector[3]=e          #Introduce e en la posición 3 del vector

Luego, para recuperar los distintos valores: 
    
    
    $ echo ${nombre_vector[2]}
    6
    $ echo ${nombre_vector[3]}
    e

Normalmente se utilizan bucles para rellenar los vectores fácilmente, de una forma parecida a esta: 
    
    
    #Forma un vector con los primeros 100 números pares
    for x in $(seq 1 100);do
       pares[$x]=$(($x*2))
    done

Aunque también se pueden recuperar los valores del vector con un bucle, bash nos ofrece otras 2 maneras (equivalentes) para hacerlo de forma sencilla: 
    
    
    $ echo ${pares[*]}
    2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 72 74 76 78 80 82 84 86 88 90 92 94 96 98 100 102 104 106 108 110 112 114 116 118 120 122 124 126 128 130 132 134 136 138 140 142 144 146 148 150 152 154 156 158 160 162 164 166 168 170 172 174 176 178 180 182 184 186 188 190 192 194 196 198 200
    $ echo ${pares[@]}
    2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 66 68 70 72 74 76 78 80 82 84 86 88 90 92 94 96 98 100 102 104 106 108 110 112 114 116 118 120 122 124 126 128 130 132 134 136 138 140 142 144 146 148 150 152 154 156 158 160 162 164 166 168 170 172 174 176 178 180 182 184 186 188 190 192 194 196 198 200

Por último, podemos recuperar el número de valores contenidos en un vector determinado de este modo: 
    
    
    $ echo ${#pares[@]}
    100

**EXTRA:** El uso de vectores nos puede ayudar también para saber el tamaño de una variable, o el numero de palabras que tiene. Lo mejor es verlo con un ejemplo: 
    
    
    #!/bin/bash
    
    #Recibe x parametros arbitrarios (x>0)
    
    if [ $# -eq 0 ]
    then
      echo "Necesarios 1 o más argumentos"
      exit 1
    fi  
    echo $#
    
    var01=qwerty
    echo "var01 = $var01"
    echo "Tamaño var01 = ${#var01}"
    var02="Damian Baldomero Punto Com"		#Los espacios cuentan como carácter
    echo "var02 = $var02"
    echo "Tamaño var02 = ${#var02}"
    
    #Intenté ligar con una programadora... pero no se deJava
    
    echo "Numero de argumentos (palabras) = ${#@}"
    echo "Numero de argumentos (palabras) = ${#*}"		#2a manera
    echo "Numero de argumentos (palabras) = $#"		    #3a manera
    aux="$@"
    echo "Tamaño de los argumentos (caracteres)= ${#aux}"
    
    exit 0

[Fuente](http://tldp.org/LDP/abs/html/)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Born](#396 "2015-01-01 13:28:08"):** Hola! Buen blog llevas y con mucha informacion interesante. Lo unico malo es que hay que moverse por todas las paginas para ver entradas al posts. I al final una pregunta. ¿Puede una persona coriente sin estudios en informatica y programacion aprender y entender Bash? Gracias

**[admin](#399 "2015-01-05 12:24:05"):** Hola Born, no entiendo que quieres decir con que hay que moverse por todas las páginas :S Y por supuesto, todo es echarle ganas y tiempo ;)



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
