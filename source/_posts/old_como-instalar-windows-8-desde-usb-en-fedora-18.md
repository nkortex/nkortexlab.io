---
title: (ES)Como instalar Windows 8 desde USB en Fedora 18
date: 2013/03/18 16:49:31
layout: post
path: "/install-w8-livecd/"
category: "Imported from my older blog"
---
Podéis matarme. Estoy escribiendo una entrada sobre como instalar Windows 8 desde Linux. Y os preguntaréis: ¿Para que quiero Windows teniendo Linux, no sería eso un paso atrás? Efectivamente, lo es; pero por desgracia la mayoría de los juegos los hacen para Windows (Aunque se pueden correr en Linux, no van igual, por mucho que nos empeñemos). Asi que, con todo el dolor de mi alma, voy a explicar como hacernos un LiveUSB para instalar Windows 8 junto a nuestra distro favorita. AVISO: Doy permiso para usar la información de este post en caso de que la vayas a usar para instalar Windows **junto a** Linux. Si pretendes instalar Windows **en vez de** Linux lárgate, no eres bienvenido aquí :) Dicho esto, empiezo. Lo primero es bajarse la imagen de W8. Ni se te ocurra comprarlo, ya has pagado muchas licencias a Microsoft a lo largo de tu vida. Aquí tenéis la ISO:  [Enlace Magnet](magnet:?xt=urn:btih:b3416ea9ae6e5d0e84ca4dbe13a81f622774d24c&dn=Windows+8+Pro+x64+Untouched+ISO+%2B+KMS+activator&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80) Ahora nos bajamos el WinUsb, que sirve para hacer USBs booteables. Desde aquí nos podemos bajar los paquetes .deb o el código fuente. Si tu distro no acepta .deb simplemente abres un terminal, te vas a la carpeta donde lo hayas descomprimido y ejecutas: 
    
    
    ./configure
    make
    make install
    

Ahora abrimos el programa y nos saldrá algo parecido a esto: ![Captura de pantalla de 2013-03-18 16:47:13](/wp-content/uploads/2013/03/Captura-de-pantalla-de-2013-03-18-164713.png)   A partir de aquí todo es bastante intuitivo, en verdad. Seleccionamos la iso o el DVD donde tengamos el güindous y en tarjet device el USB donde queramos instalarlo. Ni que decir tiene que todo el contenido que tenga el usb se va a borrar, así que cuidadín. Si no te funciona correctamente, puedes usar el programa a través de línea de comandos ejecutando: 
    
    
    winusb --format en_windows_8_x64_dvd_915440.iso /dev/sdb

y cambiando en_windows_8_x64_dvd_915440.iso por tu archivo y /dev/sdb por la ruta a tu USB. Mientras que acaba, vamos fabricándonos una partición NTFS del tamaño que nos interese para instalar el Windows con gParted. Luego reiniciamos y seguimos el instalador a prueba de tontos que nos dan los de Mocosoft. :D !!!OJO!!! Hacer esto borra el Grub xD

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
