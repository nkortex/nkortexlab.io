---
title: (ES)(Bash Scripting) Tutorial SQLite
date: 2013/10/20 11:40:18
layout: post
path: "/sqlite-tutorial/"
category: "Imported from my older blog"
---
Para almacenar datos desde nuestros scripts puede ser útil un archivo de texto plano con un formato determinado; pero si la cantidad de datos que queremos almacenar es bastante grande, queremos más eficiencia en el acceso y manipulación de los datos o queremos más facilidad a la hora de intercambiar datos con otros programas, lo mejor es trabajar con bases de datos. Así que vamos a ver como utilizar una base de datos **SQLite** desde la terminal o desde nuestros scripts. ![](http://www.sqlite.org/images/sqlite370_banner.gif) Lo primero que tenemos que hacer es instalar el sistema gestor:

sudo yum install sqlite sudo apt-get install sqlite
Sqlite se puede usar desde su propio prompt, ejecutando **sqlite3**, o mediante redirecciones, que es el que nos interesa a nosotros.Como esta parte se centra en SQLite y no en el lenguaje SQL, vamos a comentar tan solo los distintos comandos y su modo de uso. El comando principal es:

sqlite3 "datos.db"
Esto creará la base de datos "datos.db". Si ya existe, la abrirá. Para hacer consultas desde terminal, por ejemplo:

sqlite3 datos.db "SELECT * FROM Tabla1;"
Metacomandos:

.exit .quit
Sale del prompt

.help
Muestra la ayuda

.read
Lee un script SQL

.dump
Muestra los comandos en SQL necesarios para crear una determinada tabla

.headers on/off
Muestra o no las cabeceras

.mode
Cambia el formato de salida. Pueden consultarse los formatos disponibles en la ayuda. Algunos de ellos son: csv column html insert line list

.nullvalue CADENA
Utiliza CADENA en lugar de NULL

.output ARCHIVO
Redirecciona la salida a ARCHIVO. Si se especifica como archivo stdout, saldrá por pantalla.
Espero que sea de utilidad ;)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
