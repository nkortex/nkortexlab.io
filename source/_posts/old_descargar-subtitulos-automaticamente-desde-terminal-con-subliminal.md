---
title: (ES)Descargar subtítulos automáticamente desde terminal con Subliminal
date: 2013/08/11 10:14:32
layout: post
path: "/download-subtitles-automagically-subliminal/"
category: "Imported from my older blog"
---

Hace ya un tiempo escribí un post acerca de como bajarse subtítulos de series o películas de forma mas o menos automática gracias a un plugin de VLC (puedes leer el post aquí: ). Pero ahora he encontrado un nuevo método para hacer la descarga a través del terminal gracias a **subliminal**. El hecho de que no tenga interfaz gráfica hace que bajarse los subtítulos sea lo más sencillo de automatizar. Lo primero que tenemos que hacer es bajarnos el programa desde su [github](https://github.com/Diaoul/subliminal/tree/master). Luego lo descomprimimos, abrimos un terminal en la carpeta y ejecutamos: 
    
    
    python setup.py build
    sudo python setup.py install

Con esto lo tendremos instalado. Ahora, para ejecutarlo: 
    
    
    subliminal -l es capitulo.avi

La opción -l indica el idioma en el que se los va a descargar. Una vez encontrados, los descargará con el mismo nombre que el archivo de vídeo, para que el reproductor los detecte de forma automática. Si ya queréis dejar de preocuparos por el tema de subtítulos, aquí podéis ver como ejecutarlo desde cron: <http://subliminal.readthedocs.org/en/latest/user.html#cron-job> Gracias a [eckelon](http://eckelon.net/informatica-e-internet/how-to/como-descargar-subtitulos-automaticamente-con-subliminal.html) por la info ;)[ ](http://eckelon.net/informatica-e-internet/how-to/como-descargar-subtitulos-automaticamente-con-subliminal.html)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[eckelon](#204 "2013-08-11 19:12:31"):** Me alegra que te haya servido mi apunte! Subliminal es especialmente útil para descargar todos los subs de una temporada de golpe (usando el *.mkv, por ejemplo :) Saludos!

**[admin](#207 "2013-08-14 15:35:42"):** Muchas gracias por el apunte, por la visita y por el comentario. Espero compartir más información entre nuestros blogs ;)

**[carnet manipulador de alimentos](#358 "2014-07-01 17:05:38"):** Está fenomenal.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
