---
title: (ES)Acceder a red wifi a partir de un código QR
date: 2013/01/05 11:15:45
layout: post
path: "/qr-wifi-generator/"
category: "Imported from my older blog"
---
Para evitar tener que meter una contraseña super larga cada vez que te quieras conectar a un wifi, o alguien se quiera conectar al tuyo, os voy a mostrar un modo de conectarse simplemente capturando un código QR con vuestro móvil Android. Esto es especialmente útil para bares y locales con wifi, para que los clientes no tengan que meter la contraseña a mano. Lo único que hay que hacer es ir a esta página: <http://zxing.appspot.com/generator> En contents escogemos "wifi network". SSID y Password lo rellenamos con el nombre y la contraseña de nuestra red; y network type elegimos el tipo de encriptación que tenga: WEP, WPA o ninguna. Si no tiene ninguna seguridad no sé para que estamos haciendo esto, pero bueno xD Abajo te ponen más opciones. El tamaño del código y la tolerancia a errores. En la tolerancia os recomiendo M o Q, ya que H genera un código demasiado grande y L es demasiado propenso a fallar; pero esto ya es según vuestras necesidades. La codificación no la cambiéis, no es preciso. Por último, pulsáis "Generate" y guardáis el resultado. Una buena idea para usar esta herramienta es hacer códigos QR de todos los wifis que conozcáis, los imprimáis y los peguéis en la calle junto a unas pequeñas instrucciones de uso. Así ayudas a otros que no tengan conexión 3G a poder usar internet :D ![](http://chart.apis.google.com/chart?cht=qr&chs=230x230&chld=M&choe=UTF-8&chl=WIFI%3AS%3AMiWifetaso%3BT%3AWPA%3BP%3Acontrase%C3%B1as%40%3B%3B)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
