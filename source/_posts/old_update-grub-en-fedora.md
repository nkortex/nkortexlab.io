---
title: (ES)update-grub en Fedora
date: 2013/01/16 19:25:14
layout: post
path: "/update-grub-fedora/"
category: "Imported from my older blog"
---
![](http://www.acik-kaynak.net/image/cache/data/Fedora_logo-v2-480x480.png) Uno de los comandos que mas se echa en falta en Fedora es el de update-grub . Este comando actualiza el grub para aplicar alguna modificación de la configuración o para que se añada a la lista un sistema añadido recientemente. Lo malo es que ese comando solo se podía usar en Debian y derivados; hoy os enseño a configurar tu Fedora para poder utilizarlo. El comando en cuestión es: 
    
    
    grub2-mkconfig -o /boot/grub2/grub.cfg

Pero es demasiado dificil como para acordarnos, asi que vamos a crear un alias. Primero, abrimos el archivo .bashrc de tu home y añadimos esta línea: 
    
    
     alias update-grub='grub2-mkconfig -o /boot/grub2/grub.cfg'

Ahora reiniciamos el sistema y podemos usarlo cuando queramos :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
