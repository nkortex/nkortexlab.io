---
title: (ES)Mi .bashrc perfecto
date: 2012/12/23 13:25:49
layout: post
path: "/my-perfect-bashrc/"
category: "Imported from my older blog"
---
Para los que no sepan los que es el archivo .bashrc es un archvo oculto que todos tenemos en nuestro home (todos los que tengamos linux. Si usas Windows o Mac, te pido que abandones el blog y no vuelvas hasta que no hayas madurado) Este archivo se carga automáticamente cada vez que se abre un terminal. Esto le dá mucho juego y ahora os voy a explicar como lo tengo personalizado: 
    
    
    # .bashrc
    
    # Source global definitions
    if [ -f /etc/bashrc ]; then
    	. /etc/bashrc
    fi
    
    #Alias personalizados por mi
    
    alias instalar='sudo yum install'
    
    alias actualizar='sudo yum update'
    
    alias abrelampp='sudo /opt/lampp/lampp start'
    
    alias cierralampp='sudo /opt/lampp/lampp stop'
    
    alias reboot='sudo reboot'
    
    alias please='sudo'
    
    alias info='screenfetch-dev'
    
    alias leds='sudo tleds -d 10 wlan0'
    
    alias resolucion='xrandr'
    
    #Personalizacion del terminal
    
    PS1="\[\e[33;1m\]─(\[\e[31;1m\]\u\[\e[33;1m\]) » {\[\e[34;1m\]\w\[\e[33;1m\]}\[\e[0m\] "
    
    #letras guays
    
    echo "----------------------------------------------------------------------------"
    figlet María Mola Mazo
    echo "----------------------------------------------------------------------------"

Lo que pone antes de los alias es lo que venía por defecto. No sé exactamente que hace, así que mejor no tocar. Luego, los alias. Sirven para renombrar funciones para que sean más fáciles de recordar y más rápidas de escribir. Tengo unos para instalar programas y actualizar el sistema. En vez de escribir "sudo yum install firefox" pongo "instalar firefox" y ya mi terminal me entiende. También tengo alias para abrir y cerrar el lampp, para mostrar la información de mi distro, para ejecutar la aplicación tleds y para cambiar la resolución de la pantalla. Después tengo la personalización del prompt. Con colores y demás. A mi gusto todo. Y por último tengo el mensaje que me sale cada vez que abro la terminal: María Mola Mazo. En letras grandes y bonitas. Os gusta?

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
