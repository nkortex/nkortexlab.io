---
title: (ES)Xvfb, trabajando con pantallas virtuales en Linux
date: 2013/12/15 11:39:51
layout: post
path: "/virtual-displays-on-linux/"
category: "Imported from my older blog"
---

Si usamos nuestros scripts para automatizar cosas, es probable que tengamos que utilizar algún programa con interfaz gráfica. Para esto, la solución es configurar pantallas virtuales. Estas pantallas funcionan igual que un monitor, pero sin mostrar nada por pantalla, como si estuvieran en segundo plano. Para empezar, instalamos **Xvfb** y algunas dependencias: 
    
    
    sudo yum install Xvfb xorg-x11-server-Xvfb xfwm4          #Ojo, la X mayúscula

Luego creamos la nueva pantalla con la resolución que queramos: 
    
    
    Xvfb :1 -screen 0 1024x768x24 &

Esto creará la pantalla virtual :1 con una resolución de 1024x768x24, y la opción -screen 0 hará que no se muestre en ningún monitor. En vez de :1, podemos crear la pantalla :2, :3,... Ahora, para ejecutar algún programa en esta pantalla, por ejemplo Firefox, lo lanzamos de este modo: 
    
    
    DISPLAY=:1 Firefox &

### Teclado y ratón virtuales

Una vez tenemos el programa corriendo, aún sin tener pantalla, tenemos que interactuar con el. Y el ratón y teclado físicos no sirven. Para esto vamos a instalar el paquete **xdotool**: 
    
    
    sudo yum install xdotool

Vamos a ver algunas de sus opciones: 

  * xdotool key Down [Enter/Return/Tab/...]          Presiona la tecla indicada
  * xdotool key shift+60                                      Combinación de teclas
  * xdotool type "palabra"                                    Escribe el texto pasado como parámetro
  * xdotool search '[^"]Firefox' windowactivate        Activa la pantalla cuyo nombre acaba en Firefox
  * xdotool mousemove x y                                 Mueve el ratón a la posición [x,y]
  * xdotool mousemove restore                            Mueve el ratón a la posición anterior
  * xdotool mousemove_relative x y                      Mueve el ratón a la posición anterior, pero pasándole un movimiento relativo
  * xdotool click [0-5]                                          Hace click al botón pasado por parámetro: 1=izquierdo, 2=derecho, 3=central, 4=rueda
  * xdotool mouseup/mousedown [0-5]                 Igual que click, pero solo manda la señal depulsar/soltar. Útil para los drag n drop.
  * xdotool getmouselocation                              Devuelve la posición actual del ratón
Esta herramienta tiene también opciones para manejar ventanas, como veremos más adelante. Los comandos escritos arriba están funcionando en la pantalla predeterminada. Para ejecutarlos en la pantalla virtual que hemos creado antes lo hacemos así: 
    
    
    DISPLAY=:0 xdotool key Down

### Capturar pantalla

Para comprobar si todo está funcionando podemos hacer capturas de pantalla y descargarlas del servidor o mandarlas por correo. Para hacer una captura de la pantalla :1 ejecutamos: 
    
    
    DISPLAY=:1 import -window root captura.jpg

(es necesario el paquete ImageMagick) 

### Supervisión automática

Nunca podemos dar por hecho que un programa no va a fallar, menos todavía cuando interviene una interfaz gráfica y una conexión a Internet. Para supervisar que todo va correctamente podemos hacer capturas de pantalla y revisarla cada cierto tiempo. Pero esto también se puede automatizar; para ello vamos a usar el paquete ImageMagick: 
    
    
    sudo yum install ImageMagick

El método será hacer una captura de pantalla una vez que se ejecute, luego seleccionar un pixel de una zona que tengamos previamente estudiada. Dependiendo del color de ese pixel, sabremos si ha ido bien o no. Para ver el color del pixel: 
    
    
    convert imagen.jpg -format '%[pixel:p{40,30}]' info:-

De este modo es relativamente fácil saber si la operación acabó de forma correcta,comparando con los valores esperados.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[jsbsan](#347 "2014-05-28 08:27:01"):** Muy bueno... pantallas virtuales :)

**[fracielarevalo](#348 "2014-05-29 01:20:29"):** muy interesante lo felicito y agradesco que comparta esta aplicasion



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
