---
title: (ES)(Bash Scripting)Manejando el portapapeles desde línea de comandos
date: 2013/11/05 18:12:44
---

Muchas veces, en vez de recibir respuesta de nuestros scripts por pantalla, puede ser interesante que la dejara en el portapapeles para poder usarla luego. También podemos necesitar en un momento dado utilizar el contenido del portapapeles para algo. ![](http://www3.uji.es/~vrubert/unimajors/2011-iim/sesion03/images/clipboard.jpg) La forma en que yo trabajo con el portapapeles es con **xclip**: 
    
    
    sudo yum install xclip
    sudo apt-get install xclip

Ahora nos añadimos unos alias en nuestro archivo .bashrc (en nuestro home): 
    
    
    alias ctrlv='xclip -selection c -o'
    alias ctrlc='xclip -selection c'

El nombre de los alias lo podemos cambiar, pero esos nombres me parecen bastante autoexplicativos. Una vez hecho eso, basta con redireccionar la salida de un script mediante un cauce a "ctrlc" para copiarla: 
    
    
    echo "Hola Mundo" | ctrlc

Si pegamos en otro lado podemos ver como nos ha copiado el resultado. Y para recuperar el contenido del portapapeles, ejecutamos: 
    
    
    ctrlv

Un pequeño truco que nos puede hacer la vida más fácil. PD: Este es el post número 100 de este, mi humilde blog. Gracias por leerme !! ;D ;D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[BGBgus](#275 "2013-11-06 10:00:04"):** Esto puede ser realmente útil para un servidor sin interfaz gráfica. De todas formas, creo que faltaría decir que en el terminal, al menos en Ubuntu, que es donde lo he probado, puedes utilizar Ctrl.+Shift+C/V para copiar al portapapeles la línea seleccionada. Pensandolo mejor, tendrá más que ver con Gnome-Terminal que con Ubuntu, pero yo lo digo xD Mi senicillísimo método no funcionará, repito, en un sistema sin interfaz gráfica.

**[admin](#276 "2013-11-06 15:57:16"):** De todos modos, si a una máquina sin interfaz gráfica te conectas mediante ssh, puedes utilizar el portapapeles del cliente, con lo que conseguirías un resultado muy parecido



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
