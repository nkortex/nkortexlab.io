---
title: (ES)Instalar clementine en fedora/ubuntu
date: 2013/01/08 17:32:51
---
Y otro tutorial de mis aportaciones a usemos linux! **Clementine** es un reproductor de música derivado de la versión 1.4 de **Amarok**, pero que incluye tantas **novedades** y **mejoras** que parece un reproductor completamente distinto. A finales del año pasado se publicó la versión 1.0. Tras el tremendo cambio que introdujeron en Amarok en las versiones 1.4 y posteriores, algunos de los usuarios descontentos con el camino que estaba tomando su reproductor favorito se pusieron manos a la obra para crear un reproductor mucho mejor. Eso es lo bueno que tiene el software libre, si no te gusta algo lo cambias y listo. Gracias a esto hoy podemos disfrutar de este gran programa. 

  * Las principales características que incorpora Clementine en su versión 1.0 son:
  * Capacidad de crear listas de reproducción inteligentes.
  * Organización de las listas en pestañas.
  * Letras de las canciones y biografías de los artistas.
  * Visualizaciones ProjectM.
  * Conversor de archivos a MP3, OGG, FLAC,...
  * Descarga automática de caratulas de álbumes.
  * Sincronización con reproductores MP3 y iPod.
  * Control remoto mediante el mando de Wii WiiMote.
  * Conexión con Last.fm, Spotify, DI.com, ...
Como veis, no le falta detalle a este programa así que os recomendamos que lo probéis y veáis como os acaba convenciendo. ![](http://1.bp.blogspot.com/-PQY2zlegC9k/T9mrhWD9TcI/AAAAAAAAAvw/cPeSZI1Utv0/s640/clementinefoto.png) Dado que es multiplataforma, podrás descargarlo para Windows, Mac, Ubuntu, Debian y Fedora. En la página oficial: <http://www.clementine-player.org/es/downloads> tienes los paquetes correspondientes. También lo puedes conseguir a través de los repositorios. Para ello abre la terminal y escribe: En Ubuntu: 
    
    
    sudo apt-get install clementine

En Fedora: 
    
    
    sudo yum install clementine

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
