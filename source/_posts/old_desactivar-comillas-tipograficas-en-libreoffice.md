---
title: (ES)Desactivar comillas "tipográficas" en libreoffice
date: 2013/04/14 18:34:30
layout: post
path: "/turn-off-quotation-marks-libre/"
category: "Imported from my older blog"
---
Si alguna vez habéis estado trabajando en algún documento con LibrreOffice en el que insertábais trozos de código, puede que os haya dado el problema de que sustituye las comillas normales: " por comillas tipográficas:  “ ”. Esto puede ser un problema si luego alguien hace un copy-paste y no le funciona nada del código que le has pasado. ![](http://ayudawordpress.com/wp-content/uploads/2012/08/comillas-tipogr%C3%A1ficas-y-comillas-normales-185x180.jpg) Una posible solución a esto es trabajar con editores sin formato (gedit, kate, nano, vi o incluso notepad). Pero si necesitáis incluir algo de formato podéis arreglar el problema desactivando la opción "Reemplazar comillas dobles". Esta opción se encuentra en: **Herramientas > Opciones de autocorrección** > **Opciones regionales** > **Reemplazar**. :D

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
