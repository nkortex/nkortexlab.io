---
title: (ES)Bajar subtítulos automáticamente con VLC - Linux
date: 2013/01/06 11:36:59
---
Hace ya dije que iba a enseñar como bajarse los subtítulos automáticamente con VLC. Yo lo hago con VLSub, una extensión de VLC. Seguro que hay muchos mas metodos, pero este me parece el más rápido. Tendrás que bajarte esto: <http://addons.videolan.org/content/show.php/?content=148752> Luego lo mueves a /usr/share/vlc/lua/extensions así: 
    
    
    sudo mv ~/Descargas/vlsub.lua /usr/share/vlc/lua/extensions

Es posible que no tengas creada la carpeta Extensions. Créala con un mkdir. Ahora abres el VLC, abres el archivo de serie o película que quieras, le das a ver > VLSub y pulsas "go", cuando haya encontrado algo seleccionas el archivo que quieras bajar y el programa lo baja, le cambia el nombre y lo añade para que automáticamente el reproductor lo reconozca. ![Captura de pantalla de 2013-01-06 12:33:08](/wp-content/uploads/2013/01/Captura-de-pantalla-de-2013-01-06-123308-1024x575.png) En la extensión, se puede cambiar el idioma que sale por defecto. Estaba en francés y yo lo cambié a español. Si quieres poner cualquier otro basta conque abras el archivo .lua con un editor de texto y busques el campo "Default language" o algo así. Lo modificas a tu gusto y lo guardas, así de fácil.

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
