---
title: (ES)Encontrar archivos grandes de forma interactiva en Linux
date: 2014/04/11 11:00:22
layout: post
path: "/find-big-files-with-ncdu/"
category: "Imported from my older blog"
---
Cuando el espacio en disco escasea y necesitamos borrar archivos grandes, la mayoría veníamos usando** find **o** du** para localizarlos. Hoy os traigo una pequeña pero útil herramienta que nos permite recorrer el sistema de archivos de forma interactiva ordenando los archivos y directorios por tamaño. Esta herramienta se llama **ncdu**. Se instala desde los repositorios y se lanza con el siguiente comando: 
    
    
    ncdu /carpeta/a/escanear

Una vez termine de escanear el directorio y todos los subdirectorios podemos empezar a revisar su tamaño. Si queremos borrar un archivo o directorio nos situamos encima y pulsamos "d". Para entrar en un directorio, flecha derecha o enter, para volver al directorio padre, flecha izquierda. Para ver todas las teclas y su función: '?' ![](http://dev.yorhel.nl/img/ncdudone.png) ;)

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
