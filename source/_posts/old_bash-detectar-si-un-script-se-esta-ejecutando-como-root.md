---
title: (ES)(Bash) Detectar si un script se está ejecutando como root
date: 2013/06/13 19:10:31
---

![](http://www.areandroid.com/wp-content/uploads/2012/06/root1.jpg) Hay veces que para que un script funcione correctamente, es necesario que se ejecute con privilegios de administrador, o root. Para detectar si el usuario está ejecutando tu script de este modo, puedes hacer la siguiente comprobación: 
    
    
    user=$(whoami)
    if [ "$user" == "root" ]; then
       esroot=true
       echo "Eres Root! :)"
    else
       esroot=false
       echo "No eres root... :'(" 
       exit
    fi

Espero que os sea de utilidad :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[SeG](#319 "2014-02-01 15:24:46"):** Genial, un must have en todos mis script a partir de ahora. Ahorra la aparición de muchos errores a causa de privilegios limitados.

**[admin](#320 "2014-02-01 19:26:42"):** Hola Seg, gracias por tu comentario, pero no veo buena idea eso de añadirlo a todos los script. Ejecutar algo con privilegios de administrador cuando no son estrictamente necesarios puede dar lugar a brechas importantes de seguridad

**[Yair](#333 "2014-03-22 23:10:59"):** Genial muchas gracias me fui muy útil para mi instalador de paquetes por materias gracias :D

**[René](#366 "2014-09-23 14:15:18"):** Muchas Gracias Admin. justo lo que estuve buscando, porque por descuido ejecute un script que necesitaba permisos de root como user común y me trajo algunos inconvenientes, que si bien los solucioné, me entró la inquietud de como hacer que el scrip no se ejecute si no tenia permisos de root.. para evitar futuras complicaciones y así encontre tu codigo que era justo lo que estaba buscando.. Muy buen aporte.. Gracias y saludos.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
