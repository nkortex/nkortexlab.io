---
title: (ES)Webscraping con PHP
date: 2013/05/28 15:14:14
layout: post
path: "/webscraping-with-php/"
category: "Imported from my older blog"
---

Hay veces que necesitamos usar los datos de una página web en la nuestra. Puesto que tenemos esa información de forma pública, podemos acceder a ella gracias a PHP. Este método es útil sobre todo en ocasiones en las que la información varía muy rápidamente. No aconsejo usarlo sin embargo si vais a sacar la información de páginas pesadas o si la información varía cada mucho tiempo, pues el tiempo de carga de tu página crecerá (tiempo de carga + tiempo que tarda en descargar la página externa). Para enseñar como es, voy a utilizar un ejemplo. Vamos a crear una página que muestre la imagen de fondo de Bing.com. Esta foto cambia cada día, así que cada día que accedamos será una foto distinta. Las funciones que vamos a usar son **file_get_contents()** y **preg_match($expr_reg , $data , $cap) **d. Si le echamos un ojo al código de Bing, vemos que el fondo está en esta parte del código: ![Captura de pantalla de 2013-05-28 16:36:19](/wp-content/uploads/2013/05/Captura-de-pantalla-de-2013-05-28-163619-1024x83.png) Por lo que escribiremos las funciones así: 
    
    
    $datos = file_get_contents("http://bing.com");
    
    if ( preg_match('|(g_img={url:\')(.*?)(\',id:\'bgDiv\')|' , $datos , $res ) )
    {
    echo "<img src=http://bing.com".$cap[2].">";
    }

Paso a explicarlo: 

  * file_get_contents se baja la página de bing(solo el código) y la guarda en $datos
  * Luego, preg_match busca una línea que empiece por "g_img={url:'" y que acabe por "',id:'bgDiv'" en la variable $datos y guarda el resultado en el array $res. La parte del medio se guarda en la 2ª posición.
  * Por último, hace un echo para publicar la imagen
El resultado sería este: ![Captura de pantalla de 2013-05-28 17:06:21](/wp-content/uploads/2013/05/Captura-de-pantalla-de-2013-05-28-170621.png) Esto es un ejemplo muy simple. Pero ya os digo, que se descargará la página entera de Bing cada vez que alguien visite la página. Espero que sea útil :D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Alex](#394 "2014-12-29 16:25:35"):** Gracias, ha sido valioso para mí tu aporte.

**[admin](#400 "2015-01-05 12:25:30"):** Me alegro! :D Gracias a ti por el comentario

**[luis](#413 "2015-04-16 12:06:53"):** Este post le falta mucha información , no muestra lo que dice el titulo



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
