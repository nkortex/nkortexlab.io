---
title: (ES)(Bash) Desordenar un fichero con bash
date: 2013/03/11 16:14:10
---

Normalmente, cuando tenemos un fichero que queremos ordenar, usamos la orden sort y nuestro PC lo hará por nosotros. Pero, que pasa si queremos desordenar un archivo? Pues eso me estaba preguntando yo ahora mismo. Estaba preparando un post para explicaros como hacernos un script que desordenara un archivo aleatoriamente cuando, por casualidades de la vida, me da por meterme en la ayuda de sort y, a que no sabéis que opción tiene? Efectivamente: la de ordenar aleatoriamente (desordenar) un archivo. Pero como ya tengo el post a medio publicar, os voy a decir como se hace para que ya lo sepáis. El código es: 
    
    
    cat archivoordenado.txt | sort -R > archivodesordenado.txt

:D

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Francisco](#351 "2014-06-06 00:45:14"):** Gracias, estaba buscando lo mismo!!.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
