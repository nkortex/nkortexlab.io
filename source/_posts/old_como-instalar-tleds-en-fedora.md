---
title: (ES)Cómo instalar Tleds en Fedora
date: 2012/12/31 11:41:55
layout: post
path: "/install tleds-fedora/"
category: "Imported from my older blog"
---
Tercer tutorial aportado a Usemos linux: **Tleds **es un programa que reconfigura los leds del teclado para mostrar la actividad de la red (subida y bajada) en vez de la información que muestran por defecto (ScrLK y NumLK) En Usemos Linux ya se escribió una entrada en su momento explicando cómo funciona y como se instala este programa, podéis ver la entrada [aquí](http://usemoslinux.blogspot.com.es/2012/08/como-controlar-el-trafico-de-red-desde.html). El problema es que en Fedora no se encuentra en los repositorios y encontrar el RPM es bastante difícil  por eso os lo volvemos a traer. Antes que nada, debéis instalar las dependencias para ejecutar programas en 32 bits. Si vuestra máquina es de 32 bits o ya las tenéis instaladas, saltad al paso siguiente: 
    
    
    sudo yum install alsa-lib.i686 fontconfig.i686 freetype.i686 glib2.i686 libSM.i686 libXScrnSaver.i686 libXi.i686 libXrandr.i686 libXrender.i686 libXv.i686 libstdc++.i686 pulseaudio-libs.i686 qt.i686 qt-x11.i686 zlib.i686

Luego, lo que tenéis que hacer es bajar el RPM de [aquí](http://www.mediafire.com/?6tbr5dt4z44t85g). Lo abres, dejas que se instale y luego, para ejecutarlo, teclear en la terminal, como administrador o mediante sudo lo siguiente: 
    
    
    tleds -d 10 wlan0

...cambiando el 10 por el tiempo que quieres de actualización (en milisegundos) y wlan0 por el nombre del dispositivo que quieres monitorear. Si quieres acceder a opciones más avanzadas, entra en la ayuda con el siguiente código: 
    
    
    tleds -h

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
