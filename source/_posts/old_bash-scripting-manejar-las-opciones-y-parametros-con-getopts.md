---
title: (ES)(Bash Scripting) Manejar las opciones y parámetros con getopts
date: 2013/09/14 18:48:45
---

Cuando nuestros script bash se van haciendo más complicados, solemos incluir la posibilidad de pasarles opciones como parámetro. Hasta ahora lo hacíamos con un case simple que comprobaba el valor de $1, $2,... Esto tenía el problema de que las opciones tenían que estar en un orden determinado. Hoy os voy a enseñar a usar el programa **getopts**, que nos quita ese problema y muchos más. Antes de nada, varias aclaraciones: 

  * **getopts** solamente funciona con opciones monocaracter ( **-x -f -v** ...) no con las del tipo **\--opcion** u **-opcion**
  * El carácter que identifique la opción puede ser mayúscula, minúscula o dígito. Otros caracteres son válidos, pero se desaconseja su uso para evitar problemas con caracteres especiales
  * En este caso: 
    
        miprograma -f archivo.txt -x

**-x** es un flag, es decir, una opción que puede tomar el valor **1** o **0**; y **-f** es una opción que necesita un argumento adicional. Esta forma de escribirlo sería equivalente: 
    
         miprograma -xf archivo.txt

Dicho esto, veamos como utilizar** getopts** con un primer ejemplo sencillo: 
    
    
    while getopts ":x" opt; do        #Cuando no quedan parámetros, getopts devuelve FALSE y sale del bucle
       case $opt in
          x) echo "Opción -x presente. " ;;
          *) echo "Opción inválida: -$OPTARG" ;;
       esac
    done

El script detectará si se le ha pasado la opción **-x** o no. Si se le ha pasado cualquier otra, la identificará como no válida: 
    
    
    bash ejemplo.sh -a -x -h -x
    Opción inválida: -a
    Opción -x presente. 
    Opción inválida: -h

Como veis, una opción inválida no detiene la ejecución en ningún caso. Si ese es tu objetivo tendrás que introducir un **exit** donde corresponda. Fijaos también en que se le pueden pasar varias veces la misma opción sin ningún problema. Vamos ahora con algo un poco más complejo: una opción con un parámetro adicional: 
    
    
    while getopts ":x:" opt; do
      case $opt in
        x)  echo "Opción -x activada con parámetro: $OPTARG";;
        \?) echo "Opción inválida: -$OPTARG";;
        :)  echo "Opción -$OPTARG necesita un parámetro";;
      esac
    done

Al ejecutarlo.... 
    
    
    bash ejemplo.sh -x param1 -x param2 -f -x
    Opción -x activada con parámetro: param1
    Opción -x activada con parámetro: param2
    Opción inválida: -f
    Opción -x necesita un parámetro

  A partir de ahora, os será más fácil lidiar con las opciones y los argumentos de estas. Recuerda, comentar es gratis ;) [Fuente](http://wiki.bash-hackers.org/howto/getopts_tutorial)

<!-- PELICAN_END_SUMMARY -->
## Comments

**[jhonatan](#433 "2015-11-28 05:32:52"):** Execelente post, algo tan sencillo como esto nadie lo explica (Y)

**[Andres](#451 "2016-03-29 16:05:19"):** Genial !!! Un ejemplo vale mas que mil palabras. Además me encantó el detalle de las aclaraciones. Los felicito por la dedicacion y el esfuerzo de compartir conocimiento y por tomarse el trabajo de hacerlo bien.



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
