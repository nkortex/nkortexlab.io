---
title: (ES)Utilizar Dropbox desde Terminal con Dropbox Uploader
date: 2013/09/12 10:28:48
layout: post
path: "/dropbox-from-cli/"
category: "Imported from my older blog"
---
Acabo de descubrir un script llamado Dropbox Uploader que sirve para manejar tus archivos de dropbox (descargar, subir, borrar,...) pero sin interfaz gráfica, solo desde terminal. Esto puede ser muy útil para automatización, y para ser usado desde otros scripts que hagamos. Voy a enseñaros como utilizarlo. ![](http://blogs.protegerse.com/laboratorio/wp-content/logo.png) Antes de nada, nos descargamos los programas necesarios: 
    
    
    sudo yum install git curl

curl es probable que ya lo tengáis, pero por si acaso. Luego nos descargamos el programa en si: 
    
    
    git clone https://github.com/andreafabrizi/Dropbox-Uploader/

Y ya lo tenemos en el ordenador. Simple eh? Pues ahora hay que darle acceso a tu cuenta. Ejecutalo por primera vez y sigue los pasos que se indican (Hay que crear una nueva aplicación de dropbox y darle los permisos necesarios) Para usarlo simplemente ejecutádlo sin parámetros y os devolverá la ayuda: 
    
    
    ./dropbox_uploader.sh
    Dropbox Uploader v0.12
    Andrea Fabrizi - andrea.fabrizi@gmail.com
    Usage: ./dropbox_uploader.sh COMMAND [PARAMETERS]...
    
    Commands:
    upload [LOCAL_FILE/DIR] <remote_file dir="">
    download [REMOTE_FILE/DIR] <local_file dir="">
    delete [REMOTE_FILE/DIR]
    move [REMOTE_FILE/DIR] [REMOTE_FILE/DIR]
    copy [REMOTE_FILE/DIR] [REMOTE_FILE/DIR]
    mkdir [REMOTE_DIR]
    list <remote_dir>
    share [REMOTE_FILE]
    info
    unlink
    
    Optional parameters:
    -f [FILENAME] Load the configuration file from a specific file
    -s Skip already existing files when download/upload. Default: Overwrite
    -d Enable DEBUG mode
    -q Quiet mode. Don't show messages
    -p Show cURL progress meter
    -k Doesn't check for SSL certificates (insecure)
    
    For more info and examples, please see the README file.

  Y esto para que vale? Si dropbox hace sincronización automática de las carpetas que le indiquemos? Pues la imaginación es el límite. Podemos hacer copias de seguridad selectivas, comprimir los archivos para que ocupen menos,...

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
