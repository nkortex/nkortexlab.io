---
title: (ES)Monitorear el uso de ancho de banda de tus programas con nethogs
date: 2014/02/05 09:16:37
layout: post
path: "/nethogs-tutorial/"
category: "Imported from my older blog"
---
![](http://definanzas.com/wp-content/uploads/banda-ancha1.jpg) **Nethogs** es una herramienta que nos permite obtener datos de la cantidad de ancho de banda que consume cada programa en tiempo real. Se encuentra en la mayoría de los repositorios, así que lo descargamos: 
    
    
    # yum install nethogs

Una vez instalado correctamente lo lanzamos así: 
    
    
    # nethogs wlan0

cambiando wlan0 por la interfaz que queramos monitorizar ( Si no estás seguro,** iwconfig** ). Esto nos dará una salida parecida a esta: ![Captura de pantalla de 2014-02-05 10:11:38](/wp-content/uploads/2014/02/Captura-de-pantalla-de-2014-02-05-101138.png) Como veis, indica el ancho de banda actual de cada proceso mas una suma total. Para más info, **man **

> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
