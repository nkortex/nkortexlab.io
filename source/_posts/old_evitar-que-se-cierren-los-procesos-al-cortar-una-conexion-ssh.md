---
title: (ES)Evitar que se cierren los procesos al cortar una conexión SSH
date: 2013/10/02 18:06:55
layout: post
path: "/permanent-processes-ssh-nohup/"
category: "Imported from my older blog"
---

![](http://hotfixed.net/wp-content/uploads/2011/09/tunel-ssh.jpg) Cuando cortamos la conexión de forma manual, o por causas ajenas a nosotros, los procesos que estén asociados a esa sesión, se finalizarán automáticamente. Por ejemplo, si has lanzado un programa que descarga un archivo y cierras la terminal en el ordenador local, se cancelará la descarga; posiblemente no sea eso lo que quieres. Pero hay un modo de evitar esto: el programa nohup. Este programa se usa de este modo: 
    
    
    nohup script_que_no_quiero_cerrar.sh &

Esto ejecutará el script hasta que acabe, no hasta que se corte la conexión. Nohup almacenará la salida que produzca en un archivo de ese mismo directorio para que puedas comprobar el funcionamiento en otro momento, a modo de log. El símbolo & al final le indica al sistema que lo ejecute en segundo plano, para poder seguir trabajando sin problema.

<!-- PELICAN_END_SUMMARY -->
## Comments

**[Alejandro](#258 "2013-10-13 21:47:57"):** Interesante, también tienes el programa "screen" que te permite trabajar con múltiples consolas virtuales simultáneamente, y si se corta la conexión, las consolas se mantienen. Pruebalo, creo que te gustará. Un saludo.

**[admin](#259 "2013-10-15 18:04:45"):** Le echaré un ojo, gracias Alejandro!



> Article automatically imported from my old wordpress blog. Here only for reference. Possible format errors and probable missing images. Sorry about that.
