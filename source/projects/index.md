---
title: projects
date: 2018-07-07 09:16:13
---

### md5dec.sh . CLI tool for online md5 decryption 
https://github.com/ncortex/md5dec

### Streaming-dl. Descarga vídeos de servicios de streaming como Allmyvideos.

Este script permite descargar vídeos desde el terminal. Pasándole como parámetro las urls de los vídeos, el script hará las peticiones necesarias al servidor, haciéndose pasar por un usuario legítimo, para conseguir la localización final del archivo y descargarlo sin tener que pagar suscripción premium.

Esto, mas que un programa 100% depurado, se trata de una prueba de concepto para demostrar que todo lo que se puede “ver online” en verdad se puede descargar. Sin embargo, los términos y condiciones de estos servicios pueden prohibir el uso de este tipo de programas.

Por el momento funciona con los siguientes servicios: AllMyVideos, VidSpot, Played.to, Powvideo y StreamCloud.
https://github.com/ncortex/Streaming-dl


### Thief Alert. Evita que te roben el portátil.
Si vas a dejar tu portatil descuidado, mejor ejecuta antes este script que hará sonar una alarma si se desconecta de la corriente. Además, hará una foto con la webcam y la enviará por telegram. Si te lo roban sin llegar a desconectarlo de la corriente no sonará, pero solo tienes que seguir el cable para dar con el culpable x)


### Instant working. Extension de gnome-shell
Vuelve rápidamente al primer escritorio haciendo scroll en el panel superior de gnome3.
https://extensions.gnome.org/extension/1003/instant-working

